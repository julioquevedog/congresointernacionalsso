<?php
class Model_congreso_virtual extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	
	/*
	 * @Function que extrae la informacion de los cuadros que se muestran debajo del banner
    */
      

        public function login($correo,$password,$hoy) {
            $this->db->where('correo',$correo);
            $this->db->where('password',$password);            
            $result =  $this->db->get('usuarios_congreso')->row_array();
            if(count($result)>0){
                if($result['estado']<>'A'){
                    return 'estado';
                }else{
                    $timeIni = strtotime($result['fecha_ini']);
                    $timeFin = strtotime($result['fecha_fin']);
                    $timeHoy = strtotime($hoy);
                    if($timeIni <= $timeHoy and $timeHoy <= $timeFin){
                        return $result;
                    }else{
                        return 'vigencia';
                    }
                }
                
            }else{
                return 'credenciales';
            }
        }        
        
       
}
?>