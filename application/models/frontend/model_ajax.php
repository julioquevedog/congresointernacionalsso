<?php
class Ajax_model extends CI_Model {
    public function __construct()
    {
            parent::__construct();
    }
    
    public function getProducto($id_producto) {
        $this->db->select('nombre,codigo');
        $this->db->where('id_producto', $id_producto);
        $query =  $this->db->get('productos');
        return $query->row();
    }  

    public function getPrecio($id_unidad,$id_producto) {
        $this->db->select('precio');
        $this->db->where('id_producto', $id_producto);
        $this->db->where('id_unidad', $id_unidad);        
        $query =  $this->db->get('precios');
        return $query->row();
    }
    public function grabaPedido($data) {
       $resultado = $this->db->insert('ordenes', $data);
       return $resultado;
   }
   
    public function verifEmail($email) {
        $this->db->where('email', $email);
        $query =  $this->db->get('inscritos');
        return $query->num_rows();
    } 
	
	
}
?>