<?php
class Model_index extends CI_Model {
	public function __construct()
	{
		parent::__construct();
	}
	
	/*
	 * @Function que extrae la informacion de los cuadros que se muestran debajo del banner
    */
        public function getIn($table,$where) {
            $this->db->where_in($where['field'],$where['data']);
            $this->db->order_by('orden');
            $query =  $this->db->get($table);
            return $query->result_array();
        }

        public function login($correo,$password,$hoy) {
            $this->db->where('correo',$correo);
            $this->db->where('password',$password);            
            $result =  $this->db->get('usuarios_mem')->row_array();
            if(count($result)>0){
                if($result['estado']<>'A'){
                    return 'estado';
                }else{
                    $timeIni = strtotime($result['fecha_ini']);
                    $timeFin = strtotime($result['fecha_fin']);
                    $timeHoy = strtotime($hoy);
                    if($timeIni <= $timeHoy and $timeHoy <= $timeFin){
                        return $result;
                    }else{
                        return 'vigencia';
                    }
                }
                
            }else{
                return 'credenciales';
            }
        }        
        
        public function getInTema($table,$where) {
            $this->db->select('a.*,b.nombres as nomPonente,b.imagen as imgPonente,b.cargo as carPonente,b.link_facebook,b.link_twitter,b.link_linkedin,b.link_google');
            $this->db->join('ponentes b','b.id = a.ponente_id','left');
            $this->db->where_in($where['field'],$where['data']);
            $this->db->order_by('a.orden');
            $query =  $this->db->get($table.' a');
            return $query->result_array();
        }        
        
        public function get($table,$where=null) {
            $this->db->where($where);
            $this->db->order_by('orden');
            $query =  $this->db->get($table);
            return $query->result_array();
        }

     
        public function getItem($table,$where) {
            $this->db->where($where);
            $query =  $this->db->get($table);
            return $query->row_array();
        }  
        
        public function verifica_code_video($table,$where) {

            $this->db->where($where);
            $query =  $this->db->get($table);
            return $query->row_array();
        }         

        public function getEvento($table,$where) {
            $this->db->where($where);
            $this->db->order_by('id desc');
            $this->db->limit(1);            
            $query =  $this->db->get($table);
            return $query->row_array();
        }    
        
        public function insert($table,$data) {
            $this->db->insert($table,$data);
            return $this->db->insert_id();
        }                  

       
        public function update($table,$data,$where) {
            $this->db->where($where);
            $result=$this->db->update($table, $data);
            return $result;
        }        
}
?>