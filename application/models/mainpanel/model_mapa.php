<?php
class Model_mapa extends CI_Model {
    public function __construct()
    {
            parent::__construct();
    }

    
    public function getMapa() {
        $query =  $this->db->get('sedes');
        return $query->row_array();
    }

	
    public function updateMapa($data, $id) {
        $this->db->where('id_sede', $id);
        $result=$this->db->update('sedes', $data);
        return $result;
    }
	

}
?>
