<?php
class Model_base extends CI_Model {
    public function __construct()
    {
            parent::__construct();
    }

    public function getList($table,$where=null,$orden=null) {
        if($where) $this->db->where($where);        
        if($orden){$this->db->order_by($orden);}else{$this->db->order_by('orden');}
        $query =  $this->db->get($table);
        return $query->result();
    }
    


    public function get($table,$where){
        $this->db->where($where);
        $query =  $this->db->get($table);
        return $query->row();
    }


    public function delete($id,$table) {
        $this->db->where('id', $id);
        $result=$this->db->delete($table);
        return $result;
    }	
    

    public function update($data,$table,$where) {
        $this->db->where($where);
        $result=$this->db->update($table, $data);
        return $result;
    }
	
    public function save($table,$data) {
        $resultado = $this->db->insert($table, $data);
        return $resultado;
    }

    public function lastItem($table,$where) {
        $this->db->select('max(orden) as orden');
        $this->db->where($where);
        $resultado = $this->db->get($table);
        return $resultado->row();
    }    
}
?>
