<?php
class Model_album extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->tabla="album";      
    }

    public function lastItem($where) {
        $this->db->select('max(orden) as orden');
        $this->db->where($where);
        $resultado = $this->db->get($this->tabla);
        return $resultado->row();
    }
}
?>
