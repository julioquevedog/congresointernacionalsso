<?php
class Model_ajax extends CI_Model {
    public function __construct()
    {
            parent::__construct();
    }
    
    public function updateusuario($nombres,$newpasw,$idusuario,$password) {
        $this->db->where('idusuario', $idusuario);
        $this->db->where('password', $password);        
        $query =  $this->db->get('usuarios');
        $num=$query->num_rows();
        if($num>0){
            if(!empty($newpasw)):$ps=$newpasw; else :$ps=$password; endif;
            $data = array(
               'password' => $ps,
               'nombres' => $nombres,                
            );
            $this->db->where('idusuario', $idusuario);
            $result=$this->db->update('usuarios', $data);
        }else{
            $result='';
        }
        return $result;
    }  

    public function deleteCliente($id_cliente) {
        $this->db->where('id', $id_cliente);
        $this->db->delete('inscritos');
    } 

    public function getColor($id_color) {
        $this->db->where('id', $id_color);
        $query =  $this->db->get('colores');
        return $query->row();
    }     

    public function getListaProductos($id_categoria) {
        $this->db->select('id_producto');
	$this->db->where('id_categoria_padre',$id_categoria);
        $query =  $this->db->get('productos');
        return $query->result();
    }
    public function updateStock($id_stock, $data) {
        $this->db->where('id', $id_stock);
        $this->db->update('stock_color', $data);
    }      
    public function ordProd($id,$data) {
        $this->db->where('id_producto', $id);
        $this->db->update('productos', $data);
    }       
}
?>