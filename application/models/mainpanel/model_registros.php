<?php
class Model_registros extends CI_Model {
    public function __construct()
    {
            parent::__construct();
            $this->tabla="registros";            
            $this->idtabla="id_".$this->tabla; 
    }

    public function getList() {
        $this->db->where('type_comprador','1');        
        $this->db->where('delete_logic', 0);        
        $this->db->order_by('orden');
        $query =  $this->db->get($this->tabla);
        return $query->result();
    }

    
    public function get($id) {
        $this->db->where('type_comprador','1');       
        $this->db->where('delete_logic',0);         
        $this->db->where( $this->idtabla, $id);
        $query =  $this->db->get($this->tabla);
        return $query->row();
    }

	
    public function update($data) {
        $this->db->where( $this->idtabla, $data[$this->idtabla]);
        $result=$this->db->update($this->tabla, $data);
        return $result;
    }
	
    public function save($data) {
        $resultado = $this->db->insert($this->tabla, $data);
        return $resultado;
    }


    public function delete($id) {
        $this->db->where( $this->idtabla, $id);
        $result=$this->db->delete($this->tabla);
        return $result;
    }    
    
    
    public function ult() {
        $this->db->select_max('orden');
        $query =  $this->db->get($this->tabla);
        return $query->row_array();
    }      
}
?>
