<?php
class Model_servicios_stands extends CI_Model {
    public function __construct()
    {
            parent::__construct();
            $this->tabla="servicios_stands";               
    }

    	
    public function lastItem($where) {
        $this->db->select('max(orden) as orden');
        $this->db->where($where);
        $resultado = $this->db->get($this->tabla);
        return $resultado->row();
    }
}
?>
