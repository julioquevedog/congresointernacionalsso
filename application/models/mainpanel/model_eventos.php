<?php
class Model_eventos extends CI_Model {
    public function __construct()
    {
            parent::__construct();
            $this->tablaPadre="subcategorias";
            $this->tabla="eventos";               
    }

    // public function get($id=null) {
    //     $this->db->select('a.*,b.nombre as categoria_padre_nombre, c.nombres vendedor_nombres, c.apellidos vendedor_apellidos,d.nombre_categoria');
    //     $this->db->from($this->tabla.' as a');
    //     $this->db->join('subcategorias b', 'b.id_subcategoria=a.id_categoria_padre', 'left');        
    //     $this->db->join('registros c', 'c.id_registros=a.id_vendedor', 'left');                
    //     $this->db->join('categorias d', 'd.id_categoria=b.id_categoria', 'left');                        
    //     ($id<>null) ? $this->db->where('a.id_categoria_padre', $id) : $this->db->where('a.closet_sales','0'); 
    //     $this->db->order_by('a.fecha_insert','desc');
    //     $this->db->order_by('a.fecha_update','desc');        
    //     $query =  $this->db->get();
    //     return $query->result();
    // }
    
	
    public function save($data) {
        $resultado = $this->db->insert($this->tabla, $data);
        return $resultado;
    }

    public function update($data) {
        $this->db->where('id', $data['id']);
        $resultado=$this->db->update($this->tabla, $data);
        return $resultado;
    }    
    
    
    public function delete($id) {
        $this->db->where('id', $id);
        $result=$this->db->delete($this->tabla);
        return $result;
    }    
    
}
?>
