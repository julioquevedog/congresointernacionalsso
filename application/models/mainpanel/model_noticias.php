<?php
class Model_noticias extends CI_Model {
    public function __construct()
    {
            parent::__construct();
    }

    public function getListaNoticias() {
        $this->db->order_by('orden');
        $query =  $this->db->get('noticias');
        return $query->result();
    }
    
    public function getNoticia($id_noticia) {
        $this->db->where('id_noticia', $id_noticia);
        $query =  $this->db->get('noticias');
        return $query->row();
    }
	

    public function updateNoticia($id_noticia, $data) {
        $this->db->where('id_noticia', $id_noticia);
        $result=$this->db->update('noticias', $data);
        return $result;
    }
	
    public function grabarNoticia($data) {
        $resultado = $this->db->insert('noticias', $data);
        return $resultado;
    }

    public function delete($id) {
        $this->db->where('id_noticia', $id);
        $result=$this->db->delete('noticias');
        return $result;
    }    

    public function ultNoti() {
        $this->db->select_max('orden');
        $query =  $this->db->get('noticias');
        return $query->row_array();
    }      
}
?>
