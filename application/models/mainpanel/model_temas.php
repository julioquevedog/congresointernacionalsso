<?php
class Model_temas extends CI_Model {
    public function __construct()
    {
        parent::__construct();
        $this->tabla="temas";      
    }

    public function lastItem($where) {
        $this->db->select('max(orden) as orden');
        $this->db->where($where);
        $resultado = $this->db->get($this->tabla);
        return $resultado->row();
    }
}
?>
