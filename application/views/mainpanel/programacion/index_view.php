
    <ul class="breadcrumb">
        <li><b>Evento: </b><?php echo $evento->nombre;?></li>
        <!-- <span class="divider">/</span> -->
        <!-- <li><a href="mainpanel/controller_programacion/nuevo">Agregar Noticia</a></li> -->
    </ul>
    <a href="mainpanel/controller_programacion/nuevo/<?php echo $evento->id?>" class="btn btn-mini btn-success">Crear programación</a>

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Programación</h2>
            <div class="box-icon">
            <a href="mainpanel/controller_eventos/listar" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="25%">Imagen</th>                                                
                        <th width="25%">Texto 1</th>                        
                        <th width="25%">Texto 2</th>                                                
                        <th width="5%">Estado</th>                        
                        <th width="5%">Orden</th>                                                
                        <th width="10%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {

                        if(is_file('files/programacion/'.$value->imagen))
                        {
                            $img = getimagesize('files/programacion/'.$value->imagen);
                            $ancho = (int)($img[0]/2);
                            $alto = (int)($img[1]/2);                            
                            $pic = '<img src="files/programacion/'.$value->imagen.'" border="0" width="'.$ancho.'" />';
                        }
                        else
                        {
                            $pic='foto';
                        }

                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$pic.'</td>';                        
                        echo '<td>'.$value->texto1.'</td>';
                        echo '<td>'.$value->texto2.'</td>';                        
                        echo '<td class="center">'.$value->estado.'</td>';                        
                        echo '<td class="center">'.$value->orden.'</td>';                                                
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_programacion/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el programa '.$value->texto1.'?" data-id="'.$value->id.'"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_programacion/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_bloques/listar/'.$value->id.'" title="Bloques"><i class="icon-tag icon-white"></i></a> ';                        
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->