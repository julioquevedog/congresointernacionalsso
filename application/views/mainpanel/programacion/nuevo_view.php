    <ul class="breadcrumb">
        <li><b><?php echo $evento->nombre;?></b></li>
        <!-- <span class="divider">/</span> -->
        <!-- <li><a href="mainpanel/controller_programacion/nuevo">Agregar Noticia</a></li> -->
    </ul>
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-plus"></i> Crear programación
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="mainpanel/controller_programacion/grabar" method="post" enctype="multipart/form-data" 
                    onsubmit="return validar()">
                    <fieldset>
                        <legend>Ingrese los datos</legend>
                        <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        } 
                        ?>

                                        
                                        <div class="control-group">
                            <label class="control-label" for="typeahead">Texto 1</label>
                            <div class="controls">
                                <input type="text" class="span12 typeahead" id="texto1" name="texto1" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Texto 2</label>
                            <div class="controls">
                                <input type="text" class="span12 typeahead" id="texto2" name="texto2" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>   
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Texto 3</label>
                            <div class="controls">
                                <input type="text" class="span12 typeahead" id="texto3" name="texto3" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Orden</label>
                            <div class="controls">
                                <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $orden;?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                        

                        <div class="control-group">
                            <label class="control-label">Estado</label>
                            <div class="controls">
                                <label class="radio">
                                    <input type="radio" name="estado" id="estado1" value="A" checked="checked">
                                    Activo
                                </label>
                                <div style="clear:both"></div>
                                <label class="radio">
                                    <input type="radio" name="estado" id="estado2" value="I">
                                    Inactivo
                                </label>
                            </div>
                        </div> 
                        <hr>                   
                        <div class="control-group">
                            <label class="control-label">Subir Imagen <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                            <div class="controls">
                            <input type="file" name="imagen" id="imagen">
                            <br>
                            <span class="help-inline red">La imagen que suba debe ser de 164px de ancho por 164px de alto..</span>
                            </div>
                        </div>  
                                                
                        <div class="form-actions">
                            <input type="hidden" value="<?php echo $evento->id;?>" name="evento_id">
                            <input type="submit" class="btn btn-primary" value="GRABAR">
                            &nbsp;&nbsp;
                            <a class="btn btn-danger" href="mainpanel/controller_programacion/listar/<?php echo $evento->id?>">VOLVER AL LISTADO</a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
    </div><!--/row-->        



