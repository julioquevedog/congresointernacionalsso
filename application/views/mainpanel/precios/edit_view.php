

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar PRECIO</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_precios/listar" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_precios/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validarp()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>

                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Título</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="titulo" name="titulo" value="<?php echo $data->titulo?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Sub título</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="subtitulo" name="subtitulo" value="<?php echo $data->subtitulo?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Moneda</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="moneda" name="moneda" value="<?php echo $data->moneda?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Precio</label>
                        <div class="controls">
                            <input type="text" class="span4 typeahead" id="precio" name="precio" value="<?php echo $data->precio?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                       
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 1</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt1" name="txt1" value="<?php echo $data->txt1?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                       
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Caracteristica 1</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="item1" name="item1" value="<?php echo $data->item1?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>  
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Caracteristica 2</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="item2" name="item2" value="<?php echo $data->item2?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                      
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Caracteristica 3</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="item3" name="item3" value="<?php echo $data->item3?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                       
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Caracteristica 4</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="item4" name="item4" value="<?php echo $data->item4?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Caracteristica 5</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="item5" name="item5" value="<?php echo $data->item5?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                                               
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Caracteristica 6</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="item6" name="item6" value="<?php echo $data->item6?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                                                                   
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $data->orden?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link para pagar</label>
                        <div class="controls">
                            <input type="text" class="span12 typeahead" id="link_pago" name="link_pago" value="<?php echo $data->link_pago?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                    
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado1" value="A" <?php echo ($data->estado=='A')?'checked="checked"':'';?>>
                                Activo
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado2" value="I" <?php echo ($data->estado=='I')?'checked="checked"':'';?>>
                                Inactivo
                            </label>
                        </div>
                    </div>  
                    <hr>                   
                    <div class="control-group">
                        <label class="control-label">Imagen <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                        <div class="controls">
                            <div class="span6"><img src="files/precios/<?php echo $data->imagen; ?>" /></div>
                            <input type="file" name="imagen" id="imagen">
                            <br>
                            <span class="help-inline red">La imagen que suba debe ser de 263px de ancho por 229px de alto.</span>                        
                        </div>
                    </div>                     
                    
                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">                        
                        <input type="hidden" name="imgatng" value="<?php echo $data->imagen; ?>">                        
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_precios/listar">VOLVER AL LISTADO</a>
                        <!-- <a data-rel="tooltip" class="btn btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        la pregunta <?php echo $data->titulo;?>?" data-id="<?php echo $data->id;?>"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_precios/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i>ELIMINAR</a> -->
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

