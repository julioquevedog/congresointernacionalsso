<!-- <a href="mainpanel/controller_precios/nuevo" class="btn btn-mini btn-success">Crear testimonio</a> -->

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> PRECIOS</h2>
            <div class="box-icon">
            
            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="10%">Img</th>
                        <th width="15%">Precio</th>                                                
                        <th width="25%">Título</th>
                        <th width="15%">Sub título</th>                        
                        <th width="10%">Estado</th>
                        <th width="10%">Orden</th>                        
                        <th width="10%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {

                        if(is_file('files/precios/'.$value->imagen))
                        {
                            $img = getimagesize('files/precios/'.$value->imagen);
                            $ancho = (int)($img[0]/2);
                            $alto = (int)($img[1]/2);                            
                            $pic = '<img src="files/precios/'.$value->imagen.'" border="0" width="'.$ancho.'" />';
                        }
                        else
                        {
                            $pic='foto';
                        }

                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$pic.'</td>';
                        echo '<td>'.$value->moneda.' '.$value->precio.'</td>';                        
                        echo '<td>'.$value->titulo.'</td>';                                                
                        echo '<td>'.$value->subtitulo.'</td>';
                        echo '<td class="center">'.$value->estado.'</td>';
                        echo '<td class="center">'.$value->orden.'</td>';                        
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_precios/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        //echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        //la FAQ '.$value->pregunta.'?" data-id="'.$value->id.'"
                        //data-elementos-dependientes="0" data-url="mainpanel/controller_precios/delete" 
                        //href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->