
<ul class="breadcrumb">
    <li><b>Album: </b><?php echo $album->title;?></li>
</ul>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Fotografía</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_fotos/listar/<?php echo $album->id?>" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_fotos/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validarp()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>

                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $data->orden?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado1" value="A" <?php echo ($data->estado=='A')?'checked="checked"':'';?>>
                                Activo
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado2" value="I" <?php echo ($data->estado=='I')?'checked="checked"':'';?>>
                                Inactivo
                            </label>
                        </div>
                    </div>    
                    <hr>                   
                    <div class="control-group">
                        <label class="control-label">Imagen <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                        <div class="controls">
                            <div class="span6"><img src="files/fotos/<?php echo $data->imagen; ?>" /></div>
                            <input type="file" name="imagen" id="imagen">
                            <br>
                            <span class="help-inline red">La imagen que suba debe ser de 875px de ancho por 690px de alto.</span>                        
                        </div>
                    </div>     

                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">                        
                        <input type="hidden" name="imgatng" value="<?php echo $data->imagen; ?>">                        
                        <input type="hidden" name="imgatng2" value="<?php echo $data->imagen2; ?>">                                                
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_fotos/listar/<?php echo $album->id;?>">VOLVER AL LISTADO</a>
                        <a data-rel="tooltip" class="btn btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        la fotografía ?" data-id="<?php echo $data->id;?>"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_fotos/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i>ELIMINAR</a>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<script>
    function validarp(){

        var val = $('select[name=id_categoria_padre]').val();
        if(val==0){
            alert("Debe escoger una subcategoria");
            return false;
        }
    }

    $("input[name=closet_sales]").change(function(){
        var val=$(this).val();
        if(val==1){
            $(".wrSelectCloset").show();
        }else{
            $(".wrSelectCloset").hide();
        }
    });

</script>