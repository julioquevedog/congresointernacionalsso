<a href="mainpanel/controller_videos/nuevo" class="btn btn-mini btn-success">Crear Video</a>

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> VIDEOS</h2>
            <div class="box-icon">
            
            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="10%">Video</th>
                        <th width="10%">Fuente</th>                        
                        <th width="5%">Tipo</th>                                                
                        <th width="10%">Código</th>                                                
                        <th width="15%">Nombre</th>
                        <th width="25%">Sumilla</th>                        
                        <th width="5%">Estado</th>
                        <th width="5%">Orden</th>                        
                        <th width="10%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {
                            
                        if($value->fuente==0){
                            $thumbURL = 'http://img.youtube.com/vi/'.$value->code.'/0.jpg';
                            $img = '<img src="'.$thumbURL.'" width="100px"/>';                                
                        }elseif($value->fuente==1){
                            $img = '<iframe src="https://player.vimeo.com/video/'.$value->code.'" width="100" height="60" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                        }
                        
                        $fuente = ($value->fuente==0)? '<img src="./assets/frontend/images/logo-youtube.png" alt="">':'<img src="./assets/frontend/images/logo-vimeo.png" alt="">';
                        $tipo = ($value->tipo==0)? '<img src="./assets/frontend/images/unlocked.png" alt="">':'<img src="./assets/frontend/images/locked.png" alt="">';
                        $estado = ($value->estado=='A')? '<span class="label label-success">Activo</span>':'<span class="label label-important">Inactivo</span>';
                        

                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$img.'</td>';
                        echo '<td>'.$fuente.'</td>';                        
                        echo '<td>'.$tipo.'</td>';
                        echo '<td>'.$value->code.'</td>';                        
                        echo '<td>'.$value->nombre.'</td>'; 
                        echo '<td>'.$value->sumilla.'</td>';
                        echo '<td class="center">'.$estado.'</td>';
                        echo '<td class="center">'.$value->orden.'</td>';                        
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_videos/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el video '.$value->nombre.'?" data-id="'.$value->id.'"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_videos/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->