

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Video</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_videos/listar" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_videos/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validarp()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>

                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombre:</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="nombre" name="nombre" value="<?php echo $data->nombre?>" data-required="true" data-message-required="Se requiere" maxlength="25">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Sumilla:</label>
                        <div class="controls">
                            <textarea id="sumilla" name="sumilla" class="span6" style="width:400px;height:100px;"><?php echo $data->sumilla;?></textarea>
                        </div>
                    </div>  
                    <div class="control-group">
                        <label class="control-label">Fuente:</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="fuente" id="fuente1" value="0" <?php echo ($data->fuente==0)?'checked="checked"':'';?>>
                                <img src="./assets/frontend/images/logo-youtube.png" alt="">
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="fuente" id="fuente2" value="1" <?php echo ($data->fuente==1)?'checked="checked"':'';?>>
                                <img src="./assets/frontend/images/logo-vimeo.png" alt="">
                            </label>
                        </div>
                    </div> 

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Código del video:</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="code" name="code" value="<?php echo $data->code?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>  

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Img del Video</label>
                        <div class="controls">
                            <?php
                            
                            if($data->fuente==0){
                                $thumbURL = 'http://img.youtube.com/vi/'.$data->code.'/0.jpg';
                                echo '<img src="'.$thumbURL.'" width="100px"/>';                                
                            }elseif($data->fuente==1){
                                $iframe = '<iframe src="https://player.vimeo.com/video/'.$data->code.'" width="100" height="70" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
                                echo $iframe;
                            }
                            ?>
                        </div>
                    </div>   
                    
                    <div class="control-group">
                        <label class="control-label">Tipo</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="tipo" id="tipo1" value="0" <?php echo ($data->tipo=='0')?'checked="checked"':'';?>>
                                <img src="./assets/frontend/images/unlocked.png" alt=""> Público
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="tipo" id="tipo2" value="1" <?php echo ($data->tipo=='1')?'checked="checked"':'';?>>
                                <img src="./assets/frontend/images/locked.png" alt=""> Privado
                            </label>
                        </div>
                    </div> 

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $data->orden?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                   
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado1" value="A" <?php echo ($data->estado=='A')?'checked="checked"':'';?>>
                                Activo
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado2" value="I" <?php echo ($data->estado=='I')?'checked="checked"':'';?>>
                                Inactivo
                            </label>
                        </div>
                    </div>  
                                      
                    
                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">                        
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_videos/listar">VOLVER AL LISTADO</a>
                        <!-- <a data-rel="tooltip" class="btn btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        la pregunta <?php echo $data->titulo;?>?" data-id="<?php echo $data->id;?>"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_videos/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i>ELIMINAR</a> -->
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

