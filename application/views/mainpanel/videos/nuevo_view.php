
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-plus"></i> Crear Video
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="mainpanel/controller_videos/grabar" method="post" enctype="multipart/form-data" 
                    onsubmit="return validar()">
                    <fieldset>
                        <legend>Ingrese los datos</legend>
                        <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        } 
                        ?>

                                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Nombre:</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="nombre" name="nombre" value="" data-required="true" data-message-required="Se requiere" maxlength="25">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Sumilla:</label>
                            <div class="controls">
                                <textarea id="sumilla" name="sumilla" class="span6" style="width:400px;height:100px;"></textarea>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label">Fuente:</label>
                            <div class="controls">
                                <label class="radio inline">
                                    <input type="radio" name="fuente" id="fuente1" value="0" checked="checked">
                                    <img src="./assets/frontend/images/logo-youtube.png" alt="">
                                </label>
                                
                                <label class="radio inline">
                                    <input type="radio" name="fuente" id="fuente2" value="1">
                                    <img src="./assets/frontend/images/logo-vimeo.png" alt="">
                                </label>
                            </div>
                        </div> 
                                                
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Código del video:</label>
                            <div class="controls">
                                <input type="text" class="span2 typeahead" id="code" name="code" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                        
                      
                        <div class="control-group">
                            <label class="control-label">Tipo</label>
                            <div class="controls">
                                <label class="radio inline">
                                    <input type="radio" name="tipo" id="tipo1" value="0" checked="checked">
                                    <img src="./assets/frontend/images/unlocked.png" alt=""> Público
                                </label>
                                
                                <label class="radio inline">
                                    <input type="radio" name="tipo" id="tipo2" value="1">
                                    <img src="./assets/frontend/images/locked.png" alt=""> Privado
                                </label>
                            </div>
                        </div> 

                        <div class="control-group">
                            <label class="control-label" for="typeahead">Orden:</label>
                            <div class="controls">
                                <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $orden?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Estado:</label>
                            <div class="controls">
                                <label class="radio inline">
                                    <input type="radio" name="estado" id="estado1" value="A" checked="checked">
                                    Activo
                                </label>
                                
                                <label class="radio inline">
                                    <input type="radio" name="estado" id="estado2" value="I">
                                    Inactivo
                                </label>
                            </div>
                        </div>  
                

                        <div class="form-actions">
                            <input type="submit" class="btn btn-primary" value="GRABAR">
                            &nbsp;&nbsp;
                            <a class="btn btn-danger" href="mainpanel/controller_videos/listar">VOLVER AL LISTADO</a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
    </div>


