
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-plus"></i> Crear Stand
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="mainpanel/controller_stands/grabar" method="post" enctype="multipart/form-data" 
                    onsubmit="return validar()">
                    <fieldset>
                        <legend>Ingrese los datos</legend>
                        <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        } 
                        ?>

                                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Nombre</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="nombre" name="nombre" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Email</label>
                            <div class="controls">
                                <input type="text" class="span4 typeahead" id="email" name="email" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Celular</label>
                            <div class="controls">
                                <input type="text" class="span4 typeahead" id="celular" name="celular" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                          
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Url Web</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="url_web" name="url_web" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Url Facebook</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="url_facebook" name="url_facebook" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Url Twitter</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="url_twitter" name="url_twitter" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                                                                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Url Instagram</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="url_instagram" name="url_instagram" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                          
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Orden</label>
                            <div class="controls">
                                <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $orden?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Estado</label>
                            <div class="controls">
                                <label class="radio inline">
                                    <input type="radio" name="estado" id="estado1" value="A" checked="checked">
                                    Activo
                                </label>
                                
                                <label class="radio inline">
                                    <input type="radio" name="estado" id="estado2" value="I">
                                    Inactivo
                                </label>
                            </div>
                        </div>  
                        <hr>                   
                        <div class="control-group">
                            <label class="control-label">Banner <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                            <div class="controls">
                                <div class="span6">
                                    <input type="file" name="imagen" id="imagen">
                                    <br>
                                    <span class="help-inline red">El banner debe ser de 360px de ancho por 280px de alto.</span>                        
                                </div>
                            </div>
                        </div>                 
                        <hr>                   
                        <div class="control-group">
                            <label class="control-label">Logo <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                            <div class="controls">
                                <div class="span6">
                                    <input type="file" name="logo" id="logo">
                                    <br>
                                    <span class="help-inline red">El logo debe ser de 140px de ancho por 70px de alto.</span>                        
                                </div>
                            </div>
                        </div>    
                        <div class="form-actions">
                            <input type="submit" class="btn btn-primary" value="GRABAR">
                            &nbsp;&nbsp;
                            <a class="btn btn-danger" href="mainpanel/controller_stands/listar">VOLVER AL LISTADO</a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
    </div>


