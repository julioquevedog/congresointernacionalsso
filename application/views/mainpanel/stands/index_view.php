<a href="mainpanel/controller_stands/nuevo" class="btn btn-mini btn-success">Crear Stands</a>

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> STANDS</h2>
            <div class="box-icon">
            
            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="15%">Banner</th>
                        <th width="15%">Logo</th>                                                                        
                        <th width="25%">Nombre</th>                                                                                                
                        <th width="10%">Estado</th>
                        <th width="10%">Orden</th>                        
                        <th width="20%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {

                        if(is_file('files/stands/'.$value->imagen))
                        {
                            $img = getimagesize('files/stands/'.$value->imagen);
                            $ancho = (int)($img[0]/3);
                            $alto = (int)($img[1]/3);                            
                            $pic1 = '<img src="files/stands/'.$value->imagen.'" border="0" width="'.$ancho.'" />';
                        }
                        else
                        {
                            $img = getimagesize('assets/frontend/images/imagen-estandar-banner.jpg');
                            $ancho = (int)($img[0]/3);
                            $alto = (int)($img[1]/3);                            
                            $pic1 = '<img src="assets/frontend/images/imagen-estandar-banner.jpg" border="0" width="'.$ancho.'" />';                            
                        }

                        if(is_file('files/stands/'.$value->logo))
                        {
                            $img = getimagesize('files/stands/'.$value->logo);
                            $ancho = (int)($img[0]/1);
                            $alto = (int)($img[1]/1);                            
                            $pic2 = '<img src="files/stands/'.$value->logo.'" border="0" width="'.$ancho.'" />';
                        }
                        else
                        {
                            $img = getimagesize('assets/frontend/images/imagen-estandar-logo.jpg');
                            $ancho = (int)($img[0]/1);
                            $alto = (int)($img[1]/1);                            
                            $pic2 = '<img src="assets/frontend/images/imagen-estandar-logo.jpg" border="0" width="'.$ancho.'" />';                            

                        }                        
                        
                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$pic1.'</td>';                        
                        echo '<td>'.$pic2.'</td>';
                        echo '<td>'.$value->nombre.'</td>'; 
                        echo '<td class="center">'.$value->estado.'</td>';
                        echo '<td class="center">'.$value->orden.'</td>';                        
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_stands/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_servicios_stands/listar/'.$value->id.'" title="Servicios">Servicios</a> ';                        
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el stand '.$value->nombre.'?" data-id="'.$value->id.'"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_stands/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->