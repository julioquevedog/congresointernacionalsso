

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Stand</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_stands/listar" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_stands/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validarp()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>

                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombre</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="nombre" name="nombre" value="<?php echo $data->nombre?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                            <label class="control-label" for="typeahead">Email</label>
                            <div class="controls">
                                <input type="text" class="span4 typeahead" id="email" name="email" value="<?php echo $data->email?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Celular</label>
                            <div class="controls">
                                <input type="text" class="span4 typeahead" id="celular" name="celular" value="<?php echo $data->celular?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                          
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Url Web</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="url_web" name="url_web" value="<?php echo $data->url_web?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Url Facebook</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="url_facebook" name="url_facebook" value="<?php echo $data->url_facebook?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Url Twitter</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="url_twitter" name="url_twitter" value="<?php echo $data->url_twitter?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                                                                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Url Instagram</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="url_instagram" name="url_instagram" value="<?php echo $data->url_instagram?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                          
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Orden</label>
                            <div class="controls">
                                <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $data->orden?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Estado</label>
                            <div class="controls">
                                <label class="radio inline">
                                    <input type="radio" name="estado" id="estado1" value="A" <?php echo ($data->estado=='A')?'checked="checked"':'';?>>
                                    Activo
                                </label>
                                
                                <label class="radio inline">
                                    <input type="radio" name="estado" id="estado2" value="I" <?php echo ($data->estado=='I')?'checked="checked"':'';?>>
                                    Inactivo
                                </label>
                            </div>
                        </div>                       

                    <hr>                   
                    <div class="control-group">
                        <label class="control-label">Banner <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                        <div class="controls">
                            <div class="span6">
<?php
                        if(is_file('files/stands/'.$data->imagen))
                        {
                            $img = getimagesize('files/stands/'.$data->imagen);
                            $ancho = (int)($img[0]/1);
                            $alto = (int)($img[1]/1);                            
                            $pic1 = '<img src="files/stands/'.$data->imagen.'" border="0" width="'.$ancho.'" />';
                        }
                        else
                        {
                            $img = getimagesize('assets/frontend/images/imagen-estandar-banner.jpg');
                            $ancho = (int)($img[0]/1);
                            $alto = (int)($img[1]/1);                            
                            $pic1 = '<img src="assets/frontend/images/imagen-estandar-banner.jpg" border="0" width="'.$ancho.'" />';                            
                        }
                        echo $pic1;
?>                                
                        
                        </div>
                            <input type="file" name="imagen" id="imagen">
                            <br>
                            <span class="help-inline red">El banner debe ser de 360px de ancho por 280px de alto.</span>                        
                        </div>
                    </div>    
                    <hr>                   
                    <div class="control-group">
                        <label class="control-label">Logo <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                        <div class="controls">
                            <div class="span6">
                                
                            <?php
                            if(is_file('files/stands/'.$data->logo))
                            {
                                $img = getimagesize('files/stands/'.$data->logo);
                                $ancho = (int)($img[0]/1);
                                $alto = (int)($img[1]/1);                            
                                $pic2 = '<img src="files/stands/'.$data->logo.'" border="0" width="'.$ancho.'" />';
                            }
                            else
                            {
                                $img = getimagesize('assets/frontend/images/imagen-estandar-logo.jpg');
                                $ancho = (int)($img[0]/1);
                                $alto = (int)($img[1]/1);                            
                                $pic2 = '<img src="assets/frontend/images/imagen-estandar-logo.jpg" border="0" width="'.$ancho.'" />';                            

                            }    
                            echo $pic2;
                            ?>
                        
                            </div>
                            <input type="file" name="logo" id="logo">
                            <br>
                            <span class="help-inline red">El logo debe ser de 140px de ancho por 70px de alto.</span>                        
                        </div>
                    </div>                                        

                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">                        
                        <input type="hidden" name="imgatng" value="<?php echo $data->imagen; ?>">                        
                        <input type="hidden" name="logoatng" value="<?php echo $data->logo; ?>">                                                
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_stands/listar">VOLVER AL LISTADO</a>
                        <a data-rel="tooltip" class="btn btn-danger btndelete" data-message="Esta seguro que desea eliminar el stand
                         <?php echo $data->nombre;?>?" data-id="<?php echo $data->id;?>"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_stands/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i>ELIMINAR</a>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

