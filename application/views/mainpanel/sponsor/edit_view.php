

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Sponsor</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_sponsor/listar" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_sponsor/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validarp()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>

                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Título</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="title" name="title" value="<?php echo $data->title?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $data->orden?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado1" value="A" <?php echo ($data->estado=='A')?'checked="checked"':'';?>>
                                Activo
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado2" value="I" <?php echo ($data->estado=='I')?'checked="checked"':'';?>>
                                Inactivo
                            </label>
                        </div>
                    </div>  
                    <hr>                   
                    <div class="control-group">
                        <label class="control-label">Imagen <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                        <div class="controls">
                            <div class="span6"><img src="files/sponsor/<?php echo $data->imagen; ?>" /></div>
                            <input type="file" name="imagen" id="imagen">
                            <br>
                            <span class="help-inline red">La imagen que suba debe ser de 294px de ancho por 190px de alto.</span>                        
                        </div>
                    </div>                     

                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">                        
                        <input type="hidden" name="imgatng" value="<?php echo $data->imagen; ?>">                        
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_sponsor/listar">VOLVER AL LISTADO</a>
                        <a data-rel="tooltip" class="btn btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el sponsor <?php echo $data->title;?>?" data-id="<?php echo $data->id;?>"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_sponsor/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i>ELIMINAR</a>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

