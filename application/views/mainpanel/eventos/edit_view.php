
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Evento</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_eventos/listar" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_eventos/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validarp()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>

                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombre</label>
                        <div class="controls">
                            <input type="text" class="span12 typeahead" id="nombre" name="nombre" value="<?php echo $data->nombre;?>" data-required="true" data-message-required="Se requiere el nombre del evento">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Frase</label>
                        <div class="controls">
                            <input type="text" class="span12 typeahead" id="frase" name="frase" value="<?php echo $data->frase;?>" data-required="true" data-message-required="Se requiere la frase del evento">
                        </div>
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Fecha inicio del banner ?</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="fecha_mostrar" name="fecha_mostrar" value="<?php echo $data->fecha_mostrar;?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto1</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt1" name="txt1" value="<?php echo $data->txt1;?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                        
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto2</label>
                        <div class="controls">
                            <input type="text" class="span12 typeahead" id="txt2" name="txt2" value="<?php echo $data->txt2;?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                            
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Fecha Exacta de inicio</label>
                        <div class="controls">
                            <input type="text" class="span3 typeahead" id="fecha" name="fecha_inicio" value="<?php echo $data->fecha_inicio;?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                        
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombre del Planner</label>
                        <div class="controls">
                            <input type="text" class="span12 typeahead" id="planner" name="planner" value="<?php echo $data->planner;?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Sumilla</label>
                        <div class="controls">
                            <textarea class="span6 typeahead" name="sumilla" id="sumilla" style="width: 600px;height:150px;"><?php echo $data->sumilla;?></textarea>
                        </div>
                    </div>   

                                     
                    <div class="control-group ">
                        <label class="control-label" for="typeahead">Descripci&oacute;n</label>
                        <div class="controls">
                            <textarea class="span6 typeahead" name="descripcion" id="descripcion" style="height:100px"><?php echo $data->descripcion;?></textarea>
                            <script type="text/javascript">
                                CKEDITOR.replace( 'descripcion',{toolbar:'Basic',height:'300px',width:'650px'} );
                            </script>                            
                        </div>
                    </div>  
                     
                    
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado1" value="A" <?php echo ($data->estado=='A')?'checked="checked"':'';?>>
                                Activo
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado2" value="I" <?php echo ($data->estado=='I')?'checked="checked"':'';?>>
                                Inactivo
                            </label>
                        </div>
                    </div> 
                    <hr>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Imagen Planner</label>
                        <div class="controls">
                            <div class="span6"><img src="files/eventos/<?php echo $data->imagen_planner; ?>" /></div>
                            <input type="file" name="imagen_planner" id="imagen_planner">
                            <br>
                            <span class="help-inline red">La imagen que suba debe ser de 150px de ancho por 150px de alto.</span>
                        </div>
                    </div>                     
                    <hr>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Imagen</label>
                        <div class="controls">
                            <div class="span6"><img src="files/eventos/<?php echo $data->imagen; ?>" /></div>
                            <input type="file" name="imagen" id="imagen">
                            <br>
                            <span class="help-inline red">La imagen que suba debe ser de 1920px de ancho por 940px de alto.</span>
                        </div>
                    </div>
                    <hr>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Brochure (PDF)</label>
                        <div class="controls">
                            <div class="span6"><span><?php 
                            if($data->archivo<>''){
                                echo '<img src="assets/frontend/images/pdf_icon.png"> '.$data->archivo;
                            }
                            ?></span></div>
                            <input type="file" name="archivo" id="archivo">
                            <br>
                            <span class="help-inline red">El brochure debe tener formato pdf</span>
                        </div>
                    </div>     
                    <div class="control-group">
                        <label class="control-label">Habilitar brochure ?</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado_brochure" id="estado_brochure1" value="1" <?php echo ($data->estado_brochure=='1')?'checked="checked"':'';?>>
                                SI
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado_brochure" id="estado_brochure2" value="0" <?php echo ($data->estado_brochure=='0')?'checked="checked"':'';?>>
                                NO
                            </label>
                        </div>
                    </div>                                     
                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">                        
                        <input type="hidden" name="imgatng" value="<?php echo $data->imagen; ?>">
                        <input type="hidden" name="archivotng" value="<?php echo $data->archivo; ?>">
                        <input type="hidden" name="imagen_plannerantg" value="<?php echo $data->imagen_planner; ?>">                        
                        
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_eventos/listar">VOLVER AL LISTADO</a>
                        <a data-rel="tooltip" class="btn btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el producto <?php echo $data->nombre;?>?" data-id="<?php echo $data->id;?>"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_eventos/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i>ELIMINAR</a>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<script>
    function validarp(){

        var val = $('select[name=id_categoria_padre]').val();
        if(val==0){
            alert("Debe escoger una subcategoria");
            return false;
        }
    }

    $("input[name=closet_sales]").change(function(){
        var val=$(this).val();
        if(val==1){
            $(".wrSelectCloset").show();
        }else{
            $(".wrSelectCloset").hide();
        }
    });

</script>