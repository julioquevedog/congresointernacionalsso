
    <!-- <ul class="breadcrumb">
        <li><a href="mainpanel/controller_eventos/nuevo">Crear evento</a> </li>
        <span class="divider">/</span><li><a href="mainpanel/controller_eventos/nuevo">Agregar Noticia</a></li>
    </ul> -->
    <a href="mainpanel/controller_eventos/nuevo" class="btn btn-mini btn-success">Crear evento</a>

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Lista de eventos</h2>
            <div class="box-icon">

            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="10%">Imagen</th>                        
                        <th width="20%">Nombre</th>                                                
                        <th width="20%">Lema</th>
                        <th width="15%">Inicio</th>                        
                        <th width="15%">Planner</th>                        
                        <th width="5%">Estado</th>
                        <th width="10%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {
                        if(is_file('files/eventos/'.$value->imagen))
                        {
                            $img = getimagesize('files/eventos/'.$value->imagen);
                            $ancho = (int)($img[0]/4);
                            $alto = (int)($img[1]/4);                            
                            $pic = '<img src="files/eventos/'.$value->imagen.'" border="0" width="'.$ancho.'" />';
                        }
                        else
                        {
                            $pic='foto';
                        }   

                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$pic.'</td>';                        
                        echo '<td>'.$value->nombre.'</td>';
                        echo '<td>'.$value->frase.'</td>';                        
                        echo '<td class="center">'.$value->fecha_mostrar.'</td>';
                        echo '<td class="center">'.$value->planner.'</td>';
                        echo '<td class="center">'.$value->estado.'</td>';
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_eventos/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el evento '.$value->nombre.'?" data-id="'.$value->id.'"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_eventos/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_programacion/listar/'.$value->id.'" title="programación"><i class="icon-calendar icon-white"></i></a> ';                        
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->