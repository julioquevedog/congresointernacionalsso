    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-plus"></i> Crear evento
            </h2>
            <div class="box-icon">
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_eventos/grabar" method="post" enctype="multipart/form-data" 
                  onsubmit="return validar()">
                <fieldset>
                    <legend>Ingrese los datos</legend>
                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>

                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombre</label>
                        <div class="controls">
                            <input type="text" class="span12 typeahead" id="nombre" name="nombre" value="" data-required="true" data-message-required="Se requiere el nombre del evento">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Frase</label>
                        <div class="controls">
                            <input type="text" class="span12 typeahead" id="frase" name="frase" value="" data-required="true" data-message-required="Se requiere la frase del evento">
                        </div>
                    </div>    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto cuando inicia ?</label>
                        <div class="controls">
                            <input type="text" class="span12 typeahead" id="fecha_mostrar" name="fecha_mostrar" value="" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Fecha Exacta de inicio</label>
                        <div class="controls">
                            <input type="text" class="span3 typeahead" id="fecha" name="fecha_inicio" value="0000-00-00" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                                                  
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombre del Planner</label>
                        <div class="controls">
                            <input type="text" class="span12 typeahead" id="planner" name="planner" value="" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Sumilla</label>
                        <div class="controls">
                            <textarea class="span6 typeahead" name="sumilla" id="sumilla"></textarea>
                        </div>
                    </div>   

                    <div class="control-group ">
                        <label class="control-label" for="typeahead">Descripci&oacute;n</label>
                        <div class="controls">
                            <textarea class="span6 typeahead" name="descripcion" id="descripcion" style="height:100px"></textarea>
                            <script>
                                CKEDITOR.replace( 'descripcion',{toolbar:'Basic',height:'300px',width:'650px'} );                             
                            </script>
                        </div>
                    </div>  

                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="estado" id="estado1" value="A" checked="checked">
                                Activo
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="estado" id="estado2" value="I">
                                Inactivo
                            </label>
                        </div>
                    </div> 
                    <hr>                   
                    <div class="control-group">
	                    <label class="control-label">Imagen del Planner <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                        <div class="controls">
                          <input type="file" name="imagen_planner" id="imagen_planner">
                          <br>
                          <span class="help-inline red">La imagen que suba será reducida a 150px de ancho por 150px de alto..</span>
                        </div>
                    </div>                    
                    <hr>                   
                    <div class="control-group">
	                    <label class="control-label">Subir Imagen <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                        <div class="controls">
                          <input type="file" name="imagen" id="imagen">
                          <br>
                          <span class="help-inline red">La imagen que suba debe ser de 1920px de ancho por 940px de alto..</span>
                        </div>
                    </div>
                    <hr>                   
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Brochure (PDF) <span class="help-inline red"><b>(Obligatorio)</b></span></label>
                        <div class="controls">
                            <div class="span6"></div>
                            <input type="file" name="archivo" id="archivo">
                            <br>
                            <span class="help-inline red">El brochure debe tener formato pdf </span>
                        </div>
                    </div>                    
                    <div class="form-actions">
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_eventos/listar">VOLVER AL LISTADO</a>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->



