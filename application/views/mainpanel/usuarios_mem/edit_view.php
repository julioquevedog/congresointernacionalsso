

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Usuario para membresía</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_usuarios_mem/listar" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_usuarios_mem/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validarp()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>

                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombres</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="nombres" name="nombres" value="<?php echo $data->nombres?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Apellidos</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="apellidos" name="apellidos" value="<?php echo $data->apellidos?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>   
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Correo electrónico</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="correo" name="correo" value="<?php echo $data->correo;?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Password</label>
                        <div class="controls">
                            <input type="text" class="span4 typeahead" id="password" name="password" value="<?php echo $data->password?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                       
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Fecha inicio:</label>
                        <div class="controls">
                            <input type="text" class="span3 typeahead" id="fecha2" name="fecha_ini" value="<?php echo $data->fecha_ini?>" data-required="true" data-message-required="Se requiere" autocomplete="off">
                        </div>
                    </div>                                       
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Fecha fin:</label>
                        <div class="controls">
                            <input type="text" class="span3 typeahead" id="fecha" name="fecha_fin" value="<?php echo $data->fecha_fin?>" data-required="true" data-message-required="Se requiere" autocomplete="off">
                        </div>
                    </div>                                                           

                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado1" value="A" <?php echo ($data->estado=='A')?'checked="checked"':'';?>>
                                Activo
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado2" value="I" <?php echo ($data->estado=='I')?'checked="checked"':'';?>>
                                Inactivo
                            </label>
                        </div>
                    </div>  
                    
                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">                        
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_usuarios_mem/listar">VOLVER AL LISTADO</a>
                        <a data-rel="tooltip" class="btn btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        al usuario <?php echo $data->nombres.' '.$data->apellidos;?>?" data-id="<?php echo $data->id;?>"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_usuarios_mem/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i>ELIMINAR</a>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

