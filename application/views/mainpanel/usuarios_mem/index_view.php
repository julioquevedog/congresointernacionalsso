<a href="mainpanel/controller_usuarios_mem/nuevo" class="btn btn-mini btn-success">Crear Usuario</a>

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> MEMBRESÍA</h2>
            <div class="box-icon">
            
            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="10%">Nombres</th>
                        <th width="15%">Apellidos</th>                                                
                        <th width="15%">Correo</th>                        
                        <th width="15%">Estado</th>                        
                        <th width="10%">Vigencia</th>
                        <th width="10%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {
                        $estado = ($value->estado=='A')? '<span class="label label-success">Activo</span>':'<span class="label label-important">Inactivo</span>';

                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$value->nombres.'</td>';
                        echo '<td>'.$value->apellidos.'</td>';                        
                        echo '<td>'.$value->correo.'</td>';
                        echo '<td class="center">'.$estado.'</td>';
                        echo '<td class="center">'.$value->fecha_ini.' entre '.$value->fecha_fin.'</td>';                        
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_usuarios_mem/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el USUARIO '.$value->nombres.' '.$value->apellidos.'?" data-id="'.$value->id.'"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_usuarios_mem/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a>';
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->