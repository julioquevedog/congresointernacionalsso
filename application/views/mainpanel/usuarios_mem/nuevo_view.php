
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-plus"></i> Crear Usuario para membresía
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="mainpanel/controller_usuarios_mem/grabar" method="post" enctype="multipart/form-data" 
                    onsubmit="return validar()">
                    <fieldset>
                        <legend>Ingrese los datos</legend>
                        <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        } 
                        ?>

                                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Nombres</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="nombres" name="nombres" value="" data-required="true" data-message-required="Se requiere" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Apellidos</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="apellidos" name="apellidos" value="" data-required="true" data-message-required="Se requiere" required>
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Correo electrónico</label>
                            <div class="controls">
                                <input type="email" class="span6 typeahead" id="correo" name="correo" value="" data-required="true" data-message-required="Se requiere" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Password</label>
                            <div class="controls">
                                <input type="text" class="span4 typeahead" id="password" name="password" value="" data-required="true" data-message-required="Se requiere" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Fecha inicio:</label>
                            <div class="controls">
                                <input type="text" class="span3 typeahead" id="fecha" name="fecha_ini" value="" data-required="true" data-message-required="Se requiere" autocomplete="off" required>
                            </div>
                        </div>   
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Fecha fin:</label>
                            <div class="controls">
                                <input type="text" class="span3 typeahead" id="fecha2" name="fecha_fin" value="" data-required="true" data-message-required="Se requiere" autocomplete="off" required>
                            </div>
                        </div>                                              
                        <div class="control-group">
                            <label class="control-label">Estado</label>
                            <div class="controls">
                                <label class="radio inline">
                                    <input type="radio" name="estado" id="estado1" value="A" checked="checked">
                                    Activo
                                </label>
                                
                                <label class="radio inline">
                                    <input type="radio" name="estado" id="estado2" value="I">
                                    Inactivo
                                </label>
                            </div>
                        </div>  
                

                        <div class="form-actions">
                            <input type="submit" class="btn btn-primary" value="GRABAR">
                            &nbsp;&nbsp;
                            <a class="btn btn-danger" href="mainpanel/controller_usuarios_mem/listar">VOLVER AL LISTADO</a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
    </div>


