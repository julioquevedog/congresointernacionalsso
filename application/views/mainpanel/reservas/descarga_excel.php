<?php
/*
|--------------------------------------------------------------------------
| Estilos
|--------------------------------------------------------------------------
*/     

$font14='font-size:14px;';
$backgnone='background:none;';
$border='border: 100% solid #fff';
/*
|--------------------------------------------------------------------------
*/   
$timestamp=time();
        
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition:attachment; filename=".$timestamp.".xls");

    echo '<body style="'.$border.'">';
	echo '<table width="190mm" cellpadding="0" cellspacing="1"  style="'.$font14.'" border="0">';
	echo '<thead>';
	echo '<tr height="20">';
            echo '<td align="center" valign="middle">Item</td>';                
            echo '<td align="center" valign="middle">Nombres</td>';
            echo '<td align="center" valign="middle">Email</td>';                                
            echo '<td align="center" valign="middle">'.utf8_decode('Teléfono').'</td>';
            echo '<td align="center" valign="middle">Interes</td>';                                            
            echo '<td align="center" valign="middle">Realizado</td>';

	echo '</tr>';
	echo '</thead>';
        

        $num=count($data);
        
        if($num>0){
            $f=0;
            echo '<tbody>';
            foreach ($data as $value) {

                $names                      = $value->names;                
                $email                      = $value->email;                                
                $phone                      = $value->phone;
                $interes                    = $value->texto_precio;
                $fecha_insert               = $value->fecha_insert;
                $f++;
                echo '<tr height="17">';
                echo '<td align="center">'.$f.'</td>';
                echo '<td align="left">'.utf8_decode($names).'</td>';                
                echo '<td align="left">'.$email.'</td>';
                ?>
                <td style="mso-number-format:'@'"><?php echo $phone;?></td>
                <?php
                echo '<td align="left">'.$interes.'</td>';                                
                ?>
                <td align="center" valign="center" style="mso-number-format:'@'"><?php echo $fecha_insert; ?></td>                
                 <?php
                echo '</tr>';                            
            }
            echo '</tbody>';
        }
        echo '</table>';
        echo '</body>';

        ?>