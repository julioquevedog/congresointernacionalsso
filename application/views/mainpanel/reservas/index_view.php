<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Reservas por la web</h2>
            <div class="box-icon">
            <a href="mainpanel/controller_reservas/descargar_reserva" class="btn btn-round" title="VOLVER"><i class="icon-download"></i></a>  
            </div>
        </div>
        <div class="box-content">
            <div class="table-responsive">
                <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                ?>              
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th width="5%">Nro</th>
                            <th width="25%">Nombre</th>                                                
                            <th width="25%">Email</th>
                            <th width="15%">Teléfono</th>
                            <th width="10%">Plan</th>
                            <th width="10%">Registrado</th>                        
                            <th width="10%">Acción</th>
                        </tr>
                    </thead>   
                    <tbody>
                    <?php
                        
                        
                        foreach ($data as $key => $value) {
                            echo '<tr>';
                            echo '<td class="center">'.($key+1).'</td>';
                            echo '<td>'.$value->names.'</td>';                        
                            echo '<td>'.$value->email.'</td>';                                                
                            echo '<td class="center">'.$value->phone.'</td>';
                            echo '<td class="center">'.$value->texto_precio.'</td>';
                            echo '<td class="center">'.$value->fecha_insert.'</td>';                        
                            echo '<td class="center">';
                            echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                            la reserva de '.$value->names.'?" data-id="'.$value->id.'"
                            data-elementos-dependientes="0" data-url="mainpanel/controller_reservas/delete" 
                            href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                            echo '</td>';
                            echo '</tr>';
                            
                        }
                    ?>
                    </tbody>
                </table>            
            </div>
        </div>
     </div><!--/span-->
</div><!--/row-->