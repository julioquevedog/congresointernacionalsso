<div>
    <ul class="breadcrumb">
        <li><a href="mainpanel/banners">Lista de Banners</a> <span class="divider">/</span></li>
        <li><a href="mainpanel/banners/nuevo">Nuevo Banner</a></li>
    </ul>
</div>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Nuevo Banner</h2>
            <div class="box-icon">
                <a href="javascript:history.back(-1)" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                                                
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/banners/grabar" method="post" enctype="multipart/form-data" onsubmit="return valida_banner()">
                <fieldset>
                    <legend>Ingrese los datos del nuevo banner</legend>
                    <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        } 
                    ?>       
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Titulo</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="title" name="title" value="" >
                        </div>
                    </div>                                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link" name="link" value="" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span1 typeahead" id="orden" name="orden" value="<?php echo $ultimo;?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 1</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt1" name="txt1" value="" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 2</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt2" name="txt2" value="" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 3</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt3" name="txt3" value="" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 4</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt4" name="txt4" value="" >
                        </div>
                    </div>       
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 5</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt5" name="txt5" value="" >
                        </div>
                    </div>       
                    <div class="control-group">
                        <label class="control-label">Mostrar textos ?</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="hidetxt" id="hidetxt1" value="0">
                                No
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="hidetxt" id="hidetxt2" value="1" checked="checked">
                                Si
                            </label>
                        </div>
                    </div>                                                                                                              
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="estado" id="estado1" value="A" checked="checked">
                                Activo
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="estado" id="estado2" value="I">
                                Inactivo
                            </label>
                        </div>
                    </div>     
                    <div class="control-group">
                        <label class="control-label">Tipo de banner</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="type" id="type1" value="0" checked="checked">
                                Una Imagen
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="type" id="type2" value="1">
                                Dos imagenes
                            </label>
                        </div>
                    </div>                        
                    <div class="control-group img1">
                    <label class="control-label">Imagen (1)</label>
                        <div class="controls">
                          <input type="file" name="imagen" id="imagen">
                          <div class="alert alert-block ">
                          <p>La imagen a subir debe tener 1920px de ancho y 879px de alto. Caso contrario la imagen se forzará al tamaño indicado.</p>                            </div>                           
                        </div>
                    </div>
                    <div class="control-group img2" style="display:none">
                    <label class="control-label">Imagen (2)</label>
                        <div class="controls">
                          <input type="file" name="imagen2" id="imagen2">
                          <div class="alert alert-block ">
                            <p>La imagen a subir debe tener 750px de ancho y 900px de alto. Caso contrario la imagen se forzará al tamaño indicado.</p>                            </div>                           
                        </div>
                    </div>                    
                    <div class="form-actions">
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<script>
$("input[name=type]").click(function(){
    var val=$(this).val();
    if(val==1){
        $(".img2").fadeIn(500);
    }else{
        $(".img2").fadeOut(500);        
    }
});
</script>