<div>
    <ul class="breadcrumb">
        <li><a href="mainpanel/banners">Lista de Banners</a> <span class="divider">/</span></li>
        <li><a href="mainpanel/banners/nuevo">Nuevo Banner</a></li>
    </ul>
</div>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Banner</h2>
            <div class="box-icon">
                <a href="javascript:history.back(-1)" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                                                
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/banners/actualizar" method="post" enctype="multipart/form-data" onsubmit="return valida_banner()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>
                    <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        }                        
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Titulo</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="title" name="title" value="<?php echo $banner->title; ?>" >
                        </div>
                    </div>                    
                    <div class="control-group">

                        <label class="control-label" for="typeahead">Link</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link" name="link" value="<?php echo $banner->link; ?>" >
                        </div>
                    </div>                    

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span1 typeahead" id="orden" name="orden" value="<?php echo $banner->orden; ?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 1</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt1" name="txt1" value="<?php echo $banner->txt1; ?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 2</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt2" name="txt2" value="<?php echo $banner->txt2; ?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 3</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt3" name="txt3" value="<?php echo $banner->txt3; ?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 4</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt4" name="txt4" value="<?php echo $banner->txt4; ?>" >
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Texto 5</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="txt5" name="txt5" value="<?php echo $banner->txt5; ?>" >
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label">Mostrar textos ?</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="hidetxt" id="hidetxt1" value="0"<?php if($banner->hidetxt=="0") echo ' checked="checked"'; ?>>
                                No
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="hidetxt" id="hidetxt2" value="1"<?php if($banner->hidetxt=="1") echo ' checked="checked"'; ?>>
                                Si
                            </label>
                        </div>
                    </div>                                                                                    
                                        
                    <div class="control-group">
                        <label class="control-label">Tipo de banner</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="type" id="type1" value="0"<?php if($banner->type=="0") echo ' checked="checked"'; ?>>
                                Una Imagen
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="type" id="type2" value="1"<?php if($banner->type=="1") echo ' checked="checked"'; ?>>
                                Dos imagenes
                            </label>
                        </div>
                    </div>                                                                
                    <div class="control-group img1">
                        <label class="control-label" for="typeahead">Imagen(1)</label>
                        <div class="controls">
                            <div class="span6"><img src="files/banners/<?php echo $banner->imagen; ?>" width="300"/></div>
                        </div>
                    </div>
                    <div class="control-group img2" <?php if($banner->type=="0") echo ' style="display:none"'; ?>>
                        <label class="control-label" for="typeahead">Imagen(2)</label>
                        <div class="controls">
                            <?php if($banner->imagen2<>''){ ?> 
                            <div class="span6"><img src="files/banners/<?php echo $banner->imagen2; ?>" width="300"/></div>
                            <?php }else{
                                echo '<p>Aun no ha cargado la Imagen (2)</p>';
                            }?>
                        </div>
                    </div>  

                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="estado" id="estado1" value="A"<?php if($banner->estado=="A") echo ' checked="checked"'; ?>>
                                Activo
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="estado" id="estado2" value="I"<?php if($banner->estado=="I") echo ' checked="checked"'; ?>>
                                Inactivo
                            </label>
                        </div>
                    </div>  

                    <div class="control-group">
                        <label class="control-label">Cambiar imagen (1)</label>
                        <div class="controls">
                            <input type="file" name="imagen">
                            <div class="alert alert-block ">
                            <p>La imagen a subir debe tener 1920px de ancho y 879px de alto. Caso contrario la imagen se forzará al tamaño indicado.</p>
                            </div>                             
                        </div>
                    </div>
                    <div class="control-group img2" <?php if($banner->type=="0") echo ' style="display:none"'; ?>>
                        <label class="control-label">Cambiar imagen (2)</label>
                        <div class="controls">
                            <input type="file" name="imagen2">
                            <div class="alert alert-block ">
                            <p>La imagen a subir debe tener 750px de ancho y 900px de alto. Caso contrario la imagen se forzará al tamaño indicado.</p>
                            </div>                             
                        </div>
                    </div>                    
                    <div class="form-actions">
                        <input type="hidden" name="idbanner" id="idbanner" value="<?php echo $banner->idbanner; ?>">
                        <input type="hidden" name="imgatng" id="imgatng" value="<?php echo $banner->imagen; ?>">                        
                        <input type="hidden" name="imgatng2" id="imgatng2" value="<?php echo $banner->imagen2; ?>">
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<script>
$("input[name=type]").click(function(){
    var val=$(this).val();
    if(val==1){
        $(".img2").fadeIn(500);
    }else{
        $(".img2").fadeOut(500);        
    }
});
</script>