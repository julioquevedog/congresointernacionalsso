<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Mapa</h2>
            <div class="box-icon">
                <a href="javascript:history.back(-1)" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                                                
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
            </div>
        </div>
        <?php //var_dump($sede);?>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/mapa/actualizar" method="post" enctype="multipart/form-data" onsubmit="return valida_banner()">
                <fieldset>
                    <legend>Usted puede modificar la ubicaci&oacute;n geogr&aacute;fica</legend>
                    <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        }                        
                    ?>
                    <div class="control-group">

                        <label class="control-label" for="typeahead"></label>
                        <div class="controls">
                            <div id="mapa" style="width: 350px; height: 400px; border: #000 solid 1px;"></div>
                        </div>
                    </div>

                    <div class="form-actions">
                      
                        <input type="hidden" name="latitud_centro" id="latitud_centro" value="<?php echo $sede['latitud_centro']; ?>">
                        <input type="hidden" name="longitud_centro" id="longitud_centro" value="<?php echo $sede['longitud_centro']; ?>">
                        <input type="hidden" name="latitud_punto" id="latitud_punto" value="<?php echo $sede['latitud_punto']; ?>">
                        <input type="hidden" name="longitud_punto" id="longitud_punto" value="<?php echo $sede['longitud_punto']; ?>">                        
                        <input type="hidden" name="zoom" id="zoom" value="<?php echo $sede['zoom']; ?>">                        
                        <input type="hidden" name="tipomapa" id="tipomapa" value="<?php echo $sede['tipomapa']; ?>">                                                
                        <input type="hidden" name="id_sede" id="id_sede" value="<?php echo $sede['id_sede']; ?>">                                                                        
                        
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->