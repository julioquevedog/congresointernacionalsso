<style>
.accordionjq{
    position: relative;
}

.submenujq{
    list-style: square;
    display: none;
}

.submenujq li{
    border-bottom: solid 1px #c0c0c0;
}
.submenujq li a{
    display: block;
    padding: 5px;
    color: #616161 !important;
    text-decoration: none;
    font-size: 13px;
    font-weight: bold;    
}
.icon-arrowL {
    background-position: -460px -73px;
    position: absolute;
    top: 13px;
    right: 10px;    
}
.icon-arrowB {
    background-position: -460px -73px;
    position: absolute;
    top: 13px;
    right: 10px;    
}
</style>



<ul class="nav nav-tabs nav-stacked main-menu">
    <li class="nav-header hidden-tablet">SECCIONES</li>
    <li class="<?php if($current_section=='inicio'){echo 'active';}?>" ><a class="ajax-link" href="mainpanel"><i class="icon-home"></i><span class="hidden-tablet"> Inicio</span></a></li>    
    <li class=" <?php if($current_section=='txt_galeria'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_galeria"><i class="icon-th"></i><span class="hidden-tablet"> Texto fotografías</span></a></li>     
    <li class=" <?php if($current_section=='fotos'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_album/listar"><i class="icon-th"></i><span class="hidden-tablet"> Galería de fotos</span></a></li>
    <li class=" <?php if($current_section=='txt_conocenos'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_conocenos"><i class="icon-th"></i><span class="hidden-tablet"> Texto organización</span></a></li>    
    <li class=" <?php if($current_section=='txt_time_rever'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_time_rever"><i class="icon-th"></i><span class="hidden-tablet"> Texto contador evento</span></a></li>        
    <li class=" <?php if($current_section=='txt_evento'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_evento"><i class="icon-th"></i><span class="hidden-tablet"> Texto evento</span></a></li>        
    <li class=" <?php if($current_section=='txt_ponente'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_ponente"><i class="icon-th"></i><span class="hidden-tablet"> Texto ponentes</span></a></li> 
    <li class=" <?php if($current_section=='txt_video'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_video"><i class="icon-th"></i><span class="hidden-tablet"> Texto videos</span></a></li> 
    <li class=" <?php if($current_section=='txt_testimonios'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_testimonios"><i class="icon-th"></i><span class="hidden-tablet"> Texto testimonios</span></a></li>                
    <li class=" <?php if($current_section=='txt_mapa'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_mapa"><i class="icon-th"></i><span class="hidden-tablet"> Texto Mapa</span></a></li>              
    <li class=" <?php if($current_section=='txt_register_form'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_register_form"><i class="icon-th"></i><span class="hidden-tablet"> Texto reg. form.</span></a></li>                  
    <li class="<?php if($current_section=='eventos'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_eventos/listar"><i class="icon-th"></i><span class="hidden-tablet"> Eventos</span></a></li>	
    <li class="<?php if($current_section=='ponentes'){echo 'active';}?>"><a class="ajax-link"href="mainpanel/controller_ponentes/listar"><i class="icon-th"></i><span class="hidden-tablet"> Ponentes</span></a></li>    
    <li class="<?php if($current_section=='stands'){echo 'active';}?>"><a class="ajax-link"href="mainpanel/controller_stands/listar"><i class="icon-th"></i><span class="hidden-tablet"> Stands</span></a></li>        
    <li class="<?php if($current_section=='testimonios'){echo 'active';}?>"><a class="ajax-link"href="mainpanel/controller_testimonios/listar"><i class="icon-th"></i><span class="hidden-tablet"> Testimonios</span></a></li>        
    <li class=" <?php if($current_section=='txt_faq'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_faq"><i class="icon-th"></i><span class="hidden-tablet"> Texto FAQ</span></a></li>      
    <li class="<?php if($current_section=='faq'){echo 'active';}?>"><a class="ajax-link"href="mainpanel/controller_faqs/listar"><i class="icon-th"></i><span class="hidden-tablet"> FAQ</span></a></li>                      
    <li class=" <?php if($current_section=='txt_has_pregunta'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_has_pregunta"><i class="icon-th"></i><span class="hidden-tablet"> Texto has tu pregunta</span></a></li>          
    <li class=" <?php if($current_section=='txt_sponsor'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_sponsor"><i class="icon-th"></i><span class="hidden-tablet"> Texto Sponsor</span></a></li>              
    <li class=" <?php if($current_section=='txt_precios'){echo 'active';}?>"><a class="ajax-link" href="mainpanel/controller_informativa/edit/txt_precios"><i class="icon-th"></i><span class="hidden-tablet"> Texto precios</span></a></li>
    <li class="<?php if($current_section=='precios'){echo 'active';}?>"><a class="ajax-link"href="mainpanel/controller_precios/listar"><i class="icon-th"></i><span class="hidden-tablet"> Precios</span></a></li>            
    <li class="<?php if($current_section=='videos'){echo 'active';}?>"><a class="ajax-link"href="mainpanel/controller_videos/listar"><i class="icon-th"></i><span class="hidden-tablet"> Videos</span></a></li>
    <li class="<?php if($current_section=='usuarios_mem'){echo 'active';}?>"><a class="ajax-link"href="mainpanel/controller_usuarios_mem/listar"><i class="icon-th"></i><span class="hidden-tablet"> Membresia</span></a></li>
    <li class="<?php if($current_section=='sponsor'){echo 'active';}?>" data-id-menu="2"><a class="ajax-link" href="mainpanel/controller_sponsor/listar"><i class="icon-align-center"></i><span class="hidden-tablet"> Sponsor</span></a></li>        
    <li <?php if($current_section=='mapa'){echo 'class="active"';}?>><a class="ajax-link" href="mainpanel/mapa"><i class="icon-globe"></i><span class="hidden-tablet"> Mapa evento</span></a></li>
    <li <?php if($current_section=='configuracion'){echo 'class="active"';}?>><a class="ajax-link" href="mainpanel/configuracion/listado"><i class="icon-cog"></i><span class="hidden-tablet"> Parámetros</span></a></li> 
    <li <?php if($current_section=='reservas'){echo 'class="active"';}?>><a class="ajax-link" href="mainpanel/controller_reservas/listar"><i class="icon-eye-open"></i><span class="hidden-tablet"> Mensajes</span></a></li>
    <li <?php if($current_section=='mis-datos'){echo 'class="active"';}?>><a class="ajax-link" href="mainpanel/mis-datos"><i class="icon-user"></i><span class="hidden-tablet"> Mis datos</span></a></li>            
</ul>

<script>
$(document).ready(function(){
    if(sessionStorage.getItem("id_menu")){
        $(".accordionjq").each(function(){
            if($(this).data('id-menu')==sessionStorage.getItem("id_menu")){
                $(this).click();
            }
        })
    }
})

$(".accordionjq ").on('click',function(e){
    $(this).find(".submenujq").slideToggle( "slow", function() {
     $(".main-menu").find("li").removeClass('active');
     $(this).parent().addClass('active');
    });
    
    var id_menu=$(this).data('id-menu');
    sessionStorage.setItem("id_menu",id_menu);
    
});

$(".main-menu > li").on('click',function(){
    if(!$(this).hasClass('accordionjq')){sessionStorage.removeItem("id_menu");}
})

</script>