		<!-- content ends -->
			</div><!--/#content.span10-->
		</div><!--/fluid-row-->
		<hr>

                <div class="modal hide fade modal700" id="myModal">
                <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h3 id="tituloModal"></h3>
                </div>
                <div class="modal-body" id="cuerpoModal">
                </div>
                <div class="modal-footer" id="botoneraModal">
                        
                </div>
                </div>


		<footer>
                    <p class="pull-left">&copy; <a href="<?php echo base_url()?>" target="_blank">Congreso Internacional SSO</a> <?php echo date('Y') ?>. Todos los derechos reservados</p>
                    <p class="pull-right">Desarrollado por Peru Web Pages</p>
		</footer>

	</div><!--/.fluid-container-->
<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->


	<!-- <script src="assets/mainpanel/charisma/js/index_ordenar.js"></script>  	 -->
	<script src="assets/mainpanel/charisma/js/jquery-ui-1.8.12.custom.min.js"></script>          
	<!-- transition / effect library -->
	<script src="assets/mainpanel/charisma/js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="assets/mainpanel/charisma/js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="assets/mainpanel/charisma/js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="assets/mainpanel/charisma/js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="assets/mainpanel/charisma/js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="assets/mainpanel/charisma/js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="assets/mainpanel/charisma/js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="assets/mainpanel/charisma/js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="assets/mainpanel/charisma/js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="assets/mainpanel/charisma/js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="assets/mainpanel/charisma/js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="assets/mainpanel/charisma/js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="assets/mainpanel/charisma/js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script src="assets/mainpanel/charisma/js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='assets/mainpanel/charisma/js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='assets/mainpanel/charisma/js/jquery.dataTables.min.js'></script>

	<!-- chart libraries start -->
	<script src="assets/mainpanel/charisma/js/excanvas.js"></script>
	<script src="assets/mainpanel/charisma/js/jquery.flot.min.js"></script>
	<script src="assets/mainpanel/charisma/js/jquery.flot.pie.min.js"></script>
	<script src="assets/mainpanel/charisma/js/jquery.flot.stack.js"></script>
	<script src="assets/mainpanel/charisma/js/jquery.flot.resize.min.js"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="assets/mainpanel/charisma/js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="assets/mainpanel/charisma/js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="assets/mainpanel/charisma/js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="assets/mainpanel/charisma/js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="assets/mainpanel/charisma/js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="assets/mainpanel/charisma/js/jquery.elfinder.min.js"></script>
	<!-- star rating plugin -->
	<script src="assets/mainpanel/charisma/js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="assets/mainpanel/charisma/js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="assets/mainpanel/charisma/js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="assets/mainpanel/charisma/js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="assets/mainpanel/charisma/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="assets/mainpanel/charisma/js/charisma.js"></script>
  
	        
        <script src="assets/mainpanel/charisma/js/generales_js.js"></script>  		
        <script src="assets/mainpanel/charisma/js/funciones_ajax.js"></script>    
        
        <script type="text/javascript"> 
        function initialize() { 
                var latlng = new google.maps.LatLng(<?php echo $sede['latitud_centro']; ?>, <?php echo $sede['longitud_centro']; ?>); 
                var myOptions = { 
                        zoom: <?php echo $sede['zoom']; ?>, 
                        center: latlng, 
                        <?php
                                switch($sede['tipomapa'])
                                {
                                        case "roadmap":	
                        ?>
                                        mapTypeId: google.maps.MapTypeId.ROADMAP
                        <?php
                                        break;

                                        case "hybrid":
                        ?>
                                        mapTypeId: google.maps.MapTypeId.HYBRID
                        <?php				
                                        break;
                                }
                        ?>
                }; 
                var map = new google.maps.Map(document.getElementById("mapa"), myOptions); 
                var myOffice = new google.maps.LatLng(<?php echo $sede['latitud_punto']; ?>, <?php echo $sede['longitud_punto']; ?>); 
                var marker = new google.maps.Marker({ 
                        position: myOffice, 
                        draggable: true,
                        map: map, 
                        title:"<?php echo $sede['titulo_globo']; ?>" 
                }); 
                var infowindow = new google.maps.InfoWindow({ 
                        content: "<?php echo $sede['texto_globo']; ?>", 
                        size: new google.maps.Size(250,120) 
                }); 
                google.maps.event.addListener(marker, 'click', function() { 
                        infowindow.open(map,marker); 
                }); 
                google.maps.event.addListener(map, 'dragend', function() { 
                        var center = map.getCenter();
                        var texto_globoes = center.toString();
                        //alert(texto_globoes);
                        var latitud = extrae_latitud(texto_globoes);
                        var longitud = extrae_longitud(texto_globoes);
                        $("#latitud_centro").val(latitud);
                        $("#longitud_centro").val(longitud);
                }); 

                google.maps.event.addListener(map, 'zoom_changed', function() {
                        var newzoom = map.getZoom();
                        //alert('el zoom cambio:'+newzoom);
                        //document.nueva.zoom.value = newzoom;
                        $("#zoom").val(newzoom);
                });

                google.maps.event.addListener(map, 'maptypeid_changed', function() {
                        var tipomapa = map.getMapTypeId();
                        //alert('tipo de mapa:'+tipomapa);
                        $("#tipomapa").val(tipomapa);
                });	

                google.maps.event.addListener(marker, 'dragend', function() { 
                        var posicion = marker.getPosition();
                        var ubicacion = posicion.toString();
                        //alert(ubicacion);
                        var latitud2 = extrae_latitud(ubicacion);
                        var longitud2 = extrae_longitud(ubicacion);
                        $("#latitud_punto").val(latitud2);
                        $("#longitud_punto").val(longitud2);
                        //document.nueva.latitud_punto.value = latitud2;
                        //document.nueva.longitud_punto.value = longitud2;
                }); 
        } 
        </script>  
        
        <script type="text/javascript">
            function extrae_latitud(texto_globoes) {				
                    var aux = texto_globoes.split(",");
                    var aux2 = aux[0].split("(");
                    var latitud = aux2[1];
                    return latitud;		
            }

            function extrae_longitud(texto_globoes) {
                    var aux = texto_globoes.split(",");
                    var aux3 = aux[1].split(")");
                    var longitud = aux3[0];
                    return longitud;			
            }
        </script>      
        
        
        <script src="assets/mainpanel/charisma/js/jquery.datetimepicker.js"></script>     
        <script>
            $('#fecha,#fecha2').datetimepicker({
                    lang: 'de',
                    i18n: {
                        de: {
                            months: [
                                'Enero', 'Febrero', 'Marzo', 'Abril',
                                'Mayo', 'Junio', 'Julio', 'Agosto',
                                'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                            ],
                            dayOfWeek: [
                                "Dom", "Lun", "Mar", "Mie", "Jue",
                                "Vie", "Sab",
                            ]
                        }
                    },
                    timepicker:false,
                    format:'Y-m-d',
                    formatDate:'Y-m-d'

                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
            });

            $('#fechayhora').datetimepicker({
                    lang: 'de',
                    i18n: {
                        de: {
                            months: [
                                'Enero', 'Febrero', 'Marzo', 'Abril',
                                'Mayo', 'Junio', 'Julio', 'Agosto',
                                'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                            ],
                            dayOfWeek: [
                                "Dom", "Lun", "Mar", "Mie", "Jue",
                                "Vie", "Sab",
                            ]
                        }
                    },
                    timepicker:true,
                    format:'d-m-Y H:m:s',
                    formatDate:'d-m-Y H:m:s'


                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
            });    
            
            $('#fecha_inicio,#fecha_fin').datetimepicker({
                    lang: 'de',
                    i18n: {
                        de: {
                            months: [
                                'Enero', 'Febrero', 'Marzo', 'Abril',
                                'Mayo', 'Junio', 'Julio', 'Agosto',
                                'Septiembre', 'Octubre', 'Noviembre', 'Diciembre',
                            ],
                            dayOfWeek: [
                                "Dom", "Lun", "Mar", "Mie", "Jue",
                                "Vie", "Sab",
                            ]
                        }
                    },
                    timepicker:true,
                    format:'Y-m-d H:m:s',
                    formatDate:'Y-m-d H:m:s'


                    //minDate:'-1970/01/02', // yesterday is minimum date
                    //maxDate:'+1970/01/02' // and tommorow is maximum date calendar
            });              
            
        </script>        
</body>
</html> 