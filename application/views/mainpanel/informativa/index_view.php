
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar sección</h2>
            <div class="box-icon">
                <a href="javascript:history.back(-1)" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                                                
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_informativa/actualizar" method="post" onsubmit="return valida_informativa()" enctype="multipart/form-data">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>
                    <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        }  
                    
                    if($data->seccion<>'txt_testimonios'){
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Título</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="titulo" name="titulo" value="<?php echo $data->titulo; ?>" >
                        </div>
                    </div> 
                    <?php
                    }
                    if($data->seccion=='txt_conocenos' || $seccion=='txt_evento' || $seccion=='txt_ponente' || $seccion=='txt_faq' || $seccion=='txt_precios' || $seccion=='txt_galeria' || $seccion=='txt_sponsor' || $seccion=='txt_mapa' || $seccion=='txt_register_form'  || $seccion=='txt_video'){
                        ?>                      
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Texto</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="txt1" name="txt1" value="<?php echo $data->txt1; ?>" >
                            </div>
                        </div>      
                    <?php
                    }
                    if($seccion=='txt_evento'){
                        ?>                      
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Texto 2</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="txt2" name="txt2" value="<?php echo $data->txt2; ?>" >
                            </div>
                        </div>      
                    <?php
                    }                    
                    if($data->seccion=='txt_conocenos' || $seccion=='txt_time_rever' || $seccion=='txt_testimonios' || $seccion=='txt_has_pregunta' || $seccion=='txt_register_form' || $seccion=='txt_evento'){
                        ?>                                   
                        <div class="control-group">
                            <label class="control-label">
                                <?php
                                switch ($seccion) {
                                    case 'txt_evento':
                                        echo 'Icono';
                                        break;
                                    
                                    default:
                                        echo 'Banner de sección';
                                        break;
                                }
                            ?>
                            </label>                    
                            <div class="controls">
                            <div class="span6"><img src="files/informativa/<?php echo $data->imagen; ?>"/></div>
                            <input type="file" name="imagen"><br>
                            <?php
                            switch ($seccion) {
                                case 'txt_conocenos':
                                    ?>
                                        <span class="help-inline red">La imagen que suba debe ser de 570px de ancho por 320px de alto.</span>
                                    <?php
                                    break;
                                
                                case 'txt_time_rever':
                                    ?>
                                        <span class="help-inline red">La imagen que suba debe ser de 640px de ancho por 303px de alto.</span>
                                    <?php
                                    break;
                                case 'txt_testimonios':
                                    ?>
                                        <span class="help-inline red">La imagen que suba debe ser de 1920px de ancho por 565px de alto.</span>
                                    <?php
                                    break; 
                                case 'txt_has_pregunta':
                                    ?>
                                        <span class="help-inline red">La imagen que suba debe ser de 825px de ancho por 679px de alto.</span>
                                    <?php
                                    break;  
                                    
                                case 'txt_register_form':
                                    ?>
                                        <span class="help-inline red">La imagen que suba debe ser de 500px de ancho por 637px de alto.</span>
                                    <?php
                                    break;  
                                case 'txt_evento':
                                    ?>
                                        
                                        <span class="help-inline red">La imagen que suba debe ser de 60px de ancho por 60px de alto.</span>
                                    <?php
                                    break;                                                                        
                                    
                            }
                            ?>
                            </div>
                        </div>
                    <?php
                    }

                    if($data->seccion=='txt_conocenos'){
                        ?>
                        <div class="control-group">
                            <label class="control-label">Link de video</label>                    
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="txt2" name="txt2" value="<?php echo $data->txt2; ?>" >
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    
                    <?php
                    if($seccion=="txt_conocenos" || $seccion=='txt_evento' || $seccion=='txt_ponente' || $seccion=='txt_precios' || $seccion=='txt_galeria' || $seccion=='txt_sponsor' || $seccion=='txt_mapa' || $seccion=='txt_register_form' || $seccion=='txt_video'){
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Sumilla</label>
                        <div class="controls">
                            <textarea id="txt4" name="txt4" rows="7" style="width:400px"><?php echo $data->txt4; ?></textarea>
                            <script type="text/javascript">
                                //CKEDITOR.replace( 'texto',{height:'600px',width:'850px'} );
                            </script>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                    <div class="form-actions">
                        <input type="hidden" name="seccion" id="seccion" value="<?php echo $data->seccion;?>">
                        <input type="hidden" name="imgantg" id="imgantg" value="<?php echo $data->imagen;?>">                        
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->