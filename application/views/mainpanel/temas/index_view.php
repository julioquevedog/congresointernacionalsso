
    <ul class="breadcrumb">
        <li><b>Evento:</b> <?php echo $evento->nombre;?></li>
        <span class="divider">/</span>
        <li><b>Programación:</b> <?php echo $programacion->texto1;?></li>
        <span class="divider">/</span>
        <li><b>Bloque:</b> <?php echo $bloque->nombre;?></li>        
    </ul>
    <a href="mainpanel/controller_temas/nuevo/<?php echo $bloque->id;?>" class="btn btn-mini btn-success">Crear Tema</a>

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Temas</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_bloques/listar/<?php echo $bloque->programacion_id;?>" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="20%">Nombre</th>                        
                        <th width="45%">Horario</th>                                                
                        <th width="10%">Estado</th>
                        <th width="10%">Orden</th>                        
                        <th width="10%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {


                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$value->nombre.'</td>';                        
                        echo '<td>'.$value->horario.'</td>';                        
                        echo '<td class="center">'.$value->estado.'</td>';
                        echo '<td class="center">'.$value->orden.'</td>';                        
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_temas/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el tema '.$value->nombre.'?" data-id="'.$value->id.'"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_temas/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->