    <ul class="breadcrumb">
        <li><b>Evento:</b> <?php echo $evento->nombre;?></li>
        <span class="divider">/</span>
        <li><b>Programación:</b> <?php echo $programacion->texto1;?></li>
        <span class="divider">/</span>
        <li><b>Bloque:</b> <?php echo $bloque->nombre;?></li>        
    </ul>
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-plus"></i> Crear Tema
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="mainpanel/controller_temas/grabar" method="post" enctype="multipart/form-data" 
                    onsubmit="return validar()">
                    <fieldset>
                        <legend>Ingrese los datos</legend>
                        <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        } 
                        ?>

                                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Nombre</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="nombre" name="nombre" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Sumilla</label>
                            <div class="controls">
                                <textarea id="sumilla" name="sumilla" class="span6"></textarea>
                            </div>
                        </div>                        
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Horario</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="horario" name="horario" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label" for="typeahead">Ponente</label>
                            <div class="controls">
                                <select name="ponente_id" id="" class="span6 typeahead">
                                <option value="0">Ningun ponente</option>
                                <?php
                                foreach ($ponentes as $key => $value) {
                                    ?>
                                    <option value="<?php echo $value->id?>"><?php echo $value->nombres?></option>
                                    <?php
                                }
                                ?>
                                
                                </select>
                            </div>
                        </div>  

                        <div class="control-group">
                            <label class="control-label" for="typeahead">Orden</label>
                            <div class="controls">
                                <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $orden;?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>

                       

                        <div class="control-group">
                            <label class="control-label">Estado</label>
                            <div class="controls">
                                <label class="radio">
                                    <input type="radio" name="estado" id="estado1" value="A" checked="checked">
                                    Activo
                                </label>
                                <div style="clear:both"></div>
                                <label class="radio">
                                    <input type="radio" name="estado" id="estado2" value="I">
                                    Inactivo
                                </label>
                            </div>
                        </div> 
                        <hr>                   

                        <div class="form-actions">
                            <input type="hidden" value="<?php echo $bloque->id?>" name="bloque_id">
                            <input type="submit" class="btn btn-primary" value="GRABAR">
                            &nbsp;&nbsp;
                            <a class="btn btn-danger" href="mainpanel/controller_temas/listar/<?php echo $bloque->id?>">VOLVER AL LISTADO</a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
    </div>


