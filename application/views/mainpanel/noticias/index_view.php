<div>
    <ul class="breadcrumb">
        <li><a href="mainpanel/noticias/listado">Lista de Noticias</a> <span class="divider">/</span></li>
        <li><a href="mainpanel/noticias/nuevo">Agregar Noticia</a></li>
    </ul>
</div>
<div class="row-fluid sortable">
 
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> Noticias</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
                ?>                   
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="15%">Imagen</th>
                        <th width="30%">Título</th>
                        <th width="5%">Orden</th>                        
                        <th width="10%">Fecha</th>
                        <th width="10%">Estado</th>
                        <th width="25%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    $orden = 1;
                    foreach($noticias as $noticia)
                    {
                        if(is_file('./files/noticias/'.$noticia->imagen))
                        {
                            $pic = '<img src="./files/noticias/'.$noticia->imagen.'" />';
                        }
                        else
                        {
                            $pic = '<img src="assets/frontend/confeccionesrials/imagenes/img300x200.png" />';
                            $pic = '';
                        }
                        echo '<tr>';
                        echo '<td class="center">'.$orden.'</td>';
                        echo '<td>'.$pic.'</td>';
                        echo '<td>'.$noticia->titulo.'</td>';
                        echo '<td>'.$noticia->orden.'</td>';                        
                        echo '<td>'.Ymd_2_dmY($noticia->fecha).'</td>';
                        if($noticia->estado=="A")
                        {
                            echo '<td><span class="label label-success">ACTIVO</span></td>';
                        }
                        else
                        {
                            echo '<td><span class="label label-important">INACTIVO</span></td>';
                        }
                        echo '<td>';
                        echo '<a class="btn btn-info" href="mainpanel/noticias/edit/'.$noticia->id_noticia.'"><i class="icon-edit icon-white"></i>  Editar</a> ';
                        echo '<a class="btn btn-danger" href="javascript:deleteNoticia(\''.$noticia->id_noticia.'\')"><i class="icon-trash icon-white"></i>Borrar</a>';
                        echo '</td>';
                        echo '</tr>';
                        $orden++;
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->