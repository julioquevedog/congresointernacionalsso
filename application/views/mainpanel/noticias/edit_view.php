<div>
    <ul class="breadcrumb">
        <li><a href="mainpanel/noticias/listado">Lista de Noticias</a> <span class="divider">/</span></li>
        <li><a href="mainpanel/noticias/nuevo">Agregar Noticia</a></li>
    </ul>
</div>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Noticia</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/noticias/actualizar" method="post" enctype="multipart/form-data" onsubmit="return valida_noticia()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>
                    <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        } 
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Título</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="titulo" name="titulo" value="<?php echo $noticia->titulo;?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $noticia->orden;?>" >
                        </div>
                    </div>                    
<!--                    <div class="control-group">
                        <label class="control-label" for="typeahead">Fecha</label>
                        <div class="controls">
                            <input type="text" class="span2 datepicker" id="fecha" name="fecha" value="<?php //echo fecha_hoy_dmY(); ?>" >
                        </div>
                    </div>-->
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="estado" id="estado1" value="A"<?php if($noticia->estado=="A") echo ' checked="checked"'; ?>>
                                Activo
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="estado" id="estado2" value="I"<?php if($noticia->estado=="I") echo ' checked="checked"'; ?>>
                                Inactivo
                            </label>
                        </div>
                    </div>      
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Imagen</label>
                        <div class="controls">
                            <div class="span6"><img src="files/noticias/<?php echo $noticia->imagen; ?>" /></div>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <div class="alert alert-block ">
                            <p>La imagen a subir debe tener 150px de ancho y alto. Caso contrario la imagen se forzará al tamaño indicado.</p>
                            </div> 
                        </div>
                    </div>   
                    <div class="control-group">
                        <label class="control-label">Cambiar imagen</label>
                        <div class="controls">
                          <input type="file" name="imagen">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">TITLE</label>
                        <div class="controls">
                            <input type="text" class="span6" id="title" name="title" value="<?php echo $noticia->title;?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">KEYWORDS</label>
                        <div class="controls">
                            <input type="text" class="span6" id="keywords" name="keywords" value="<?php echo $noticia->keywords;?>" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">DESCRIPTION</label>
                        <div class="controls">
                            <input type="text" class="span12" id="description" name="description" value="<?php echo $noticia->description;?>" >
                        </div>
                    </div>                    
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Sumilla</label>
                        <div class="controls">
                            <textarea id="sumilla" name="sumilla" rows="3" class="span4"><?php echo $noticia->sumilla;?></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Descripci&oacute;n</label>
                        <div class="controls">
                            <textarea id="descripcion" name="descripcion" rows="3"><?php echo $noticia->descripcion;?></textarea>
                            <script type="text/javascript">
                                CKEDITOR.replace( 'descripcion',{height:'500px',width:'850px'} );
                            </script>
                        </div>
                    </div>                       
                    <div class="form-actions">
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        <input type="hidden" name="id_noticia" id="id_noticia" value="<?php echo $noticia->id_noticia; ?>">                        
                        <input type="hidden" name="imgantg" id="imgantg" value="<?php echo $noticia->imagen; ?>">                                                
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/noticias/listado">VOLVER AL LISTADO</a>
                    </div>

                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->