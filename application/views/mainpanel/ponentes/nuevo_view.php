
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-plus"></i> Crear Ponente
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="mainpanel/controller_ponentes/grabar" method="post" enctype="multipart/form-data" 
                    onsubmit="return validar()">
                    <fieldset>
                        <legend>Ingrese los datos</legend>
                        <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        } 
                        ?>

                                        
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombres</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="nombres" name="nombres" value="" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Cargo</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="cargo" name="cargo" value="" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Sumilla</label>
                        <div class="controls">
                            <textarea id="sumilla" name="sumilla" class="span6"></textarea>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label" for="typeahead">Descripci&oacute;n</label>
                        <div class="controls">
                            <textarea class="span6 typeahead" name="descripcion" id="descripcion" style="height:100px"></textarea>
                            <script type="text/javascript">
                                CKEDITOR.replace( 'descripcion',{toolbar:'Basic',height:'300px',width:'650px'} );
                            </script>                            
                        </div>
                    </div>                      
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $orden?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado1" value="A" checked="checked">
                                Activo
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado2" value="I" >
                                Inactivo
                            </label>
                        </div>
                    </div>  
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link de Facebook</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link_facebook" name="link_facebook" value="" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link de Twitter</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link_twitter" name="link_twitter" value="" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link de Linkedin</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link_linkedin" name="link_linkedin" value="" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>  
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link de Google</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link_google" name="link_google" value="" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                                                                  
                    <hr>                   
                    <div class="control-group">
                        <label class="control-label">Imagen <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                        <div class="controls">
                            <div class="span6">
                                <input type="file" name="imagen" id="imagen">
                                <br>
                                <span class="help-inline red">La imagen que suba debe ser de 300px de ancho por 300px de alto.</span>                        
                            </div>
                        </div>
                    </div>  
 
                        <hr>                   

                        <div class="form-actions">
                            <input type="submit" class="btn btn-primary" value="GRABAR">
                            &nbsp;&nbsp;
                            <a class="btn btn-danger" href="mainpanel/controller_ponentes/listar">VOLVER AL LISTADO</a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
    </div>


