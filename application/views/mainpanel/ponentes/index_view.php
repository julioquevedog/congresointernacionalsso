<a href="mainpanel/controller_ponentes/nuevo" class="btn btn-mini btn-success">Crear ponente</a>

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Ponentes</h2>
            <div class="box-icon">
            
            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="20%">Imagen</th>                        
                        <th width="25%">Nombres</th>                                                
                        <th width="20%">Cargo</th>
                        <th width="10%">Estado</th>
                        <th width="10%">Orden</th>                        
                        <th width="10%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {
                        if(is_file('files/ponentes/'.$value->imagen))
                        {
                            $img = getimagesize('files/ponentes/'.$value->imagen);
                            $ancho = (int)($img[0]/5);
                            $alto = (int)($img[1]/5);                            
                            $pic = '<img src="files/ponentes/'.$value->imagen.'" border="0" width="'.$ancho.'" />';
                        }
                        else
                        {
                            $pic='foto';
                        }

                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$pic.'</td>';
                        echo '<td>'.$value->nombres.'</td>';                        
                        echo '<td>'.$value->cargo.'</td>';                                                
                        echo '<td class="center">'.$value->estado.'</td>';
                        echo '<td class="center">'.$value->orden.'</td>';                        
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_ponentes/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        al ponente '.$value->nombres.'?" data-id="'.$value->id.'"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_ponentes/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->