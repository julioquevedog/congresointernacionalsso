

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Ponente</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_ponentes/listar" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_ponentes/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validarp()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>

                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                                     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombres</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="nombres" name="nombres" value="<?php echo $data->nombres?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Cargo</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="cargo" name="cargo" value="<?php echo $data->cargo?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                    
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Sumilla</label>
                        <div class="controls">
                            <textarea id="sumilla" name="sumilla" class="span6"><?php echo $data->sumilla?></textarea>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class="control-label" for="typeahead">Descripci&oacute;n</label>
                        <div class="controls">
                            <textarea class="span6 typeahead" name="descripcion" id="descripcion" style="height:100px"><?php echo $data->descripcion;?></textarea>
                            <script type="text/javascript">
                                //CKEDITOR.replace( 'descripcion',{toolbar:'Basic',height:'300px',width:'650px'} );
                                CKEDITOR.replace( 'descripcion',{height:'300px',width:'650px'} );
                            </script>                            
                        </div>
                    </div>                      
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $data->orden?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado1" value="A" <?php echo ($data->estado=='A')?'checked="checked"':'';?>>
                                Activo
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado2" value="I" <?php echo ($data->estado=='I')?'checked="checked"':'';?>>
                                Inactivo
                            </label>
                        </div>
                    </div>  
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link de Facebook</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link_facebook" name="link_facebook" value="<?php echo $data->link_facebook?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link de Twitter</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link_twitter" name="link_twitter" value="<?php echo $data->link_twitter?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>     
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link de Linkedin</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link_linkedin" name="link_linkedin" value="<?php echo $data->link_linkedin?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>  
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link de Google</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link_google" name="link_google" value="<?php echo $data->link_google?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                                                                                  
                    <hr>                   
                    <div class="control-group">
                        <label class="control-label">Imagen <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                        <div class="controls">
                            <div class="span6"><img src="files/ponentes/<?php echo $data->imagen; ?>" /></div>
                            <input type="file" name="imagen" id="imagen">
                            <br>
                            <span class="help-inline red">La imagen que suba debe ser de 300px de ancho por 300px de alto.</span>                        
                        </div>
                    </div>  

                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">                        
                        <input type="hidden" name="imgatng" value="<?php echo $data->imagen; ?>">                        
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_ponentes/listar">VOLVER AL LISTADO</a>
                        <a data-rel="tooltip" class="btn btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        al exponente <?php echo $data->nombres;?>?" data-id="<?php echo $data->id;?>"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_ponentes/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i>ELIMINAR</a>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<script>
    function validarp(){

        var val = $('select[name=id_categoria_padre]').val();
        if(val==0){
            alert("Debe escoger una subcategoria");
            return false;
        }
    }

    $("input[name=closet_sales]").change(function(){
        var val=$(this).val();
        if(val==1){
            $(".wrSelectCloset").show();
        }else{
            $(".wrSelectCloset").hide();
        }
    });

</script>