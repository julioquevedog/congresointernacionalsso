<a href="mainpanel/controller_testimonios/nuevo" class="btn btn-mini btn-success">Crear testimonio</a>

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Testimonios</h2>
            <div class="box-icon">
            
            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="10%">Foto</th>
                        <th width="15%">Nombres</th>
                        <th width="40%">Texto</th>
                        <th width="10%">Cargo</th>                        
                        <th width="5%">Estado</th>
                        <th width="5%">Orden</th>                        
                        <th width="10%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {

                        $imagen = $value->foto;
                        
                        if( (isset($imagen)) && (is_file('files/testimonios/'.$imagen)) )
                        {
                            
                            $img = getimagesize('files/testimonios/'.$imagen);
                            $ancho = (int)($img[0]/1);
                            $alto = (int)($img[1]/1);
                            $foto = '<img src="files/testimonios/'.$imagen.'" width="'.$ancho.'" height="'.$alto.'"/>';
                        }
                        else
                        {
                            $foto = 'No hay foto';
                        }


                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$foto.'</td>';
                        echo '<td>'.$value->nombres.'</td>';                        
                        echo '<td>'.$value->descripcion.'</td>';                                                
                        echo '<td class="center">'.$value->cargo.'</td>';
                        echo '<td class="center">'.$value->estado.'</td>';
                        echo '<td class="center">'.$value->orden.'</td>';                        
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_testimonios/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        al testimonio '.$value->nombres.'?" data-id="'.$value->id.'"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_testimonios/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->