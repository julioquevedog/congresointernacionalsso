
<ul class="breadcrumb">
    <li><b>Stand: </b><?php echo $stand->nombre;?></li>
</ul>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar Servicio</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_servicios_stands/listar/<?php echo $stand->id?>" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_servicios_stands/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validarp()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>

                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombre</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="nombre" name="nombre" value="<?php echo $data->nombre?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Link de pago</label>
                        <div class="controls">
                            <input type="text" class="span6 typeahead" id="link_pago" name="link_pago" value="<?php echo $data->link_pago?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>                       
                    <div class="control-group">
                        <label class="control-label" for="sumilla">Sumilla</label>
                        <div class="controls">
                            <textarea id="sumilla" name="sumilla" rows="7" style="width:400px"><?php echo $data->sumilla?></textarea>
                        </div>
                    </div>                                             
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $data->orden?>" data-required="true" data-message-required="Se requiere">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado1" value="A" <?php echo ($data->estado=='A')?'checked="checked"':'';?>>
                                Activo
                            </label>
                            
                            <label class="radio inline">
                                <input type="radio" name="estado" id="estado2" value="I" <?php echo ($data->estado=='I')?'checked="checked"':'';?>>
                                Inactivo
                            </label>
                        </div>
                    </div>    
                    <hr>                   
                    <div class="control-group">
                        <label class="control-label">Imagen <span class="help-inline red"><b>(Obligatorio)</b></span></label>
                        <div class="controls">
                            <div class="span6">
                                
                            <?php

                            if(is_file('files/servicios_stands/'.$data->imagen))
                            {
                                $img = getimagesize('files/servicios_stands/'.$data->imagen);
                                $ancho = (int)($img[0]/1);
                                $alto = (int)($img[1]/1);                            
                                $pic = '<img src="files/servicios_stands/'.$data->imagen.'" border="0" width="'.$ancho.'" />';
                            }
                            else
                            {
                                $img = getimagesize('assets/frontend/images/imagen-estandar-servicio-stand.jpg');
                                $ancho = (int)($img[0]/1);
                                $alto = (int)($img[1]/1);                            
                                $pic = '<img src="assets/frontend/images/imagen-estandar-servicio-stand.jpg" border="0" width="'.$ancho.'" />';                            
                            }  
                            
                            echo $pic;
                            
                            ?>

                            </div>
                            <input type="file" name="imagen" id="imagen">
                            <br>
                            <span class="help-inline red">La imagen debe ser de 360px de ancho por 180px de alto.</span>                        
                        </div>
                    </div>   
                    
                        <hr>                   
                                    
                        <div class="control-group">
                            <label class="control-label">PDF <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                            <div class="controls">
                                <div class="span6">
                                    <?php
                                    if(is_file('files/servicios_stands_pdf/'.$data->pdf))
                                    {
                                        $pdf = '<a href="files/servicios_stands_pdf/'.$data->pdf.'" target="_blank"><img src="assets/frontend/images/pdf_icon.png" width = "80" border="0"/></a>';
                                    }
                                    else
                                    {
                                        $pdf = 'Sin PDF';
                                    }   
                                    
                                    echo $pdf;
                                
                                    ?>

                                </div>
                                <input type="file" name="pdf" id="pdf">
                                <br>
                                <span class="help-inline red">Solo de tipo PDF, si no será rechazado.</span>

                            </div>
                        </div>                    

                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">                        
                        <input type="hidden" name="imgatng" value="<?php echo $data->imagen; ?>">                        
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_servicios_stands/listar/<?php echo $stand->id;?>">VOLVER AL LISTADO</a>
                        <a data-rel="tooltip" class="btn btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el servicio ?" data-id="<?php echo $data->id;?>"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_servicios_stands/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i>ELIMINAR</a>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->

<script>
    function validarp(){

        var val = $('select[name=id_categoria_padre]').val();
        if(val==0){
            alert("Debe escoger una subcategoria");
            return false;
        }
    }

    $("input[name=closet_sales]").change(function(){
        var val=$(this).val();
        if(val==1){
            $(".wrSelectCloset").show();
        }else{
            $(".wrSelectCloset").hide();
        }
    });

</script>