
    <ul class="breadcrumb">
        <li><b>Stand: </b><?php echo $stand->nombre;?></li>
    </ul>
    <div class="row-fluid sortable">
        <div class="box span12">
            <div class="box-header well" data-original-title>
                <h2><i class="icon-plus"></i> Crear Servicio
                </h2>
                <div class="box-icon">
                </div>
            </div>
            <div class="box-content">
                <form class="form-horizontal" action="mainpanel/controller_servicios_stands/grabar" method="post" enctype="multipart/form-data" 
                    onsubmit="return validar()">
                    <fieldset>
                        <legend>Ingrese los datos</legend>
                        <?php
                        if($this->session->userdata('success'))
                        {
                            echo '<div class="alert alert-success">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('success');
                            echo '</div>';
                            $this->session->unset_userdata('success');
                        }
                        if($this->session->userdata('error'))
                        {
                            echo '<div class="alert alert-error">';
                            echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                            echo $this->session->userdata('error');
                            echo '</div>';
                            $this->session->unset_userdata('error');
                        } 
                        ?>
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Nombre</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="nombre" name="nombre" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div> 
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Link de pago</label>
                            <div class="controls">
                                <input type="text" class="span6 typeahead" id="link_pago" name="link_pago" value="" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>                          
                        <div class="control-group">
                            <label class="control-label" for="sumilla">Sumilla</label>
                            <div class="controls">
                                <textarea id="sumilla" name="sumilla" rows="7" style="width:400px"></textarea>
                            </div>
                        </div>  
                                            
                        <div class="control-group">
                            <label class="control-label" for="typeahead">Orden</label>
                            <div class="controls">
                                <input type="text" class="span2 typeahead" id="orden" name="orden" value="<?php echo $orden;?>" data-required="true" data-message-required="Se requiere">
                            </div>
                        </div>

                      

                        <div class="control-group">
                            <label class="control-label">Estado</label>
                            <div class="controls">
                                <label class="radio">
                                    <input type="radio" name="estado" id="estado1" value="A" checked="checked">
                                    Activo
                                </label>
                                <div style="clear:both"></div>
                                <label class="radio">
                                    <input type="radio" name="estado" id="estado2" value="I">
                                    Inactivo
                                </label>
                            </div>
                        </div> 
                        <hr>                   
                                    
                        <div class="control-group">
                            <label class="control-label">Imagen <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                            <div class="controls">
                                <div class="span6">
                                    <input type="file" name="imagen" id="imagen">
                                    <br>
                                    <span class="help-inline red">La imagen debe ser de 360px de ancho por 180px de alto.</span>                        
                                </div>
                            </div>
                        </div>   
                        <hr>                   
                                    
                        <div class="control-group">
                            <label class="control-label">PDF <span class="help-inline red"><b>(Obligatorio)</b></span></label>                    
                            <div class="controls">
                                <div class="span6">
                                    <input type="file" name="pdf" id="pdf">
                                    <br>
                                    <span class="help-inline red">Solo de tipo PDF, si no será rechazado.</span>
                                </div>
                            </div>
                        </div>                           
                        <div class="form-actions">
                            <input type="hidden" value="<?php echo $stand->id?>" name="stand_id">
                            <input type="submit" class="btn btn-primary" value="GRABAR">
                            &nbsp;&nbsp;
                            <a class="btn btn-danger" href="mainpanel/controller_servicios_stands/listar/<?php echo $stand->id?>">VOLVER AL LISTADO</a>
                        </div>
                    </fieldset>
                </form>   

            </div>
        </div><!--/span-->
    </div>


