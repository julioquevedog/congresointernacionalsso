
    <ul class="breadcrumb">
        <li><b>Stand: </b><?php echo $stand->nombre;?></li>
    </ul>
    <a href="mainpanel/controller_servicios_stands/nuevo/<?php echo $stand->id;?>" class="btn btn-mini btn-success">Crear Servicio</a>

<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Servicios</h2>
            <div class="box-icon">
            <a href="mainpanel/controller_stands/listar" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>
            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="15%">Imagen</th>                                                
                        <th width="10%">PDF</th>
                        <th width="25%">Nombre</th>
                        <th width="10%">Estado</th>
                        <th width="10%">Orden</th>                        
                        <th width="20%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php
                    
                    
                    foreach ($data as $key => $value) {

                        if(is_file('files/servicios_stands/'.$value->imagen))
                        {
                            $img = getimagesize('files/servicios_stands/'.$value->imagen);
                            $ancho = (int)($img[0]/1);
                            $alto = (int)($img[1]/1);                            
                            $pic = '<img src="files/servicios_stands/'.$value->imagen.'" border="0" width="'.$ancho.'" />';
                        }
                        else
                        {
                            $img = getimagesize('assets/frontend/images/imagen-estandar-servicio-stand.jpg');
                            $ancho = (int)($img[0]/1);
                            $alto = (int)($img[1]/1);                            
                            $pic = '<img src="assets/frontend/images/imagen-estandar-servicio-stand.jpg" border="0" width="'.$ancho.'" />';                            
                        }

                        if(is_file('files/servicios_stands_pdf/'.$value->pdf))
                        {
                            $pdf = '<a href="files/servicios_stands_pdf/'.$value->pdf.'" target="_blank"><img src="assets/frontend/images/pdf_icon.png" width = "40" border="0"/></a>';
                        }
                        else
                        {
                            $pdf = 'Sin PDF';
                        }                        

                        echo '<tr>';
                        echo '<td class="center">'.($key+1).'</td>';
                        echo '<td>'.$pic.'</td>';                        
                        echo '<td>'.$pdf.'</td>';
                        echo '<td class="center">'.$value->nombre.'</td>';                        
                        echo '<td class="center">'.$value->estado.'</td>';
                        echo '<td class="center">'.$value->orden.'</td>';                        
                        echo '<td class="center">';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-success" href="mainpanel/controller_servicios_stands/edit/'.$value->id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '<a data-rel="tooltip" class="btn btn-mini btn-danger btndelete" data-message="Esta seguro que desea eliminar 
                        el servicio?" data-id="'.$value->id.'"
                        data-elementos-dependientes="0" data-url="mainpanel/controller_servicios_stands/delete" 
                        href="#" title="Eliminar"><i class="icon-remove-sign icon-white"></i></a> ';
                        echo '</td>';
                        echo '</tr>';
                        
                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->