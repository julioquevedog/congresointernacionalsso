
<div>
    <ul class="breadcrumb">
    <h2><?php echo strtoupper('Menu del FrontEnd');?></h2>
        <!-- <li><a href="mainpanel/closet/nuevo" class="btn btn-mini btn-primary"><i class="icon-plus icon-white"></i>Agregar closet</a></li> -->
    </ul>
</div>
<div class="row-fluid sortable">
  
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-th-list"></i> Listado</h2>
            <div class="box-icon">

            </div>
        </div>
        <div class="box-content">
            <?php
                if($this->session->userdata('success'))
                {
                    echo '<div class="alert alert-success">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('success');
                    echo '</div>';
                    $this->session->unset_userdata('success');
                }
                if($this->session->userdata('error'))
                {
                    echo '<div class="alert alert-error">';
                    echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                    echo $this->session->userdata('error');
                    echo '</div>';
                    $this->session->unset_userdata('error');
                } 
            ?>              
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">Nro</th>
                        <th width="35%">Nombre</th>
                        <!-- <th width="25%">Link</th>                         -->
                        <th width="15%">Orden</th>                        
                        <th width="15%">Estado</th>
                        <th width="10%">Acción</th>
                    </tr>
                </thead>   
                <tbody>
                <?php

                    //var_dump($datos);
                    for($i=0; $i<count($datos); $i++)
                    {
                        $current = $datos[$i];
                        $id = $current->id;
                        $nombre = $current->nombre;
                        $link = $current->link;                        
                        $orden = $current->orden;
                        $estado = $current->estado;                        

                        echo '<tr>';
                        echo '<td class="center">'.($i+1).'</td>';
                        echo '<td class="center">'.$nombre.'</td>';                        
                        //echo '<td class="center">'.$link.'</td>';
                        echo '<td class="center">'.$orden.'</td>';
                        echo '<td>'.$estado.'</td>';                        
                        echo '<td class="center">';
                        echo '<a class="btn btn-mini btn-success" href="mainpanel/controller_menu/edit/'.$id.'" title="Editar"><i class="icon-edit icon-white"></i></a> ';
                        echo '</td>';
                        echo '</tr>';

                    }
                ?>
                </tbody>
            </table>            
        </div>
     </div><!--/span-->
</div><!--/row-->