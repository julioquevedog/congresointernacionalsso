
<div>
    <ul class="breadcrumb">
    <h2><?php echo strtoupper('Post Destacados');?></h2>
    </ul>
</div>
<div class="row-fluid">


    <div class="box span12">
        
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Editar</h2>
            <div class="box-icon">
                <a href="mainpanel/controller_menu/listado" class="btn btn-round" title="VOLVER"><i class="icon-arrow-left"></i></a>                                
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="mainpanel/controller_menu/actualizar" method="post" 
                  enctype="multipart/form-data" onsubmit="return validar()">
                <fieldset>
                    <legend>Modifique los datos deseados</legend>
                    <?php
                    if($this->session->userdata('success'))
                    {
                        echo '<div class="alert alert-success">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('success');
                        echo '</div>';
                        $this->session->unset_userdata('success');
                    }
                    if($this->session->userdata('error'))
                    {
                        echo '<div class="alert alert-error">';
                        echo '<button type="button" class="close" data-dismiss="alert">×</button>';
                        echo $this->session->userdata('error');
                        echo '</div>';
                        $this->session->unset_userdata('error');
                    } 
                    ?>
                  
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Nombre</label>
                        <div class="controls">
                            <input type="text" class="span8 typeahead" id="nombre" name="nombre" value="<?php echo $data->nombre; ?>" data-required="true" data-message-required="Ingrese el nombre">
                        </div>
                    </div>
                    <!-- <div class="control-group">
                        <label class="control-label" for="typeahead">Link</label>
                        <div class="controls">
                            <input type="text" class="span8 typeahead" id="link" name="link" value="<?php echo $data->link; ?>" data-required="true" data-message-required="Ingrese el link">
                        </div>
                    </div> -->
                    <div class="control-group">
                        <label class="control-label">Estado</label>
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="estado" id="estado1" value="A"<?php if($data->estado=="A") echo ' checked="checked"'; ?>>
                                Activo
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="estado" id="estado2" value="I"<?php if($data->estado=="I") echo ' checked="checked"'; ?>>
                                Inactivo
                            </label>
                        </div>
                    </div>                        
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Imagen para el menu</label>
                        <div class="controls">
                            <div class="span6"><img src="files/menu/<?php echo $data->imagen; ?>" /></div>
                        </div>
                    </div>
                    <div class="control-group">
	                    <label class="control-label">Cambiar Imagen de menu</label>                    
                        <div class="controls">
                          <input type="file" name="imagen">
                          <br>
                          <span class="help-inline red">La imagen debe ser de 290px de ancho por 310 px de alto.</span>                          
                        </div>
                    </div>                                                                                                              
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Orden</label>
                        <div class="controls">
                            <input type="text" class="span3 typeahead" id="orden" name="orden" value="<?php echo $data->orden; ?>" data-required="true" data-message-required="Se requiere el número de orden">
                        </div>
                    </div>
                    

                 
                    <div class="form-actions">
                        <input type="hidden" name="id" id="id" value="<?php echo $data->id; ?>">
                        <input type="hidden" name="imgatng" id="imgatng" value="<?php echo $data->imagen; ?>">
                        <input type="submit" class="btn btn-primary" value="GRABAR">
                        &nbsp;&nbsp;
                        <a class="btn btn-danger" href="mainpanel/controller_menu/listado">VOLVER AL LISTADO</a>
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->