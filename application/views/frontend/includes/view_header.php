			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<strong class="tg-logo"><a href="./"><img src="./assets/frontend/images/logo.png" alt="company logo here"></a></strong>
						<div class="tg-navigationarea">
							<!--<a class="tg-btn tg-btnbecommember" href="#tg-venue">Reserva tu asiento</a>-->
							<?php

							if($seccion == 'index' || $seccion == 'congreso-virtual-login'){
								?>
								<a class="tg-btn tg-btnbecommember tg-btn__yellow" href="congreso-virtual/login" 
								target = "_blank">Ingresar</a>
								<a class="tg-btn tg-btnbecommember" href="https://phvaperu.com/inscripciones/idf/pujjPOj2PyEXZYng2wgk11ijdqrdGSNRMiytq65BpZnaAOOYQfcKrFFQyDPC" 
								target = "_blank">Reservar</a>							

							<a id="tg-btnopenclose" class="tg-btnopenclose" href="javascript:void(0);"><i class="icon-menu"></i></a>

							<nav id="tg-nav" class="tg-nav">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>

								<div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
									<ul>
										<li class="tg-active menu-item-has-children">
											<a href="#body">Inicio</a>
											<!-- <ul class="sub-menu">
												<li><a href="index.html" class="external">Home v1</a></li>
												<li><a href="index2.html" class="external">Home v2</a></li>
												<li><a href="index3.html" class="external">Home v3</a></li>
												<li><a href="index4.html" class="external">Home v4</a></li>
												<li><a href="index5.html" class="external">Home v5</a></li>
												<li><a href="blogdetail.html" class="external">Blogdetail</a></li>
												<li><a href="404.html" class="external">404 error</a></li>
											</ul> -->
										</li>
										<li><a href="#tg-schedule">Programaci&oacute;n</a></li>
										<li><a href="#tg-speakers">Ponentes</a></li>
										<li><a href="#tg-price">Precios</a></li>
										<li><a href="#tg-sponsers">Sponsors</a></li>
										<!--<li><a href="#tg-venue">Lugar</a></li>-->
										<li style="display:none;"><a href="#loginVideo">Videos</a></li>
									</ul>
								</div>

							</nav>
						

							<p  class="datos-menu">
								<i class="icon-phone-handset"></i> <?php echo getConfig('telefono');?> / 
								<i class="icon-mail4"></i> <?php echo getConfig('email');?>
							</p>	
							

							<?php
							}
							?>								
						</div>
					</div>
				</div>
			</div>