<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo getConfig('title'); ?></title>
<base href="<?php echo base_url(); ?>" />
<meta name="Keywords" content="<?php echo getConfig('keywords'); ?>" />
<meta name="Description" content="<?php echo getConfig("description"); ?>" />    
<meta name="author" content="www.peruwebpages.com">
<!-- Standard Favicon -->
<link rel="icon" type="image/x-icon" href="assets/frontend/images/favicon.png" />

<?php $time = time();?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="apple-touch-icon" href="apple-touch-icon.png">
<link rel="stylesheet" href="assets/frontend/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/frontend/css/normalize.css">
<link rel="stylesheet" href="assets/frontend/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/frontend/css/icomoon.css">
<link rel="stylesheet" href="assets/frontend/css/owl.carousel.min.css">
<link rel="stylesheet" href="assets/frontend/css/customScrollbar.css">
<link rel="stylesheet" href="assets/frontend/css/photoswipe.css">
<link rel="stylesheet" href="assets/frontend/css/default-skin.css">
<link rel="stylesheet" href="assets/frontend/css/prettyPhoto.css">
<link rel="stylesheet" href="assets/frontend/css/animate.css">
<link rel="stylesheet" href="assets/frontend/css/transitions.css">
<link rel="stylesheet" href="assets/frontend/css/main.css?<?php echo $time;?>">
<link rel="stylesheet" href="assets/frontend/css/color.css">
<link rel="stylesheet" href="assets/frontend/css/responsive.css">
<script src="assets/frontend/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>


<style>
<?php
$color_patron_1 = getConfig('color_patron_1');
$color_patron_2 = getConfig('color_patron_2');
?>

.tg-logo{width: 270px;}
.datos-menu{clear:  both;float: right;margin: 10px 0 0 0; color: <?php echo $color_patron_1; ?>; font-weight: bold}

.tg-navhead{min-height: 130px;background: #ffffff}
.tg-navhead:before{content: initial;}

.tg-navigation2{padding: 0;width: 100%;float: left;}
.tg-navigation2 > ul {width: 100%;float: left;font-size: 13px;list-style: none;line-height: 14px; font-family: 'Montserrat', Arial, Helvetica, sans-serif;}
.tg-navigation2 ul li {float: left;margin: 0 5px;position: relative;line-height: inherit;list-style-type: none;}
.tg-navigation2 ul li a {color: #767676;display: block;border-radius: 4px;padding: 13px 15px;}
.tg-eventspeakerimg img{width: 90px !important;}

.tg-packagedetail h4 span{font-size: 50px;}
.tg-packagedetail h4 sub{font-size: 35px;}

.tg-eventvenueregistration .tg-sectionhead{padding: 0;}
.tg-formaskquestions .tg-btn,.tg-formregister .tg-btn {color: #ffffff;}

.tg-sectionspace{padding: 30px 0}

.tg-sidenavholder .tg-logo a img{width: 70%; margin: auto;}

.tg-packagehead .tg-themepostimg{margin: 0;}

.tg-packagehead{border: solid 4px <?php echo $color_patron_2; ?>;background: #ffffff;}

.tg-bannercontent h1{font-size: 60px;line-height: 60px;}

.modal-header{background: <?php echo $color_patron_2; ?>;}
.modal-footer .btn{background: <?php echo $color_patron_2; ?>; color: #fff;border-color: <?php echo $color_patron_1; ?>; border-radius: initial !important;}
.modal-title{color: #fff;font-weight: bold;}

.spread-overlay .tg-sidenavholder{z-index: 100000 !important;top: 130px;}
.tg-fixedme.spread-overlay .tg-sidenavholder{top: 110px !important;}

.tg-theme-tag, .tg-btn:before, .tg-article:hover .tg-btnreadmore:before, .tg-eventschedulenav li.active a, .tg-eventschedulenav li:hover a, .tg-btnfarword:hover, .tg-btnopenclose:hover, .tg-bannerregister .tg-heading:before, .tg-btnvisitwebsite, .tg-postdate, .tg-tag:hover, .tg-formfeedbacksearch button {
    background: <?php echo $color_patron_2; ?>;
}
.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar, .navbar-toggle .icon-bar{
    background: <?php echo $color_patron_2; ?> !important;
}
.tg-btn:before{
    background: <?php echo $color_patron_2; ?> !important;
}
.navbar-toggle, .tg-btnopenclose{
    border-color:<?php echo $color_patron_2; ?> !important;
}
 p a, p a:hover,  a:focus, a:active, .tg-navigation ul li:hover > a, .tg-nav ul li a.current, .tg-eventcounter span:first-child, .tg-package .tg-packagedetail h4 span, .tg-article:hover .tg-title h2 a, .tg-article:hover .tg-btnreadmore, .tg-btndownload:hover i, .tg-btnfarword i, .tg-btnopenclose i, .tg-close, .tg-headervtwo .tg-nav ul li:hover a, .tg-headervtwo .tg-nav ul li a.current, .tg-bannerregister .tg-heading h2, .tg-postdetail blockquote, .tg-widgetcontent ul li:hover a, .tg-widgetcontent ul li:hover a span, .tg-authorname h4:hover a{
    color: <?php echo $color_patron_2; ?>;
}
/* a:hover, */
.tg-sectionheading:before {
    background: <?php echo $color_patron_1; ?>;
}
.imgPonente{
    float: left;
    margin-right: 20px;
    margin-bottom: 20px;
    width: 33%;
}
.imgPonente img{
    border: solid 4px <?php echo $color_patron_2; ?>;
}
.imgPonente h3{
    margin: 0;
    padding: 0;
    font-size: 13px;
    margin-top: 10px;
    text-align: center;
    text-decoration: underline;
}
 p a, p a:hover, a:hover, a:focus, a:active, .tg-navigation ul li:hover > a, .tg-nav ul li a.current, .tg-eventcounter span:first-child, .tg-package .tg-packagedetail h4 span, .tg-article:hover .tg-title h2 a, .tg-article:hover .tg-btnreadmore, .tg-btndownload:hover i, .tg-btnfarword i, .tg-btnopenclose i, .tg-close, .tg-headervtwo .tg-nav ul li:hover a, .tg-headervtwo .tg-nav ul li a.current, .tg-bannerregister .tg-heading h2, .tg-postdetail blockquote, .tg-widgetcontent ul li:hover a, .tg-widgetcontent ul li:hover a span, .tg-authorname h4:hover a
{color: <?php echo $color_patron_2; ?>;}
.modal-body{overflow: auto;}

.tg-package ul li{padding: 8px 25px;}

.tg-bannercontent h1 span{display: initial;}

.wmapa{background: url(../images/mapa.jpg) no-repeat; background-size: cover ;height: 700px;}

.tg-btndownload{width: 200px;}
.tg-btndownload span{margin: 10px;font-size: 13px;font-weight: bold;text-transform: uppercase;}
.tg-btndownload img{width: 60px;}
.tg-eventscheduletabs{min-height: 500px;}
.tg-eventschedulecontent{min-height: 430px;}

.tg-theme-tag:after, .tg-theme-tag:before, input:focus, .select select:focus, .form-control:focus, .tg-footer, .tg-btndownload:hover, .tg-eventschedulenav li.active a, .tg-eventschedulenav li:hover a, .tg-eventschedulenav, .tg-eventvenuetabs li.active a, .tg-eventvenuetabs li:hover a, .tg-btnfarword:hover, .tg-btnopenclose:hover, .tg-eventschedulecontent, .sub-menu{
    border-color:<?php echo $color_patron_2; ?>;
}
.tg-btnwhite .tg-btn:hover{
    color: #fff;
    background: <?php echo $color_patron_1; ?>;    
}

.tg-speakerinfo{max-width: 570px;}

body.tg-fixedme .tg-header .tg-logo{width: 160px;}

.tg-opaca:before{
    background: -moz-linear-gradient(-45deg,
 rgba(51,51,51,0.76) 0%,
 rgba(51,51,51,0.76) 80%) !important;
    background: -webkit-linear-gradient(-45deg,
 rgba(51,51,51,0.76) 0%,
 rgba(51,51,51,0.76) 80%) !important;
    background: linear-gradient(135deg, rgba(51,51,51,0.76) 0%, rgba(51,51,51,0.76) 80%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3f6730d', endColorstr='#b3d90845',GradientType=1 ) !important;
 
}

.tg-opacaAfter:after{
    background: -moz-linear-gradient(-45deg,
 rgba(51,51,51,0.76) 0%,
 rgba(51,51,51,0.76) 80%) !important;
    background: -webkit-linear-gradient(-45deg,
 rgba(51,51,51,0.76) 0%,
 rgba(51,51,51,0.76) 80%) !important;
    background: linear-gradient(135deg, rgba(51,51,51,0.76) 0%, rgba(51,51,51,0.76) 80%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b3f6730d', endColorstr='#b3d90845',GradientType=1 ) !important;
 
}

.tg-package:hover .tg-packagedetail h3, .tg-package:hover .tg-packagedetail span, .tg-package:hover .tg-packagedetail h4, .tg-package:hover .tg-packagedetail h4 sub, .tg-package:hover .tg-packagedetail h4 span
{
    text-shadow: 1px 1px 1px #000;
}



/* VIDEO */
.tg-videomain {
    width: 100%;float: left;position: relative;background: #b8b8b7;
    -webkit-box-shadow: inset 2px 10px 42px -20px rgba(0,0,0,0.75);
    -moz-box-shadow: inset 2px 10px 42px -20px rgba(0,0,0,0.75);
    box-shadow: inset 2px 10px 42px -20px rgba(0,0,0,0.75);
}
.tg-videomain .tg-sectionheading span{color: #fff;}
.tg-videomain h2{color: #184350;}
.tg-videomain .tg-description p{color: #fff;text-align: center;}
.tg-videomain .tg-description p .icon-user-check,.tg-videomain .tg-description p .icon-mail4{vertical-align: middle;font-size: 14px;}
.tg-videomain .tg-loginWrapper {width: 100%;float: left;}
.tg-videomain .tg-loginWrapper .login{width: 60%;margin: auto;padding: 20px;border: solid 7px #fff;}
.tg-videomain .tg-loginWrapper .login form .form-group{margin-bottom:5px;}
.tg-videomain .tg-loginWrapper .login .btn-login{background:#184350 !important;}
.tg-videomain .video-responsive{position: relative; width: 100%; height: 100%; top:0; left:0;}
.tg-videomain .previewVideoWrapper{width: 100%;height: 400px;    -ms-display: flex;
    display: flex;
    align-items: center;
    justify-content: center;
    background: #fff;}
.tg-videomain .alert-danger p{display: inline-block;}

.tg-videomain .wvideos{background: #f5f5f5;padding: 15px;clear: both;}
.tg-videomain .wvideos .row a{color: #184350;    background: #fff;padding: 5px;    display: block;}
.tg-videomain .wvideos .row a:hover{color:#d3b83a;;font-weight: bold; text-decoration: underline;}
.tg-videomain .wvideos .row a .glyphicon{font-size: 22px;vertical-align: sub;}
.btn-log-off{color: #fff !important; margin: 20px 0 0 0; background: #d3b83a;}
.btn-log-off:hover{text-decoration: none !important;}
/* .img-load{padding-top: 100px; display: block; margin: auto;} */

@media (max-width: 1200px){
    .tg-logo{width: 150px !important;}
   
}

@media (max-width: 767px){
    .tg-bannercontent h1{font-size: 20px;line-height: 30px;}
   
}

@media (max-width: 568px){
    .datos-menu{
        width: 100%;
        text-align: center;
        font-size: 10px;
    }
    body {
        padding: 198px 0 0;
    }    
    .tg-header {
        padding: 13px 0 0 0;
    }    
    body.tg-fixedme .tg-header{padding: 0;}

    .tg-main{padding: 0;}

    .tg-aboutus .tg-textshortcode{padding: 20px 0;}
    .wmapa{height: 400px !important;}
    .tg-headholder{padding: 0;}   
    .tg-datetime{font-size: 13px;} 
    .tg-videomain .tg-loginWrapper .login{width: 100%;} 
    .tg-videomain .previewVideoWrapper{height: 200px;}
    
}

.tg-testimonials:before {display: none;}
.tg-testimonials:after{display:none;}
.tg-bgtestimonials:before{display:none;}

.tg-testimonialcontent blockquote{font-size: 24px; line-height: 36px;}
.tg-clientname{font-weight: bold;font-size: 29px;}
.tg-clientname span{font-weight: initial; font-size: 14px;}


.tg-testimonialcontent > img{left: initial;bottom: initial; position: initial; margin: auto !important;}

@media (max-width: 360px){
    .wmapa{height: 300px !important;}    
}
@media (max-width: 320px){
    .tg-btn{padding: 0 25px;}
    .wmapa{height: 300px !important;}  
    .tg-datetime{font-size: 13px;}  
}

  
  </style>

<link rel="stylesheet" href="assets/frontend/css/customer.css">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-125827744-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-125827744-1');
</script>