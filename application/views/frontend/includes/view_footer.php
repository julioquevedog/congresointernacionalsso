<footer id="tg-footer" class="tg-footer tg-haslayout">

        <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="opacity: 1;color: #fff">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
								<div class="imgPonente"></div>
								<div class="textoEmpresa"></div>
								<div class="textoPonente"></div>
              </div>
              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left cancelar" data-dismiss="modal">Cerrar</button>
            	</div> -->
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
		<!-- /.modal -->
		

			<!--************************************
					Contact Us Start
			*************************************-->
			<!-- <div class="tg-contastus tg-bgcontactus tg-sectionspace">
				<h1>Want to ask something?</h1>
				<div class="container">
					<div class="row">
						<div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
							<div class="tg-sectionhead">
								<div class="tg-sectionheading">
									<span>Don’t worry we love to help</span>
									<h2>Get in touch with us</h2>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
							<form class="tg-formtheme tg-formcontactus tg-ajax-contact">
								<fieldset>
									<div class="form-group">
										<input type="text" name="contact[name]" class="form-control" placeholder="Full Name*">
									</div>
									<div class="form-group">
										<input type="email" name="contact[email]" class="form-control" placeholder="Email*">
									</div>
									<div class="form-group">
										<input type="text" name="contact[phone]" class="form-control" placeholder="Phone*">
									</div>
									<div class="form-group">
										<input type="text" name="contact[subject]" class="form-control" placeholder="Subject*">
									</div>
									<div class="form-group">
										<textarea name="contact[message]" placeholder="Message*"></textarea>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<button class="tg-btn submit-now" type="submit">Send Now</button>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div> -->
			<!--************************************
					Contact Us End
			*************************************-->
			<!--************************************
					Bottom Bar Start
			*************************************-->

			<div class="tg-footerbar">
				<?php
				if($seccion <> 'congreso-virtual'){
					?>				
					<a id="tg-btnscrolltop" class="tg-btnscrolltop" href="javascript:void(0);"><i class="icon-chevron-up"></i></a>
					<?php
				}
				?>
				<div class="container">
					<div class="row">
						<ul class="tg-socialicons pull-right">
							<!-- <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
							<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
							<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
							<li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
							<li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li> -->
						</ul>
						<p class="tg-copyrights pull-left">2018 All Rights Reserved © PHVAPERU SAC</p>
					</div>
				</div>
			</div>

			<!--************************************
					Bottom Bar End
			*************************************-->
		</footer>