    <script src="assets/frontend/js/vendor/jquery-library.js"></script>
	<script src="assets/frontend/js/vendor/bootstrap.min.js"></script>
	<!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyCgWHzkpa-cQXSlz62svASUlUnGEjreif4"></script> -->
	<script src="assets/frontend/js/jquery.singlePageNav.min.js"></script>
	<script src="assets/frontend/js/photoswipe-ui-default.js"></script>
	<script src="assets/frontend/js/customScrollbar.min.js"></script>
	<script src="assets/frontend/js/owl.carousel.min.js"></script>
	<script src="assets/frontend/js/photoswipe.min.js"></script>
	<script src="assets/frontend/js/prettyPhoto.js"></script>
	<script src="assets/frontend/js/tilt.jquery.js"></script>
	<script src="assets/frontend/js/countdown.js"></script>
	<script src="assets/frontend/js/parallax.js"></script>
	<!-- <script src="assets/frontend/js/gmap3.js"></script> -->
	<script src="assets/frontend/js/main.js?<?php echo time();?>"></script>
	<script src="assets/frontend/js/funciones_ajax.js"></script>

	<?php 
	$fecha_inicio	= str_replace("-","/",$evento['fecha_inicio']);
	
	?>
	
	<script>
	
$(document).ready(function(){
	
	var URLactual = window.location;

	 if(URLactual.href.indexOf('loginVideo')>0){
		 setTimeout(() => {
			var URLactual = window.location;
			if(URLactual.href.indexOf('loginVideo')>0){
				$('.tg-navigation ul li a[href="#loginVideo"]').click();
			}			 
		 }, 2500);
	 }
	
})

	$(document).on('click','.modal_ponente',function(){
		var nombre=$(this).data('nombre');
		var cargo=$(this).data('cargo');    
		var id=$(this).data('id');        		
		var empresa=$(this).data('empresa');
		var link_empresa=$(this).data('link-empresa');
		var descripcion=$('#li_ponentes #descripcion_ponente_'+id).html();        		
		var imagen=$(this).data('imagen');        		
		var elementoImg = '<img src="./files/ponentes/'+imagen+'">'
		var textoEmpresa = '<h3><a href="'+link_empresa+'" target="_blank">'+empresa+'</a></h3>'

		$('.modal-header .modal-title').html(nombre);
		$('.modal-body .textoPonente').html(descripcion);
		// $('.modal-body .textoEmpresa').html(textoEmpresa);
		$('.modal-body .imgPonente').html(elementoImg);
		//$('.modal-body .imgPonente').append(textoEmpresa);
		

		$('#modal-default').modal('show');       

	});		
	/* -------------------------------------
			NEXT EVENT COUNTER
	-------------------------------------- */
	var _tg_upcomingeventcounter = jQuery('#tg-upcomingeventcounter');
	_tg_upcomingeventcounter.countdown('<?php echo $fecha_inicio;?>', function(event) {
		var $this = jQuery(this).html(event.strftime(''
			+ '<div class="tg-eventcounter"><span>%-D</span><span> Días restantes</span></div>'
			+ '<div class="tg-eventcounter"><span>%H</span><span>Horas</span></div>'
			+ '<div class="tg-eventcounter"><span>%M</span><span>Minutos</span></div>'
			+ '<div class="tg-eventcounter"><span>%S</span><span>Segundos</span></div>'
		));
	});
	</script>