<!DOCTYPE html>

<head>
    <title>Zoom WebSDK</title>
    <meta charset="utf-8" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.10/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.10/css/react-select.css" />
    <meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<style>
		body,#wc-header{
			min-width: inherit !important;
			overflow: hidden !important;
		}
		#zmmtg-root{
			background: url('assets/frontend/images/zoom_loading.gif') no-repeat;
			background-position: center;
			background-color: #1a1a1a !important;
		}
	</style>

</head>

<body>
    <script src="https://source.zoom.us/1.7.10/lib/vendor/react.min.js"></script>
    <script src="https://source.zoom.us/1.7.10/lib/vendor/react-dom.min.js"></script>
    <script src="https://source.zoom.us/1.7.10/lib/vendor/redux.min.js"></script>
    <script src="https://source.zoom.us/1.7.10/lib/vendor/redux-thunk.min.js"></script>
    <script src="https://source.zoom.us/1.7.10/lib/vendor/jquery.min.js"></script>
	<script src="https://source.zoom.us/1.7.10/lib/vendor/lodash.min.js"></script>
	
    <script src="assets/frontend/js/zoom/zoom-meeting-1.7.9.1.min.js"></script>
    <script src="assets/frontend/js/zoom/tool.js"></script>
    <script src="assets/frontend/js/zoom/vconsole.min.js"></script>
	<script src="assets/frontend/js/zoom/meeting.js"></script> 
</body>

</html>