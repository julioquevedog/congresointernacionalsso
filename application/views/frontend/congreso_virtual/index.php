<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang="zxx"> <!--<![endif]-->
<html lang="es" style="overflow: auto !important;">
<head>
    <?php echo $view_head;?>
</head>
<style>
	html{
		overflow: auto !important;
	}
	#zmmtg-root{
		display: none !important;
	}
	.tg-askquestions:before {
		top: 0;
		right: 0;
		content: '';
		height: 100%;
		width: 43%;
		position: absolute;
		background: url(./files/informativa/<?php echo $txt_has_pregunta['imagen']?>) no-repeat;
		background-size: cover;
	}	
    .textoPonente{
        text-align: justify;
        font-weight: 600;
    }	
    .tg-sectionhead .tg-description{
        text-align: justify;
    }
    .tg-btn{
        color: #fff !important;
    }
</style>

<body id="body" class="tg-home tg-homeone body-congreso">
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Preloader Start
	*************************************-->
	<div id="status">
		<div id="preloader" class="preloader">
			<img src="assets/frontend/images/Loader.gif" alt="Preloader" />
		</div>
	</div>
	<!--************************************
			Preloader End
	*************************************-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="tg-wrapper" class="tg-wrapper tg-haslayout">

		<!--************************************
				Header Start
		*************************************-->
		<?php 
			$usu_congreso_virtual = $this->session->userdata('usu_congreso_virtual');
		?>
		<header>        
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<img src="./assets/frontend/images/logo.png" alt="Congreso Internacional" class="logo">
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<div class="usuario-congreso">
							<h1>Bienvenido: <?php echo $usu_congreso_virtual['nombres'].' '.$usu_congreso_virtual['apellidos']?></h1>
						</div>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
						<a class="btn tg-btnbecommember tg-btn__white txt-red" href="congreso-virtual/log-off">CERRAR SESIÓN</a>						
					</div>
				</div>
			</div>
        </header>
		<!--************************************
				Header End
		*************************************-->            


		<!--************************************
				Main Start
		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">

				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div id="zona-zoom">
								<img src="assets/frontend/images/zoom_loading.gif" alt="" class="load-zoom">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<h3>Programa CISSO</h3>
							<div class="row">	

								<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">						
									<div class="tg-speakerslider">
										<div id="tg-speakerslider" class="tg-testimonialslider tg-testimonial owl-carousel">
											<?php
											foreach ($ponentes as $key => $value) {
												?>
												<div class="item">
													<div class="tg-speakerslidercontent">
														<div class="row">
															<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 tg-speakerslidercontent-img">
																<img src="files/ponentes/<?php echo $value['imagen']?>" alt="<?php echo $value['nombres']?>">
															</div>
															<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 tg-speakerslidercontent-desc">
																<h4><?php echo $value['nombres']?></h4>
																<p><?php echo $value['sumilla']?></p>
															</div>
														</div>
													</div>
												</div>
												<?php
											}
											?>
										</div>
									</div>
								</div>
								<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
									<a href="files/eventos/<?php echo $evento['archivo']?>" target = "_blank">
										<img src="assets/frontend/images/download_pdf.jpg" alt="" class="congreso-virtual-pdf">
									</a>
								</div>								
							</div>
							<div class="row">	
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">						<div class="tg-congreso-sponsor">
										<div id="tg-congreso-sponsor" class="tg-congreso-sponsor owl-carousel">
											<?php
											foreach ($sponsor as $key => $value) {
												?>
												<div class="item">
													<div class="tg-congreso-sponsorcontent">
														<img src="files/sponsor/<?php echo $value['imagen']?>" alt="<?php echo $value['title']?>">
													</div>
												</div>
												<?php
											}
											?>
										</div>
									</div>	
								</div>		
							</div>
						</div>							
					</div>
				</div>
			
				<div class="franja">
					<h2>Feria Internacional de Seguridad y Salud en el trabajo</h2>
				</div>
			
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<div class="stands-slider">
							<?php
							$c = 0;
							foreach ($stands as $key => $value) {
								$active = ($key == 0) ? 'stands-slider-item__active':'';
								$stand_id = $value['id'];
								$c++;
								?>
								<div class="stands-slider-item" data-nombre="<?php echo $value['nombre']?>" data-stand-id = "<?php echo $stand_id?>" data-num = "<?php echo $c?>" data-email="<?php echo $value['email']?>" data-celular = "<?php echo $value['celular']?>">
									<?php
									if(is_file('files/stands/'.$value['logo']))
									{
										$img = getimagesize('files/stands/'.$value['logo']);
										$ancho = (int)($img[0]/1);
										$alto = (int)($img[1]/1);                            
										$pic1 = '<img src="files/stands/'.$value['logo'].'" border="0" width="'.$ancho.'" alt="'.$value['nombre'].'" class="'.$active.'"/>';
									}
									else
									{
										$img = getimagesize('assets/frontend/images/imagen-estandar-logo.jpg');
										$ancho = (int)($img[0]/1);
										$alto = (int)($img[1]/1);                            
										$pic1 = '<img src="assets/frontend/images/imagen-estandar-logo.jpg" border="0" width="'.$ancho.'"  alt="'.$value['nombre'].'" class="'.$active.'"/>';
									}
									echo $pic1;
									?>										
								</div>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="stands-content">
					<div class="stands-content-nombre">
						<h3 class="stands-content-name"><span><?php echo $stands[0]['nombre']?></span></h3>
					</div>
					<div class="stands-content-box">
						<?php
						$c = 0;
						foreach ($stands as $key => $value) {
							$stand_id = $value['id'];
							$dnone = ($key == 0)? '' : 'display:none';
							$c++;
							?>
							
								<div class="stands-content-item stands-content-banner group_service_<?php echo $stand_id;?>" style="<?php echo $dnone;?>">
									
									<div class="stands-content-banner-redes">
										<a href="<?php echo $value['url_web']?>" target="_blank" class="stands-content-banner-redes-web"><img src="./assets/frontend/images/icon_web.png" alt="Url Web"></a>
										<a href="<?php echo $value['url_facebook']?>" target="_blank" class="stands-content-banner-redes-face"><img src="./assets/frontend/images/icon_facebook.png" alt="Facebook"></a>
										<a href="<?php echo $value['url_twitter']?>" target="_blank" class="stands-content-banner-redes-twitter"><img src="./assets/frontend/images/icon_twitter.png" alt="Twitter"></a>
										<a href="<?php echo $value['url_instagram']?>" target="_blank" class="stands-content-banner-redes-instagram"><img src="./assets/frontend/images/icon_instagram.png" alt="Instagram"></a>
									</div>
									<div class="stands-content-banner-img">
									<?php
										if(is_file('files/stands/'.$value['imagen']))
										{
											$img = getimagesize('files/stands/'.$value['imagen']);
											$ancho = (int)($img[0]/1);
											$alto = (int)($img[1]/1);                            
											$pic1 = '<img src="files/stands/'.$value['imagen'].'" border="0" width="'.$ancho.'" alt="'.$value['nombre'].'"/>';
										}
										else
										{
											$img = getimagesize('assets/frontend/images/imagen-estandar-banner.jpg');
											$ancho = (int)($img[0]/1);
											$alto = (int)($img[1]/1);                            
											$pic1 = '<img src="assets/frontend/images/imagen-estandar-banner.jpg" border="0" width="'.$ancho.'"  alt="'.$value['nombre'].'"/>';
										}
										echo $pic1;
									?>								
									</div>
								</div>
								<div class="stands-content-item stands-content-servicios group_service_<?php echo $stand_id;?>" style="<?php echo $dnone;?>">
											<div id="tg-services-stands-<?php echo $c;?>" class="tg-services-stands owl-carousel">
												<?php
												foreach ($servicios_stands as $k => $val) {
													if($stand_id == $val['stand_id']){
													?>
													<div class="item">
														<div class="tg-services-stands-content">
															<div class="tg-services-stands-content-img">
																<?php
																if(is_file('files/servicios_stands/'.$val['imagen']))
																{
																	$img = getimagesize('files/servicios_stands/'.$val['imagen']);
																	$ancho = (int)($img[0]/1);
																	$alto = (int)($img[1]/1);                            
																	$pic1 = '<img src="files/servicios_stands/'.$val['imagen'].'" border="0" width="'.$ancho.'" alt="'.$val['nombre'].'"/>';
																}
																else
																{
																	$img = getimagesize('assets/frontend/images/imagen-estandar-servicio-stand.jpg');
																	$ancho = (int)($img[0]/1);
																	$alto = (int)($img[1]/1);                            
																	$pic1 = '<img src="assets/frontend/images/imagen-estandar-servicio-stand.jpg" border="0" width="'.$ancho.'"  alt="'.$val['nombre'].'"/>';
																}
																echo $pic1;															
																?>
															</div>
															
															<div class="tg-services-stands-content-nombre">
																<h4><?php echo $val['nombre']?></h4>
																<?php 
																if($val['pdf'] <> null){?>
																	<span><a href="files/servicios_stands_pdf/<?php echo $val['pdf']?>" target="_blank"><img src="assets/frontend/images/download_pdf.jpg" alt=""></a></span>
																	<?php
																}
																?>
															</div>
															<div class="tg-services-stands-content-sumilla">
																<p><?php echo $val['sumilla']?></p>
																<?php if(trim($val['link_pago']) <> ""){
																	?>
																	<a href="<?php echo $val['link_pago']?>" target="_blank" class="enlace_pago"><img src="assets/frontend/images/icon-pago.jpg" alt=""></a>
																	<?php
																}
																?>
															</div>
															
														</div>
													</div>
													<?php
													}
												}
												?>
											</div>	
								</div>
							<?php				
						}
						?>

						<div class="stands-content-item stands-content-contacto">
							<h4>Envíanos tu consulta <i class="icon-mail4"></i></h4>
							<form action="" method="post" id="form_contacto">
								<div class="form-group">
									<input type="email" name="correo" class="form-control" 
									placeholder="Correo electr&oacute;nico" required autocomplete="off" value="<?php echo $usu_congreso_virtual['correo']?>">
								</div>
								<div class="form-group">
									<input type="text" name="celular" class="form-control" 
									placeholder="Celular" required autocomplete="off" value="<?php echo $usu_congreso_virtual['celular']?>">
								</div>	
								<div class="form-group">
									<textarea name="consulta" class="form-control" placeholder="Escribe aquí tu consulta" required></textarea>
								</div>										
								<button type="submit" class="tg-btn tg-btn__guinda">ENVIAR</button>
								<input type="hidden" name="email_stand" value="<?php echo $stands[0]['email']?>">
								<input type="hidden" name="celular_stand" value="<?php echo $stands[0]['celular']?>">
								<input type="hidden" name="nombres" value="<?php echo 
								$usu_congreso_virtual['nombres'].' '.$usu_congreso_virtual['apellidos']?>">
								<input type="hidden" name="msg-whatsapp" value="Buenos días por favor requiero información de los servicio que ofrecen. Muchas gracias.">
								<div class="alert alert-success alert-success-contact" style="display:none;"><i class="icon-checkmark"></i> Enviado</div>
							</form>
							<div class="stands-content-contacto-wa">
								<a href="" class="enlace-whatsapp" target="_blank"><img src="./assets/frontend/images/icon_whatsapp.jpeg" alt=""></a>
							</div>
								
						</div>
					</div>										
				</div>
			</div>
			
			<input type="hidden" name="display_name" id="display_name" value="<?php echo $usu_congreso_virtual['nombres'].' '.$usu_congreso_virtual['apellidos']?>">
			<input type="hidden" name="meeting_number" id="meeting_number" value="<?php echo getConfig('meeting_number_zoom');?>">			
			<input type="hidden" name="meeting_pwd" id="meeting_pwd" value="<?php echo getConfig('meeting_pwd');?>">
			<input type="hidden" name="meeting_email" id="meeting_email" value="">
			<select id="meeting_role" style="display:none">
                                    <option value=0>Attendee</option>
                                    <!-- <option value=1>Host</option> -->
                                    <!-- <option value=5>Assistant</option> -->
								</select>		
			<select id="meeting_china" style="display:none"><option value=0>Global</option>
			<!-- <option value=1>China</option> -->
			</select>
			<select id="meeting_lang" style="display:none">
			<!-- <option value="en-US">English</option>
			<option value="de-DE">German Deutsch</option> -->
			<option value="es-ES">Spanish Español</option>
			<!-- <option value="fr-FR">French Français</option>
			<option value="jp-JP">Japanese 日本語</option>
			<option value="pt-PT">Portuguese Portuguese</option>
			<option value="ru-RU">Russian Русский</option>
			<option value="zh-CN">Chinese 简体中文</option>
			<option value="zh-TW">Chinese 繁体中文</option>
			<option value="ko-KO">Korean 한국어</option>
			<option value="vi-VN">Vietnamese Tiếng Việt</option>
			<option value="it-IT">Italian italiano</option> -->
			</select>	
					
		</main>
		<!--************************************
				Main End
		*************************************-->

		<!--************************************
				Footer Start
		*************************************-->
		<?php echo $view_footer; ?>
		<!--************************************
				Footer End
		*************************************-->                
    </div>
    <!--************************************
            Wrapper End
    *************************************-->

    <?php 
    //echo $view_foot;
    ?>

	<!--******** ZOOM *********************--> 
	<script src="https://source.zoom.us/1.7.10/lib/vendor/jquery.min.js"></script>
	
	<script defer src="https://source.zoom.us/1.7.10/lib/vendor/react.min.js"></script>
    <script defer src="https://source.zoom.us/1.7.10/lib/vendor/react-dom.min.js"></script>
    <script defer src="https://source.zoom.us/1.7.10/lib/vendor/redux.min.js"></script>
    <script defer src="https://source.zoom.us/1.7.10/lib/vendor/redux-thunk.min.js"></script>
    
    <script defer src="https://source.zoom.us/1.7.10/lib/vendor/lodash.min.js"></script>

    <script defer src="assets/frontend/js/zoom/zoom-meeting-1.7.9.1.min.js"></script>
    <script defer src="assets/frontend/js/zoom/tool.js"></script>
    <script defer src="assets/frontend/js/zoom/vconsole.min.js"></script>
	<script defer src="assets/frontend/js/zoom/index.js"></script>
	
	<script src="assets/frontend/js/owl.carousel.min.js"></script>

	<script>

	/* -------------------------------------
	 PRELOADER
	 -------------------------------------- */
	 $("#status").delay(2000).fadeOut();
	$("#preloader").delay(2000).fadeOut("slow");

	$(".stands-slider-item").click(function(){
		$(".stands-slider-item img").removeClass("stands-slider-item__active");
		$(this).children("img").addClass("stands-slider-item__active");

		var nombre = $(this).data("nombre");
		var stand_id = $(this).data("stand-id");
		var num  = $(this).data("num");
		var celular  = $(this).data("celular");
		var email  = $(this).data("email");

		$("input[name='celular_stand']").val(celular);
		$("input[name='email_stand']").val(email);

		$(".stands-content-name span").hide();
		$(".stands-content-name span").html(nombre);
		$(".stands-content-name span").fadeIn();

		$(".stands-content-servicios").hide();
		$(".stands-content-banner").hide();
		$(".group_service_"+stand_id).fadeIn(1000);

		$(".stands-content-contacto").hide();
		$(".stands-content-contacto").fadeIn(1000);

		var celular_stand = $("input[name='celular_stand']").val();
		var msg_whatsapp = $("input[name='msg-whatsapp']").val();
		// https://api.whatsapp.com/send?phone=51992852798&text=Buenos%20d%C3%ADas%20porfavor%20requiero%20informaci%C3%B3n%20de%20los%20servicio%20que%20ofrecen.%20Muchas%20gracias.
		var href = "https://api.whatsapp.com/send?phone=51"+celular_stand+"&text="+msg_whatsapp;
		$(".enlace-whatsapp").attr("href",href);

		var _tg_testimonialslider = jQuery("#tg-services-stands-"+num)
		_tg_testimonialslider.owlCarousel({
			items:1,
			loop:true,
			autoplayTimeout:3000,
			nav:false,
			animateIn: "fadeIn",
			autoplay:true,
			responsive:{
				0:{
					items:1,
				},
			}		
		});				
	})

	var celular_stand = $("input[name='celular_stand']").val();
	var msg_whatsapp = $("input[name='msg-whatsapp']").val();
	var href = "https://api.whatsapp.com/send?phone=51"+celular_stand+"&text="+msg_whatsapp;
	$(".enlace-whatsapp").attr("href",href);
	
	
	$( "#form_contacto" ).on( "submit", function( event ) {
		event.preventDefault();
		var data =$( this ).serializeArray();
		var o = {};
		$.each(data, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		
        var html = "Solicitud de servicio - CISSO 2020, datos del solicitante:<br><br>";
        html+= "Nombres: "+o.nombres+"<br>";
        html+= "Correo: "+o.correo+"<br>";
        html+= "Celular: "+o.celular+"<br>";
        html+= "Mensaje: "+o.consulta+"<br><br><br>";
		html+= "<strong>Atte.<br>PHVA PERÚ</strong>";  		
		
		// "to":o.email_stand,
		//"to":"julioquevedog@gmail.com",
        var data = {
            "html":html,
            "to":o.email_stand,
            "toname":"Cliente PHVA PERÚ",
            "from":"informes@phvaperu.com",
            "fromname":"CISSO 2020",
            "subject":"Solicitud de servicio - CISSO 2020"
		};
			
		$.ajax({
			type : 'POST',
			url : 'frontend/controller_ajax/send_email_sendgrid',
			data : {data:data},
			dataType : 'json',
			success: function(json) {
				result=json.result;
				if(result==1){
					$(".alert-success-contact").fadeIn(1000);
						setTimeout(function(){
							$(".alert-success-contact").fadeOut(1000);
						}, 3000);
				}          
			}
		})
	});


	/* -------------------------------------
			EXPONENTES SLIDER
	-------------------------------------- */
	var _tg_testimonialslider = jQuery("#tg-speakerslider")
	_tg_testimonialslider.owlCarousel({
		items:1,
		loop:true,
		autoplayTimeout:4000,
		nav:false,
		animateIn: "fadeIn",
		autoplay:true,
		responsive:{
			0:{
				items:1,
			},
		}		
	});	

	/* -------------------------------------
			SERVICES STANDS SLIDER
	-------------------------------------- */	
	var _tg_testimonialslider = jQuery("#tg-services-stands-1")
	_tg_testimonialslider.owlCarousel({
		items:1,
		loop:true,
		autoplayTimeout:5000,
		nav:false,
		animateIn: "fadeIn",
		autoplay:true,
		responsive:{
			0:{
				items:1,
			},
		}		
	});		
	
	/* -------------------------------------
			SPONSOR CONGRESO
	-------------------------------------- */
	var _tg_congreso_sponsor = jQuery("#tg-congreso-sponsor")
	_tg_congreso_sponsor.owlCarousel({
		loop:true,
		nav:false,
		animateIn: "fadeIn",
		autoplay:true,
		autoplayTimeout:2000,
		responsiveClass:true,
		responsive:{
			0:{
				items:1,
			},
			480:{
				items:2,
			},
			768:{
				items:3,
			},
			992:{
				items:3,
			}
		}
	});
		
	</script>
</body>
</html>