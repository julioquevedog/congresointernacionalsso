<!DOCTYPE html>

<head>
    <title>Zoom WebSDK</title>
    <meta charset="utf-8" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.10/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="https://source.zoom.us/1.7.10/css/react-select.css" />
    <meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<style>
		html,body{
			background: #000;
		}
		body{

		}
		.centro{
			width: 200px;
			margin: auto;
			margin-top: 100px;
		}
		.centro a{
			margin: auto;
			display: block;
		}
		
	</style>

</head>
<?php
	$url="http://localhost/congresointernacional-sso/";
?>
<body>
	<div class="centro">
		<img src="<?php echo $url;?>assets/frontend/images/zoom-logo.png" width="200" alt="">
		<a href="javascript: void(0)" class="btn btn-primary reload">Ingresar a Zoom</a>  
	</div>

</body>
<body>
    <script src="https://source.zoom.us/1.7.10/lib/vendor/jquery.min.js"></script>
</body>
<script>
	$(".reload").click(function(){
		var href = window.location.href;
		var queryString = href.split("?");
      
		var str = queryString[1];
		window.location.href = "http://localhost/congresointernacional-sso/meeting?"+str;		
	});
</script>
</html>