<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang="zxx"> <!--<![endif]-->
<html lang="es">
<head>
    <?php echo $view_head;?>
    <link rel="stylesheet" href="assets/frontend/css/customer.css">
</head>
<style>
	.tg-askquestions:before {
		top: 0;
		right: 0;
		content: '';
		height: 100%;
		width: 43%;
		position: absolute;
		background: url(./files/informativa/<?php echo $txt_has_pregunta['imagen']?>) no-repeat;
		background-size: cover;
	}	
    .textoPonente{
        text-align: justify;
        font-weight: 600;
    }	
    .tg-sectionhead .tg-description{
        text-align: justify;
    }
    .tg-btn{
        color: #fff !important;
    }
</style>

<body id="body" class="tg-home tg-homeone">
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->


	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="tg-wrapper" class="tg-wrapper tg-haslayout">
		<!--************************************
				SideBar Navigation Start
		*************************************-->
		<div class="tg-sidenavholder">
			<div id="tg-sidenav" class="tg-sidenav">
				<a href="javascript:void(0);" class="tg-close"><i class="icon-cross"></i></a>
				<div id="tg-navscrollbar" class="tg-navscrollbar">
					<div class="tg-navhead">
						<strong class="tg-logo">
							<a href="http://www.phvaperu.com/" target="_blank"><img src="assets/frontend/images/phvaPeruLogo.png" alt="www.phvaperu.com"></a>
						</strong>
					</div>
					<nav id="tg-navtwo" class="tg-nav">
						<div id="tg-sidenavigation" class="tg-navigation2">
							<ul>
								<li><a href="http://www.phvaperu.com/" target="_blank">Inicio</a></li>
								<li><a href="http://phvaperu.com/conocenos-phvaperu" target="_blank">La empresa</a></li>
								<li><a href="http://phvaperu.com/nuestro-staff" target="_blank">Staff</a></li>
								<li><a href="http://phvaperu.com/servicios-phvaperu" target="_blank">Servicios</a></li>
								<li><a href="http://phvaperu.com/articulos-phvaperu" target="_blank">Artículos</a></li>
								<li><a href="http://phvaperu.com/contactenos" target="_blank">Contáctenos</a></li>								
							</ul>
						</div>
					</nav>
					<div class="tg-sidenavbottom">
						<img src="assets/frontend/images/icon.png" alt="<?php echo $evento['nombre']?>">
						<ul class="tg-socialicons">
							<li class="tg-facebook"><a href="https://www.facebook.com/phvaperu/" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<!-- <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
								<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
								<li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
								<li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li> -->
						</ul>
						<p class="tg-copyrights">2018 All Rights Reserved By<span>© PHVA PERU SAC</span></p>
					</div>
				</div>
			</div>
		</div>
		<!--************************************
				SideBar Navigation End
		*************************************-->

		<!--************************************
				Header Start
		*************************************-->
		<header id="tg-header" class="tg-header">        
            <?php echo $view_header;?>
        </header>
		<!--************************************
				Header End
		*************************************-->            


		<!--************************************
				Main Start
		*************************************-->
		<main id="" class="tg-haslayout">
			<div class="congreso-login">
				<div class = "congreso-login-portada">

				</div>
				<div class="congreso-login-form">
						<div class="congreso-form">
							<h1>BIENVENIDO AL <?php echo $evento['nombre']?></h1>
							<p>Accede al congreso y vive el mejor evento del 2020</p>
							<?php 
							if($this->session->flashdata("error_login_congreso_virtual")){
								echo '<div class="alert alert-danger" style="clear: both;"><i class="glyphicon glyphicon-remove"></i> '.$this->session->flashdata("error_login_congreso_virtual").'</div>';
							}									
							?>							
							<h3>Iniciar Sesi&oacute;n</h3>
							<form action="congreso-virtual/process-logueo" method="post">
								<div class="form-group">
									<input type="email" name="correo" class="form-control" 
									placeholder="Correo electr&oacute;nico" required autocomplete="off">
								</div>
								<div class="form-group">
									<input type="password" name="password" class="form-control" placeholder="Contraseña" required autocomplete="off">
								</div>
								<button type="submit" class="tg-btn tg-btn__guinda">INGRESAR</button>   
								<a href="congreso-virtual/forget">¿ Constraseña olvidada ?</a>
							</form>
						</div>
						<div class="congreso-sponsor">
							<ul>
								<?php
								foreach ($sponsors as $key => $value) {
									?>
									<li>
										<div class = "">
											<img src="files/sponsor/<?php echo $value['imagen']?>" alt="<?php echo $value['title']?>">
										</div>
									</li>								
									<?php
								}
								?>
							</ul>
						</div>
				</div>            
		</main>
		<!--************************************
				Main End
		*************************************-->

		<!--************************************
				Footer Start
		*************************************-->
		<?php echo $view_footer; ?>
		<!--************************************
				Footer End
		*************************************-->                
    </div>
    <!--************************************
            Wrapper End
    *************************************-->

    <?php 
    echo $view_foot;
    ?>


</body>
</html>