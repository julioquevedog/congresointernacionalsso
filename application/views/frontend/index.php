<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang="zxx"> <!--<![endif]-->
<html lang="es">
<head>
    <?php echo $view_head;?>

</head>
<style>
	.tg-askquestions:before {
		top: 0;
		right: 0;
		content: '';
		height: 100%;
		width: 43%;
		position: absolute;
		background: url(./files/informativa/<?php echo $txt_has_pregunta['imagen']?>) no-repeat;
		background-size: cover;
	}	
    .textoPonente{
        text-align: justify;
        font-weight: 600;
    }	
    .tg-sectionhead .tg-description{
        text-align: justify;
    }
    .tg-btn{
        color: #fff !important;
    }
</style>

<body id="body" class="tg-home tg-homeone">
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
	<!--************************************
			Preloader Start
	*************************************-->
	<div id="status">
		<div id="preloader" class="preloader">
			<img src="assets/frontend/images/Loader.gif" alt="Preloader" />
		</div>
	</div>
	<!--************************************
			Preloader End
	*************************************-->
	<!--************************************
			Wrapper Start
	*************************************-->
	<div id="tg-wrapper" class="tg-wrapper tg-haslayout">
		<!--************************************
				SideBar Navigation Start
		*************************************-->
		<div class="tg-sidenavholder">
			<div id="tg-sidenav" class="tg-sidenav">
				<a href="javascript:void(0);" class="tg-close"><i class="icon-cross"></i></a>
				<div id="tg-navscrollbar" class="tg-navscrollbar">
					<div class="tg-navhead">
						<strong class="tg-logo">
							<a href="http://www.phvaperu.com/" target="_blank"><img src="assets/frontend/images/phvaPeruLogo.png" alt="www.phvaperu.com"></a>
						</strong>
					</div>
					<nav id="tg-navtwo" class="tg-nav">
						<div id="tg-sidenavigation" class="tg-navigation2">
							<ul>
								<li><a href="http://www.phvaperu.com/" target="_blank">Inicio</a></li>
								<li><a href="http://phvaperu.com/conocenos-phvaperu" target="_blank">La empresa</a></li>
								<li><a href="http://phvaperu.com/nuestro-staff" target="_blank">Staff</a></li>
								<li><a href="http://phvaperu.com/servicios-phvaperu" target="_blank">Servicios</a></li>
								<li><a href="http://phvaperu.com/articulos-phvaperu" target="_blank">Artículos</a></li>
								<li><a href="http://phvaperu.com/contactenos" target="_blank">Contáctenos</a></li>								
							</ul>
						</div>
					</nav>
					<div class="tg-sidenavbottom">
						<img src="assets/frontend/images/icon.png" alt="<?php echo $evento['nombre']?>">
						<ul class="tg-socialicons">
							<li class="tg-facebook"><a href="https://www.facebook.com/phvaperu/" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<!-- <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
								<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
								<li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
								<li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li> -->
						</ul>
						<p class="tg-copyrights">2018 All Rights Reserved By<span>© PHVA PERU SAC</span></p>
					</div>
				</div>
			</div>
		</div>
		<!--************************************
				SideBar Navigation End
		*************************************-->

		<!--************************************
				Header Start
		*************************************-->
		<header id="tg-header" class="tg-header">        
            <?php echo $view_header;?>
        </header>
		<!--************************************
				Header End
		*************************************-->            

		<!--************************************
				Home Slider Start
		*************************************-->
		<div id="tg-homebanner" class="tg-homebanner tg-haslayout">
			<figure class="tg-themepostimg">
				<img src="files/eventos/<?php echo $evento['imagen']?>" alt="<?php echo $evento['nombre']?>">
				<figcaption>
					<div class="tg-bannercontent" data-tilt>
						<span class="tg-datetime"><?php echo $evento['fecha_mostrar']?></span>
						<h1><?php echo $evento['frase']?></h1>
						<div class="tg-speakerinfo">
							<div class="tg-authorholder">
								<img src="files/eventos/<?php echo $evento['imagen_planner']?>" alt="<?php echo $evento['nombre']?>">
								<div class="tg-authorcontent">
									<div class="tg-speakername">
										<span class="tg-eventcatagory"><?php echo $evento['txt1']?></span>
										<h2><?php echo $evento['txt2']?></h2>
										<a class="tg-btnviewall" href="http://phvaperu.com" target="_blank">Visitar web</a>
									</div>
								</div>
							</div>
							<div class="tg-btnwhite">
								<a class="tg-btn" href="https://phvaperu.com/inscripciones/idf/pujjPOj2PyEXZYng2wgk11ijdqrdGSNRMiytq65BpZnaAOOYQfcKrFFQyDPC" 
							target = "_blank">Reserva tu asiento</a>
								<a class="tg-btn" href="#tg-schedule">Ver la programación</a>
							</div>
						</div>
					</div>
				</figcaption>
			</figure>
		</div>
		<!--************************************
				Home Slider End
		*************************************-->

		<!--************************************
				Main Start
		*************************************-->
		<main id="tg-main" class="tg-main tg-haslayout">
			<!--************************************
					About Us Start
			*************************************-->
			<section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="tg-aboutus">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="tg-textshortcode">
									<div class="tg-sectionhead tg-textalignleft">
										<div class="tg-sectionheading">
											<span><?php echo $txt_conocenos['txt1']?></span>
											<h2><?php echo $txt_conocenos['titulo']?></h2>
										</div>
										<div class="tg-description">
											<p><?php echo $txt_conocenos['txt4']?></p>
										</div>
										<div class="tg-btnarea">
											<a class="tg-btn" href="https://phvaperu.com/inscripciones/idf/pujjPOj2PyEXZYng2wgk11ijdqrdGSNRMiytq65BpZnaAOOYQfcKrFFQyDPC" 
							target = "_blank">Reserva tu asiento</a>
											<!-- <a class="tg-btn" href="#">Meet The Team</a> -->
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="tg-videoarea">
									<figure>
										<img src="./files/informativa/<?php echo $txt_conocenos['imagen']?>" alt="<?php echo $txt_conocenos['titulo']?>">
										<figcaption><a href="<?php echo $txt_conocenos['txt2']?>" class="tg-btnplay" data-rel="prettyPhoto[gallery]">
										    <img src="assets/frontend/images/btn-playicon.jpg" alt="<?php echo $txt_conocenos['titulo']?>"></a></figcaption>
									</figure>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					About Us End
			*************************************-->
			<!--************************************
					Event Counter Start
			*************************************-->
			<section class="tg-haslayout">
				<div class="container-fluid">
					<div class="row">
						<div class="tg-counterarea">
							<div class="tg-eventinfo">
								<figure class="tg-themepostimg">
									<img src="./files/informativa/<?php echo $txt_time_rever['imagen']?>" alt="<?php echo $txt_time_rever['titulo']?>">
									<figcaption>
										<!-- <a class="tg-btnaddtocalendar tg-tooltip" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Add to calendar">
											<i class="icon-calendar-full"></i>
										</a> -->
										<time class="tg-timedate" datetime="<?php echo $evento['fecha_inicio']?>"><?php echo $evento['fecha_mostrar']?></time>
										<h2><?php echo $txt_time_rever['titulo']?></h2>
									</figcaption>
								</figure>
							</div>
							<div id="tg-upcomingeventcounter" class="tg-upcomingeventcounter"></div>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Event Counter End
			*************************************-->
			<!--************************************
					Confrences Start
			*************************************-->
			<section id="tg-schedule" class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="tg-eventconfrences">
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<div class="tg-headholder">
									<div class="tg-sectionhead tg-textalignleft">
										<div class="tg-sectionheading">
											<span><?php echo $txt_evento['txt1'];?></span>
											<h2><?php echo $txt_evento['titulo'];?></h2>
										</div>
										<div class="tg-description">
											<p><?php echo $txt_evento['txt4'];?></p>
										</div>
										<?php if($evento['estado_brochure']==1){?>
										<a class="tg-btndownload" href="brochure/<?php echo $evento['id'];?>">
											<!-- <i class="icon-calendar-full"></i> -->
											<img src="files/informativa/<?php echo $txt_evento['imagen'];?>" alt="">
											<span><?php echo $txt_evento['txt2'];?></span>
										</a>
										<?php
										}
										?>
									</div>
								</div>
								<div class="tg-eventscheduletabs">
									<ul class="tg-eventschedulenav" role="tablist">
										<?php
										foreach ($programacion as $key => $value) {
											$active = ($key==0) ? 'active':'';
											?>
											<li role="presentation" class="<?php echo $active;?>"><a href="#day-<?php echo $value['id']?>" aria-controls="day-one" role="tab" data-toggle="tab"><?php echo $value['texto1']?><span><?php echo $value['texto2']?></span></a></li>
											<?php
										}
										?>
									</ul>
									<div class="tg-eventschedulecontent tab-content">
										<?php
										foreach ($programacion as $key => $value) {
											$active = ($key==0) ? 'active':'';
											?>
											<div role="tabpanel" class="tab-pane <?php echo $active;?>" id="day-<?php echo $value['id']?>">
												<div class="tg-eventschaduletime">
													<h2><?php echo $value['texto1']?></h2>
													<h3><?php echo $value['texto3']?></h3>
												</div>
												<div class="tg-eventvenuetabs">
													<ul class="tg-eventvenuenav" role="tablist">
														<?php
														foreach ($bloques as $keyb => $valueb) {
															if($valueb['programacion_id']==$value['id']){
																$active = ($keyb==0) ? 'active':'';									
																?>
																<li role="presentation" class="<?php echo $active?>"><a href="#hall-<?php echo $valueb['id'];?>" aria-controls="hall-<?php echo $valueb['id'];?>" role="tab" data-toggle="tab"><?php echo $valueb['nombre'];?></a></li>
																<?php
															}
														}
														?>
														
														
													</ul>
													<div class="tg-eventvenuecontent tab-content">
														<figure class="tg-hallimg">
															<img src="files/programacion/<?php echo $value['imagen']?>" alt="<?php echo $evento['nombre']?>">
														</figure>
														<?php
														foreach ($bloques as $keyb => $valueb) {
															if($valueb['programacion_id']==$value['id']){
																$active = ($keyb==0) ? 'active':'';									
																?>
																<div role="tabpanel" class="tab-pane <?php echo $active?>" id="hall-<?php echo $valueb['id'];?>">
																	<?php
																	foreach ($temas as $keyt => $valuet) {
																		if($valuet['bloque_id']==$valueb['id']){
																			$tg_eventbreak = (trim($valuet['sumilla'])=='' && $valuet['ponente_id']==0) ? 'tg-eventbreak':'';
																			?>
																			<div class="tg-event <?php echo $tg_eventbreak;?>">
																				<div class="tg-eventhead">
																					<div class="tg-leftarea">
																						<time><?php echo $valuet['horario'];?></time>
																						<div class="tg-title">
																							<h2><?php echo $valuet['nombre'];?></h2>
																						</div>
																					</div>
																					<div class="tg-rightarea">
																						<!-- <a class="tg-btnfarword" href="#"><i class="fa fa-mail-forward"></i></a> -->
																					</div>
																				</div>

																				<?php 
																				if(trim($valuet['sumilla'])<>""){
																					?>
																					<div class="tg-description">
																						<p><?php echo $valuet['sumilla'];?></p>
																					</div>
																					<?php
																				}
																				
																				if(trim($valuet['ponente_id'])<>0){
																				?>
																					<div class="tg-eventspeaker">
																						<figure class="tg-eventspeakerimg">
																							<img src="files/ponentes/<?php echo $valuet['imgPonente']?>" alt="<?php echo $evento['nombre']?>">
																						</figure>
																						<div class="tg-contentbox">
																							<div class="tg-speakername">
																								<h2><?php echo $valuet['nomPonente']?></h2>
																								<span class="tg-eventcatagory"><?php echo $valuet['carPonente']?></span>
																							</div>
																							<ul class="tg-socialicons">
																								<?php if($valuet['link_facebook']<>''){?> <li class="tg-facebook"><a href="<?php echo $valuet['link_facebook'];?>" target="_blank"><i class="fa fa-facebook"></i></a></li> <?php }?>
																								<?php if($valuet['link_twitter']<>''){?> <li class="tg-twitter"><a href="<?php echo $valuet['link_facebook'];?>" target="_blank"><i class="fa fa-twitter"></i></a></li> <?php }?>
																								<?php if($valuet['link_linkedin']<>''){?> <li class="tg-linkedin"><a href="<?php echo $valuet['link_linkedin'];?>" target="_blank"><i class="fa fa-linkedin"></i></a></li> <?php }?>
																								<?php if($valuet['link_google']<>''){?> <li class="tg-googleplus"><a href="<?php echo $valuet['link_google'];?>" target="_blank"><i class="fa fa-google-plus"></i></a></li> <?php }?>
																							</ul>
																						</div>
																					</div>
																				<?php
																				}
																				?>
																			</div>
																			<?php
																		}
																	}
																	?>
																</div>
																<?php
															}
														}
														?>														

														
													</div>
												</div>
											</div>
											<?php
										}
										?>									

										
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Confrences End
			*************************************-->
			<!--************************************
					Testimonials Start
			*************************************-->
			<div class="tg-sectionspace tg-haslayout tg-bgtestimonials" style="background:url(./files/informativa/<?php echo $txt_testimonios['imagen'];?>) no-repeat center; display:none;"> 
			<!-- background-size: contain; -->
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1">
							<div class="tg-testimonials">
								<div id="tg-testimonialslider" class="tg-testimonialslider tg-testimonial owl-carousel fadeIn">
									<?php
									foreach ($testimonios as $key => $value) {
										?>
										<div class="item">
											<div class="tg-testimonialcontent">
												<span class="tg-clientname">&nbsp;<?php echo $value['nombres'];?>&nbsp;<span>-&nbsp;<?php echo $value['cargo'];?></span></span>
												<img src="files/testimonios/<?php echo $value['foto']?>" alt="<?php echo $value['nombres']?>">
												<blockquote>
													<q>&nbsp;<br>&nbsp;<!--“--><?php echo $value['descripcion']?><!--”--></q>
												</blockquote>
												
											</div>
										</div>
										<?php
									}
									?>

									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--************************************
					Testimonials End
			*************************************-->
			<!--************************************
					Speakers Start
			*************************************-->
			<section id="tg-speakers" class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
							<div class="tg-sectionhead">
								<div class="tg-sectionheading">
									<span><?php echo $txt_ponente['txt1']?></span>
									<h2><?php echo $txt_ponente['titulo']?></h2>
								</div>
								<div class="tg-description">
									<p><?php echo $txt_ponente['txt4']?></p>
								</div>
							</div>
						</div>
						<div id="tg-eventspeakerslider" class="tg-eventspeakerslider tg-speakers owl-carousel">
							<?php
							foreach ($ponentes as $key => $value) {
								?>
								<div class="item tg-speaker">
									<figure class="tg-speakerimg">
										<img src="./files/ponentes/<?php echo $value['imagen']?>" alt="<?php echo $evento['nombre']?>">
									</figure>
									<div class="tg-contentbox">
										<div class="tg-title">
											<h2><a href="javascript:void(0);" class="modal_ponente" data-imagen="<?php echo $value['imagen']?>" 
											data-nombre="<?php echo $value['nombres']?>" data-cargo="<?php echo $value['cargo']?>" 
											data-empresa="<?php echo $value['nombre_empresa']?>" data-link-empresa="<?php echo $value['link_empresa']?>" 
											data-id="<?php echo $value['id']?>"><?php echo $value['nombres']?></a></h2>
											<span>
											    <strong><?php echo $value['cargo']?></strong>
                                                <a class="btn btn-primary btn-xs pull-right modal_ponente" data-imagen="<?php echo $value['imagen']?>" 
											data-nombre="<?php echo $value['nombres']?>" data-cargo="<?php echo $value['cargo']?>" 
											data-empresa="<?php echo $value['nombre_empresa']?>" data-link-empresa="<?php echo $value['link_empresa']?>" 
											data-id="<?php echo $value['id']?>" href="javascript:void(0)">Ver más <i class="icon-plus-circle "></i></a>											    
											</span>
										</div>
										<div class="tg-description">
											<p><?php //echo $value['sumilla']?></p>
										</div>
										<ul class="tg-socialicons">
											<?php if($value['link_facebook']<>''){?> <li class="tg-facebook"><a href="<?php echo $value['link_facebook'];?>" target="_blank"><i class="fa fa-facebook"></i></a></li> <?php }?>
											<?php if($value['link_twitter']<>''){?> <li class="tg-twitter"><a href="<?php echo $value['link_facebook'];?>" target="_blank"><i class="fa fa-twitter"></i></a></li> <?php }?>
											<?php if($value['link_linkedin']<>''){?> <li class="tg-linkedin"><a href="<?php echo $value['link_linkedin'];?>" target="_blank"><i class="fa fa-linkedin"></i></a></li> <?php }?>
											<?php if($value['link_google']<>''){?> <li class="tg-googleplus"><a href="<?php echo $value['link_google'];?>" target="_blank"><i class="fa fa-google-plus"></i></a></li> <?php }?>

										</ul>
									</div>
								</div>	
								<?php							
							}
							?>
							
							
						</div>
					</div>
				</div>
				
				<ul style="display:none;" id="li_ponentes">
				<?php
				foreach ($ponentes as $key => $value) {
					?>
					<li id="descripcion_ponente_<?php echo $value['id']?>"><?php echo $value['descripcion']?></li>
					<?php
				}
				?>
				</ul>				
			</section>
			<!--************************************
					Speakers End
			*************************************-->
			<!--************************************
					Ask Question Start
			*************************************-->
			<section class="tg-askquestions tg-haslayout tg-sectionspace tg-bglight tg-opacaAfter" id="has_pregunta">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">
							<div class="tg-faqs">
								<div class="tg-sectionhead tg-textalignleft">
									<div class="tg-sectionheading">
										<span><?php echo $txt_faq['txt1']?></span>
										<h2><?php echo $txt_faq['titulo']?></h2>
									</div>
								</div>
								<div id="tg-accordion" class="tg-accordion" role="tablist" aria-multiselectable="true">
									<?php
									foreach ($faqs as $key => $value) {
										?>
										<div class="tg-panel">
											<h4><span>Q.</span><?php echo $value['pregunta'];?></h4>
											<div class="tg-panelcontent">
												<div class="tg-description">
													<p><span>A.</span><?php echo $value['respuesta'];?></p>
												</div>
											</div>
										</div>
										
										<?php
									}
									?>
									
								</div>
								<!-- <a class="tg-btn" href="javascript:void(0);">View All</a> -->
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
							<div class="tg-askquestion ">
								<div class="tg-heading">
									<h2><?php echo $txt_has_pregunta['titulo'];?></h2>
								</div>
								<?php 
								if($this->session->flashdata("preguntaExito")){
									echo '<div class="alert alert-success" style="clear: both;"><i class="fa fa-check"></i> '.$this->session->flashdata("preguntaExito").'</div>';
								}
								if($this->session->flashdata("preguntaError")){
									echo '<div class="alert alert-danger" style="clear: both;"><i class="fa fa-check"></i> '.$this->session->flashdata("preguntaError").'</div>';
								}
								?>								
								<form class="tg-formtheme tg-formaskquestions" action="procesa-pregunta" method="post">
									<fieldset>
										<div class="form-group">
											<input type="text" name="names" class="form-control" placeholder="Nombres*" required>
										</div>
										<div class="form-group">
											<input type="email" name="email" class="form-control" placeholder="Email*" required>
										</div>
										<div class="form-group">
											<textarea placeholder="Tu pregunta*" name="pregunta" required></textarea>
										</div>
										<button type="submit" class="tg-btn">Enviar ahora</button>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Ask Question End
			*************************************-->
			<!--************************************
					Packages Start
			*************************************-->
			<section id="tg-price" class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
							<div class="tg-sectionhead">
								<div class="tg-sectionheading">
									<span><?php echo $txt_precios['txt1'];?></span>
									<h2><?php echo $txt_precios['titulo'];?></h2>
								</div>
								<div class="tg-description">
									<p><?php echo $txt_precios['txt4'];?></p>
								</div>
							</div>
						</div>
						<div class="tg-packages">
							<?php
							
							foreach ($precios as $key => $value) {
								?>
								<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
									<div class="tg-package tg-basicplan">
										<div class="tg-packagehead">
											<figure class="tg-themepostimg">
												<img src="files/precios/<?php echo $value['imagen']?>" alt="<?php echo $evento['nombre']?>">
												<figcaption class="tg-packagedetail">
													<h3><?php echo $value['titulo']?></h3>
													<span><?php echo $value['subtitulo']?></span>
													<h4>
														<sub><?php echo $value['moneda']?></sub>
														<span><?php echo $value['precio']?></span>
														<em><?php echo $value['txt1']?></em>
													</h4>
													<!-- <div class="tg-stars"><span></span></div> -->
												</figcaption>
											</figure>
										</div>
										<ul>
										<?php 
											echo ($value['item1']<>'') ? '<li><i class="fa fa-check"></i><span>'.$value['item1'].'</span></li>':'<li><span></span></li>'; 
											echo ($value['item2']<>'') ? '<li><i class="fa fa-check"></i><span>'.$value['item2'].'</span></li>':'<li><span></span></li>'; 
											echo ($value['item3']<>'') ? '<li><i class="fa fa-check"></i><span>'.$value['item3'].'</span></li>':'<li><span></span></li>'; 
											echo ($value['item4']<>'') ? '<li><i class="fa fa-check"></i><span>'.$value['item4'].'</span></li>':'<li><span></span></li>'; 
											echo ($value['item5']<>'') ? '<li><i class="fa fa-check"></i><span>'.$value['item5'].'</span></li>':'<li><span></span></li>';  
											echo ($value['item6']<>'') ? '<li><i class="fa fa-check"></i><span>'.$value['item6'].'</span></li>':'<li><span></span></li>'; 
										?>
										<li class="tg-btnregister"><a class="tg-btn" target="_blank " href="<?php echo $value['link_pago'];?>">Pagar ahora</a></li>
										</ul>
									</div>
								</div>
								<?php
							}
							?>

							
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Packages End
			*************************************-->
			<!--************************************
					Gallery Start
			*************************************-->
			<div class="tg-gallerymain">
				<div class="tg-containerholder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="tg-sectionhead tg-textalignleft">
									<div class="tg-sectionheading">
										<span><?php echo $txt_galeria['txt1']?></span>
										<h2><?php echo $txt_galeria['titulo']?></h2>
									</div>
									<div class="tg-description">
										<p><?php echo $txt_galeria['txt4']?></p>
									</div>
								</div>
								<div class="tg-gallerytabs">
									<ul class="tg-gallerynav" role="tablist">
									<?php
									foreach ($album as $key => $value) {
										$active = ($key == 0) ? 'active': '';
										?>
										<li role="presentation" class="<?php echo $active;?>">
											<a href="#presentacion<?php echo $value['id']?>" aria-controls="presentacion<?php echo $value['id']?>" role="tab" data-toggle="tab">
												<figure class="tg-themepostimg">
													<time datetime="<?php echo date('Y-m-d');?>"><?php echo $value['title']?></time>
													<img src="files/album/<?php echo $value['imagen']?>" alt="<?php echo $evento['nombre']?>">
												</figure>
											</a>
										</li>
										<?php
									}
									?>
										
									
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tg-sliderholder">
					<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pull-right">
						<div class="tg-gallerytabcontent tab-content">
							<?php
							foreach ($album as $key => $value) {
								$active = ($key == 0) ? 'active': '';
								?>						
								<div role="tabpanel" class="tab-pane <?php echo $active;?>" id="presentacion<?php echo $value['id']?>">
									<div class="tg-backslider">
										<div class="tg-galleryslider owl-carousel">
										<?php 
										foreach ($fotos as $keyf => $valuef) {
											if($valuef['album_id']==$value['id']){
												?>
												<div class="item">
													<figure class="tg-themepostimg">
														<img src="files/fotos/<?php echo $valuef['imagen2']?>" alt="<?php echo $evento['nombre']?>">
													</figure>
												</div>
												<?php
											}
										}
										?>
											
											
										</div>
									</div>
									<div class="tg-forntslider">
										<div class="tg-gallerthumbslider owl-carousel">
										<?php 
										foreach ($fotos as $keyf => $valuef) {
											if($valuef['album_id']==$value['id']){
												?>
											<div class="item my-gallery">
												<figure>
													<a href="files/fotos/<?php echo $valuef['imagen']?>" data-size="1024x1024">
														<i class="icon-magnifier"></i>
														<img src="files/fotos/<?php echo $valuef['imagen']?>" alt="<?php echo $evento['nombre']?>" />
													</a>
												</figure>
											</div>												
												
												<?php
											}
										}
										?>										

										</div>
									</div>
								</div>
								<?php
							}
							?>
						</div>
						<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true"> 
							<div class="pswp__bg"></div>
							<div class="pswp__scroll-wrap"> 
								<div class="pswp__container">
									<div class="pswp__item"></div>
									<div class="pswp__item"></div>
									<div class="pswp__item"></div>
								</div>
								<div class="pswp__ui pswp__ui--hidden">
									<div class="pswp__top-bar"> 
										<div class="pswp__counter"></div>
										<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
										<button class="pswp__button pswp__button--share" title="Share"></button>
										<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
										<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
										<div class="pswp__preloader">
											<div class="pswp__preloader__icn">
												<div class="pswp__preloader__cut">
													<div class="pswp__preloader__donut"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
										<div class="pswp__share-tooltip"></div>
									</div>
									<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"> </button>
									<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"> </button>
									<div class="pswp__caption">
										<div class="pswp__caption__center"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--************************************
					Gallery End
			*************************************-->


			<!--************************************
					Video Start
			*************************************-->
			<div class="tg-videomain">
					
				<div class="tg-containerholder">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="tg-sectionhead tg-textalignleft">
									<div class="tg-sectionheading">
										<span><?php echo $txt_video['txt1']?></span>
										<h2><?php echo $txt_video['titulo']?></h2>
									</div>
									<div class="tg-description">
									<?php
									if(!$this->session->userdata('usu_video')){
										?>
										
											<p><?php echo $txt_video['txt4']?></p>
										
										<?php
									}else{
										$usu_video = $this->session->userdata('usu_video');
										echo '<p><span class="icon-user-check"></span>  '.$usu_video['nombres'].' '.$usu_video['apellidos'].'&nbsp;&nbsp;<span class="icon-mail4"></span>  '.$usu_video['correo'];
										echo '</p>';
									}
									?>
									</div>										
								</div>
								<?php
								if($this->session->userdata('usu_video')){
									echo '<div class="wvideos">';
									echo '<div class="row">';
									
									foreach ($videos as $key => $value) {
										if($value['tipo']==1){
											echo '<div class="col-xs-12 col-sm-6 list_videos">';
											echo '<a href="#" class="link-video" data-code="'.$value['code'].'" data-fuente="'.$value['fuente'].'"><span class="glyphicon glyphicon-play-circle"></span>&nbsp;'.$value['nombre'].'</a>';
											echo '</div>';
										}
									}
									
									echo '</div>';
									echo '<a href="log-off" class="btn btn-sm btn-log-off">Salir</a>';
									echo '</div>';
								}else{
									?>
									<div class="tg-loginWrapper">
										<?php 
											if($this->session->flashdata("error_login_video")){
												echo '<div class="alert alert-danger" style="clear: both;"><i class="glyphicon glyphicon-remove"></i> '.$this->session->flashdata("error_login_video").'</div>';
											}									
										?>
										<div class="login">
											<?php
											$attr=['method'=>'post'];
											echo form_open('loguin-video',$attr); 
											?>
											
												<div class="form-group">
													<input type="email" name="correo" class="form-control" placeholder="Correo electrónico" required  autocomplete="off">
												</div>

												<div class="form-group">
													<input type="password" name="password" class="form-control" placeholder="Password" required autocomplete="off">
												</div>

												<div class="form-group">
													<button class="btn btn-block btn-primary btn-login">INGRESAR</button>
												</div>

											</form>
										</div>
									</div>									
									<?php
								}
								?>

							</div>
							<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
								<div class="previewVideoWrapper">
									<?php 
									$data_video = '';
									foreach ($videos as $key => $value) {
										if($value['tipo']==0){
											$data_video = $value;
											break;
										}
									}
									if($data_video<>''){
									    if($data_video['fuente']==0){
									        ?>
									        <iframe src="https://www.youtube.com/embed/<?php echo $data_video['code']?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="video-responsive"></iframe>
									        <?php
									    }else{
									        ?>
									        <iframe src="https://player.vimeo.com/video/<?php echo $data_video['code']?>" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									        <?php
									    }
									}
									?>
									
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<!--************************************
					Video Start
			*************************************-->
			
			
			
			<!--************************************
					Brands Start
			*************************************-->
			<section id="tg-sponsers" class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
							<div class="tg-sectionhead">
								<div class="tg-sectionheading">
									<span><?php echo $txt_sponsor['txt1']?></span>
									<h2><?php echo $txt_sponsor['titulo']?></h2>
								</div>
								<div class="tg-description">
									<p><?php echo $txt_sponsor['txt4']?></p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<ul class="tg-brands">
								<?php
								foreach ($sponsor as $key => $value) {
									?>
									<li class="tg-brand"><figure><a href="javascript:void(0);"><img src="files/sponsor/<?php echo $value['imagen']?>" alt="<?php echo $evento['nombre']?>"></a></figure></li>
									<?php
								}
								?>
								
							</ul>
						</div>
					</div>
				</div>
			</section>
			<!--************************************
					Brands End
			*************************************-->
			<!--************************************
					App Start
			*************************************-->
			<!-- <section class="tg-haslayout tg-appbg tg-bgparallax">
				<div class="container">
					<div class="row">
						<div class="tg-getapp">
							<div class="col-xs-12 col-sm-12 col-md-5 col-lg-6">
								<div class="tg-appcontent">
									<div class="tg-sectionhead">
										<div class="tg-sectionheading">
											<span>Trusted hands behind us</span>
											<h2>Our great sponsors</h2>
										</div>
										<div class="tg-description">
											<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamcoaris nisi ut aliquipa ex ea commodo consequat aute irure.</p>
										</div>
									</div>
									<ul class="tg-appicons">
										<li><i class="fa fa-apple"></i></li>
										<li><i class="fa fa-android"></i></li>
										<li><i class="fa fa-windows"></i></li>
									</ul>
									<div class="tg-btnwhite">
										<a class="tg-btn" href="#">Download App</a>
										<a class="tg-btn" href="#">Screenshots</a>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6 col-md-7 col-lg-6 hidden-sm hidden-xs">
								<figure class="tg-appimg"><img src="assets/frontend/images/img-02.png" alt="<?php echo $evento['nombre']?>"></figure>
							</div>
						</div>
					</div>
				</div>
			</section> -->
			<!--************************************
					App End
			*************************************-->

			<!--************************************
					Signup Start
			*************************************-->
			<!-- <section class="tg-haslayout tg-bgsignup">
				<div class="container">
					<div class="row">
						<div class="tg-signupnewsletter">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="tg-signupdetail">
									<div class="tg-sectionhead tg-textalignleft">
										<div class="tg-sectionheading">
											<span>What are you waiting for?</span>
											<h2>Signup for latest news &amp; tips</h2>
										</div>
									</div>
									<form class="tg-formtheme tg-formsingup">
										<fieldset>
											<div class="form-group tg-inputicon">
												<i class="icon-bullhorn"></i>
												<input type="email" name="email" class="form-control" placeholder="Your Email">
											</div>
											<button type="submit" class="tg-btn">Signup Now</button>
										</fieldset>
									</form>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 hidden-sm hidden-xs">
								<figure class="tg-newsletterimg"><img src="assets/frontend/images/bg-signup.png" alt="<?php echo $evento['nombre']?>"></figure>
							</div>
						</div>
					</div>
				</div>
			</section> -->
			<!--************************************
					Signup End
			*************************************-->
			<!--************************************
					News & Articles Start
			*************************************-->
			<!-- <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="tg-newsarticles">
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="tg-textshortcode">
									<div class="tg-sectionhead tg-textalignleft">
										<div class="tg-sectionheading">
											<span>News from the world</span>
											<h2>Latest tips &amp; articles</h2>
										</div>
										<div class="tg-description">
											<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt labore dolore magna aliqua enim ad minim veniam quis nostrud exercitation ullamcoaris nisi ut aliquipa ex ea commodo consequat aute irure.</p>
										</div>
										<div class="tg-btnarea">
											<a class="tg-btn" href="#">Book Your Seat</a>
											<a class="tg-btn" href="#">Meet The Team</a>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
								<div class="tg-articles">
									<div class="row">
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<article class="tg-article">
												<figure><img src="assets/frontend/images/newsarticles/img-01.jpg" alt="<?php echo $evento['nombre']?>"></figure>
												<div class="tg-contentbox">
													<div class="tg-title">
														<h2><a href="javascript:void(0);">Reach high & touch the stars</a></h2>
													</div>
													<ul class="tg-postmaradata">
														<li><i class="icon-calendar-full"></i><time datetime="2017-12-12">Jun 27, 2017</time></li>
														<li><i class="icon-bubble"></i><span>32319</span></li>
													</ul>
													<div class="tg-description">
														<p>Consectetur adipisicing elit sed doismod tempor incididunt labore</p>
													</div>
													<a class="tg-btnreadmore" href="javascript:void(0);">Read more</a>
												</div>
											</article>
										</div>
										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
											<article class="tg-article">
												<figure><img src="assets/frontend/images/newsarticles/img-02.jpg" alt="<?php echo $evento['nombre']?>"></figure>
												<div class="tg-contentbox">
													<div class="tg-title">
														<h2><a href="javascript:void(0);">Reach high & touch the stars</a></h2>
													</div>
													<ul class="tg-postmaradata">
														<li><i class="icon-calendar-full"></i><time datetime="2017-12-12">Jun 27, 2017</time></li>
														<li><i class="icon-bubble"></i><span>32319</span></li>
													</ul>
													<div class="tg-description">
														<p>Consectetur adipisicing elit sed doismod tempor incididunt labore</p>
													</div>
													<a class="tg-btnreadmore" href="javascript:void(0);">Read more</a>
												</div>
											</article>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section> -->
			<!--************************************
					News & Articles End
			*************************************-->
		</main>
		<!--************************************
				Main End
		*************************************-->

		<!--************************************
				Footer Start
		*************************************-->
		<?php echo $view_footer; ?>
		<!--************************************
				Footer End
		*************************************-->                
    </div>
    <!--************************************
            Wrapper End
    *************************************-->

    <?php 
    echo $view_foot;
    ?>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5bb44bce8a438d2b0ce00173/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

	<script>
	$(".link-video").on('click',function(e){
		e.preventDefault();
		var code 	= $(this).data('code');
		var fuente 	= $(this).data('fuente');
		var frame;
		if(fuente == 0){
			frame = '<iframe src="https://www.youtube.com/embed/'+code+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="video-responsive"></iframe>';
		}else{
			frame = '<iframe src="https://player.vimeo.com/video/'+code+'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen class="video-responsive"></iframe>';
		}
		var img= '<img src="./assets/frontend/images/loading.gif" width="100" class="img-load">';
		$(".previewVideoWrapper").html(img);
		setTimeout(function() {
			$(".previewVideoWrapper").html(frame);	
		}, 700);
		

	});


	setInterval(function(){ 
		if($("iframe").length>0){
			var frames =  $("iframe");
			var srcs = [];
			frames.each(function(i,e){
				var src = $(e).attr('src');
				if(src.indexOf('youtube')>0 || src.indexOf('vimeo')>0){
					srcs.push(src);
				}
			})
			verifica_intruso(srcs);
		};
	 }, 30000);
	</script>
</body>
</html>