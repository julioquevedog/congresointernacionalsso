<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "frontend/controller_index";
$route['404_override'] = '';

$route['registrar-reserva'] = "frontend/controller_index/procesar_reserva";
$route['procesa-pregunta'] = "frontend/controller_index/procesa_pregunta";
$route['brochure/([0-9]+)'] = "frontend/controller_index/brochure/$1";
$route['loguin-video'] = "frontend/controller_index/logueo";
$route['log-off'] = "frontend/controller_index/log_off";


$route['congreso-virtual'] = "frontend/controller_congreso_virtual";
$route['congreso-virtual/login'] = "frontend/controller_congreso_virtual/login";
$route['congreso-virtual/process-logueo'] = "frontend/controller_congreso_virtual/process_logueo";
$route['congreso-virtual/forget'] = "frontend/controller_congreso_virtual/forget";
$route['congreso-virtual/process-forget'] = "frontend/controller_congreso_virtual/process_forget";
$route['congreso-virtual/process-consulta'] = "frontend/controller_congreso_virtual/process_consulta";
$route['congreso-virtual/log-off'] = "frontend/controller_congreso_virtual/log_off";
$route['meeting'] = "frontend/controller_congreso_virtual/meeting";
$route['screen_black'] = "frontend/controller_congreso_virtual/screen_black";


// ADMINISTRACION -- MAINPANEL *****************************
$route['mainpanel'] = "mainpanel/controller_login";
$route['mainpanel/login'] = "mainpanel/controller_login/login";
$route['mainpanel/validar'] = "mainpanel/controller_login/validar";
$route['mainpanel/logout'] = "mainpanel/controller_login/logout";
$route['mainpanel/mis-datos'] = "mainpanel/controller_misdatos";

$route['mainpanel/configuracion/listado'] = "mainpanel/controller_config/listado";
$route['mainpanel/configuracion/edit/([0-9]+)'] = "mainpanel/controller_config/edit/$1";
$route['mainpanel/configuracion/actualizar'] = "mainpanel/controller_config/actualizar";

$route['mainpanel/mapa'] = "mainpanel/controller_mapa/index";
$route['mainpanel/mapa/actualizar'] = "mainpanel/controller_mapa/update";

/* End of file routes.php */
/* Location: ./application/config/routes.php */