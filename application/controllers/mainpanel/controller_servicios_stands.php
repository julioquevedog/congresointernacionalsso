<?php
class Controller_servicios_stands extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');        
        $this->load->model('mainpanel/Model_servicios_stands');                
        $this->load->library('my_upload');
        $this->current_section="album";
        date_default_timezone_set("America/Lima");
    }
    
    
    public function listar() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                             = $this->uri->segment(4);        
        $data['current_section']        = $this->current_section;
        $menu['lista_menu']             = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']                 = "servicios_stands/index_view";

        $where                          = ['stand_id'=>$id];
        $table                          = "servicios_stands";
        $data["data"]                   = $this->Model_base->getList($table,$where);

        $where                          = ['id'=>$id];
        $data["stand"]                  = $this->Model_base->get('stands',$where);

        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']             = "servicios_stands/edit_view";

        $table                      = 'servicios_stands';
        $where                      = ['id'=>$id];
        $data["data"]               = $this->Model_base->get($table,$where);      
        
        $where                      = ['id'=>$data["data"]->stand_id];
        $data["stand"]              = $this->Model_base->get('stands',$where);


        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data                   = $this->input->post();
        $data['fecha_update']   = date('Y-m-d h:m:s');         
        
        $imgatng                = $this->input->post('imgatng');
        unset($data['imgatng']);        

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 360;
            $this->my_upload->image_y          = 180;   
            $this->my_upload->process('./files/servicios_stands/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/servicios_stands/" . $this->my_upload->file_dst_name , "./files/servicios_stands/" . $newfilename);
                $data['imagen'] = $newfilename;
                @unlink('./files/servicios_stands/'.$imgatng);
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_servicios_stands/edit/'.$data['id']);
            }
          
        }

        
        $this->my_upload->upload($_FILES["pdf"]);
        if ( $this->my_upload->uploaded == true  )
        {
           
            $this->my_upload->allowed          = array('application/*');
            $this->my_upload->process('./files/servicios_stands_pdf/');
            if ( $this->my_upload->processed == true )
            {
                
                $temp = explode(".", $this->my_upload->file_dst_name);                

                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/servicios_stands_pdf/" . $this->my_upload->file_dst_name , "./files/servicios_stands_pdf/" . $newfilename);
                $data['pdf'] = $newfilename;
                
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_servicios_stands/edit/'.$data['id']);
            }
        }          
        

        $where      = ['id'=>$data['id']];
        $table      = 'servicios_stands';
        $result=$this->Model_base->update($data,$table,$where);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);            
        }                       
        redirect('mainpanel/controller_servicios_stands/edit/'.$data['id']);
    }
    

    
    public function nuevo() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                         = $this->uri->segment(4); 
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true);

       
        $where                      = ['id'=>$id];
        $data["stand"]              = $this->Model_base->get('stands',$where);

        $where                      = ['stand_id'=>$data["stand"]->id];
        $data["orden"]              = $this->Model_servicios_stands->lastItem($where)->orden;               
        $data["orden"]              = (int)$data["orden"] + 1;
        
        
        $data['cuerpo']             ="servicios_stands/nuevo_view";
        
        $this->load->view("mainpanel/includes/template", $data);        
    }
    
    public function grabar() { 
        $this->validacion->validacion_login();
        
        $data                   = $this->input->post();
        $data['fecha_insert']   = date('Y-m-d h:m:s'); 
        $id                     = $data['stand_id'];

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 360;
            $this->my_upload->image_y          = 180;   
            $this->my_upload->process('./files/servicios_stands/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/servicios_stands/" . $this->my_upload->file_dst_name , "./files/servicios_stands/" . $newfilename);
                $data['imagen'] = $newfilename;
                
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_servicios_stands/nuevo/'.$id);
            }
        }else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_servicios_stands/nuevo/'.$id);
            }  
                          
            $this->my_upload->upload($_FILES["pdf"]);
            if ( $this->my_upload->uploaded == true  )
            {
                $this->my_upload->allowed          = array('application/pdf');
                $this->my_upload->process('./files/servicios_stands_pdf/');
                if ( $this->my_upload->processed == true )
                {
                    $temp = explode(".", $this->my_upload->file_dst_name);                
                    $newfilename = round(microtime(true)) . '.' . end($temp);
                    rename("./files/servicios_stands_pdf/" . $this->my_upload->file_dst_name , "./files/servicios_stands_pdf/" . $newfilename);
                    $data['pdf'] = $newfilename;
                    
                }
                else
                {
                    $error = $this->my_upload->error;
                    $this->session->set_userdata("error",$error);       
                    redirect('mainpanel/controller_servicios_stands/nuevo/'.$id);
                }
            }else
                {
                    $error = $this->my_upload->error;
                    $this->session->set_userdata("error",$error);       
                    redirect('mainpanel/controller_servicios_stands/nuevo/'.$id);
                }  
                
        $resultado = $this->Model_base->save('servicios_stands',$data);
        if($resultado==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
            redirect('mainpanel/controller_servicios_stands/listar/'.$id);
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);
            redirect('mainpanel/controller_servicios_stands/nuevo/'.$id);
        }                   
    }
	
    
    public function delete($id) {
        $this->validacion->validacion_login();

        $where                      = ['id'=>$id];
        $table                      = 'servicios_stands';
        $data["data"]               = $this->Model_base->get($table,$where);  

        @unlink('./files/servicios_stands/'.$data["data"]->imagen);

        $result=$this->Model_base->delete($id,$table);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información ';
            $this->session->set_userdata("error",$error);            
        }          
        redirect('mainpanel/controller_servicios_stands/listar/'.$data["data"]->stand_id);
    }



    
}
?>
