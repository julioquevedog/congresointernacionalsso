<?php
class Controller_usuarios_mem extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');        
        $this->current_section="usuarios_mem";
        date_default_timezone_set("America/Lima");
    }
    
    
    public function listar() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']        = $this->current_section;
        $menu['lista_menu']             = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']                 = "usuarios_mem/index_view";

        $where                          = ['id >'=>0];
        $table                          = "usuarios_mem";
        $data["data"]                   = $this->Model_base->getList($table,$where);

        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']             = "usuarios_mem/edit_view";

        $table                      = 'usuarios_mem';
        $where                      = ['id'=>$id];
        $data["data"]               = $this->Model_base->get($table,$where);      
        
        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data                   = $this->input->post();
        $data['password']       = trim($data['password']);
        $data['fecha_update']   = date('Y-m-d h:m:s');  

        $where      = ['id'=>$data['id']];
        $table      = 'usuarios_mem';
        $result=$this->Model_base->update($data,$table,$where);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información '.$error;
            $this->session->set_userdata("error",$error);            
        }                       
        redirect('mainpanel/controller_usuarios_mem/edit/'.$data['id']);
    }
    

    
    public function nuevo() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************

        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true);

    
        
        $data['cuerpo']="usuarios_mem/nuevo_view";
        
        $this->load->view("mainpanel/includes/template", $data);        
    }
    
    public function grabar() {
        $this->validacion->validacion_login();
        
        $data                   = $this->input->post();
        $data['password']       = trim($data['password']);
        $data['fecha_insert']   = date('Y-m-d h:m:s'); 
                
        $resultado = $this->Model_base->save('usuarios_mem',$data);
        if($resultado==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
            redirect('mainpanel/controller_usuarios_mem/listar/');
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);
            redirect('mainpanel/controller_usuarios_mem/nuevo/');
        }                   
    }
	
    
    public function delete($id) {
        $this->validacion->validacion_login();

        $result=$this->Model_base->delete($id,'usuarios_mem');
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información ';
            $this->session->set_userdata("error",$error);            
        }          
        redirect('mainpanel/controller_usuarios_mem/listar/');
    }



    
}
?>
