<?php
class controller_mapa extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/model_mapa');
    }
    
    
    public function index() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section'] = 'mapa';
        $menu['lista_menu'] = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $data['modal'] = $this->load->view('mainpanel/banners/modal_banner', true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']="mapa/index_view";
        $data['sede']=$this->model_mapa->getMapa();
        // *****************************************************************
        $this->load->view("mainpanel/includes/template", $data);
    }
    
    public function update() {
        $this->validacion->validacion_login();
        // EDITAR BANNER
        $data['latitud_centro'] = $this->input->post('latitud_centro');
        $data['longitud_centro'] = $this->input->post('longitud_centro');
        $data['latitud_punto'] = $this->input->post('latitud_punto');
        $data['longitud_punto'] = $this->input->post('longitud_punto');
        $data['zoom'] = $this->input->post('zoom');
        $data['tipomapa'] = $this->input->post('tipomapa');
        
        $id_sede = $this->input->post('id_sede');        
        
        $result=$this->model_mapa->updateMapa($data, $id_sede);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);            
        }            
        
        redirect('mainpanel/mapa');
    }    
    

}
?>
