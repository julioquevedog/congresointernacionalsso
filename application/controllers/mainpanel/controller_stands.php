<?php
class Controller_stands extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');        
        $this->load->model('mainpanel/Model_stands');
        $this->load->library('my_upload');
        $this->current_section="stands";
        date_default_timezone_set("America/Lima");
    }
    
    
    public function listar() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']        = $this->current_section;
        $menu['lista_menu']             = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']                 = "stands/index_view";

        $where                          = ['id >'=>0];
        $table                          = "stands";
        $data["data"]                   = $this->Model_base->getList($table,$where);

        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']             = "stands/edit_view";

        $table                      = 'stands';
        $where                      = ['id'=>$id];
        $data["data"]               = $this->Model_base->get($table,$where);      
        
        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data                   = $this->input->post();
        $data['fecha_update']   = date('Y-m-d h:m:s');  

        $imgatng                = $this->input->post('imgatng');
        $logoatng                = $this->input->post('logoatng');        
        unset($data['imgatng']);
        unset($data['logoatng']);

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 360;
            $this->my_upload->image_y          = 280;   
            $this->my_upload->process('./files/stands/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/stands/" . $this->my_upload->file_dst_name , "./files/stands/" . $newfilename);
                $data['imagen'] = $newfilename;
                $this->my_upload->clean();
                @unlink('./files/stands/'.$imgatng);

                
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_stands/edit/'.$data['id']);
            }
        } 


        $this->my_upload->upload($_FILES["logo"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 140;
            $this->my_upload->image_y          = 70;   
            $this->my_upload->process('./files/stands/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . 'logo.' . end($temp);
                rename("./files/stands/" . $this->my_upload->file_dst_name , "./files/stands/" . $newfilename);
                $data['logo'] = $newfilename;
                $this->my_upload->clean();
                @unlink('./files/stands/'.$logoatng);
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_stands/edit/'.$data['id']);
            }
        } 

        $where      = ['id'=>$data['id']];
        $table      = 'stands';
        $result=$this->Model_base->update($data,$table,$where);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información '.$error;
            $this->session->set_userdata("error",$error);            
        }                       
        redirect('mainpanel/controller_stands/edit/'.$data['id']);
    }
    

    
    public function nuevo() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************

        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true);

       
        $where                          = ['id >'=>0];
        $data["orden"]                  = $this->Model_stands->lastItem($where)->orden;               
        $data["orden"]                  = (int)$data["orden"] + 1;
        
        
        $data['cuerpo'] = "stands/nuevo_view";
        
        $this->load->view("mainpanel/includes/template", $data);        
    }
    
    public function grabar() {
        $this->validacion->validacion_login();
        
        $data                   = $this->input->post();
        $data['fecha_insert']   = date('Y-m-d h:m:s'); 
                
        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 360;
            $this->my_upload->image_y          = 280;   
            $this->my_upload->process('./files/stands/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/stands/" . $this->my_upload->file_dst_name , "./files/stands/" . $newfilename);
                $data['imagen'] = $newfilename;
                $this->my_upload->clean();

                $this->my_upload->upload($_FILES["logo"]);
                if ( $this->my_upload->uploaded == true  )
                {
                    $this->my_upload->allowed          = array('image/*');
                    $this->my_upload->image_resize     = true;
                    $this->my_upload->image_ratio_crop = true;
                    $this->my_upload->image_x          = 140;
                    $this->my_upload->image_y          = 70;   
                    $this->my_upload->process('./files/stands/');
                    if ( $this->my_upload->processed == true )
                    {
                        $temp = explode(".", $this->my_upload->file_dst_name);                
                        $newfilename = round(microtime(true)) . 'logo.' . end($temp);
                        rename("./files/stands/" . $this->my_upload->file_dst_name , "./files/stands/" . $newfilename);
                        $data['logo'] = $newfilename;
                        $this->my_upload->clean();
                    }
                    else
                    {
                        $error = $this->my_upload->error;
                        $this->session->set_userdata("error",$error);       
                        redirect('mainpanel/controller_stands/nuevo/');
                    }
                }                
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_stands/nuevo/');
            }
        }  else
        {
            $error = $this->my_upload->error;
            $this->session->set_userdata("error",$error);       
            redirect('mainpanel/controller_stands/nuevo/');
        }  

        $resultado = $this->Model_base->save('stands',$data);
        if($resultado==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
            redirect('mainpanel/controller_stands/listar/');
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);
            redirect('mainpanel/controller_stands/nuevo/');
        }                   
    }
	
    
    public function delete($id) {
        $this->validacion->validacion_login();

        $table                  = 'stands';
        $where                  = ['id'=>$id];
        $album                  = $this->Model_base->get($table,$where);

        $imagen         = $album->imagen;                

        @unlink('files/stands/'.$imagen);

        $result=$this->Model_base->delete($id,$table);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información ';
            $this->session->set_userdata("error",$error);            
        }          
        redirect('mainpanel/controller_stands/listar/');
    }



    
}
?>
