<?php
class Controller_menu extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');
        $this->load->library('my_upload');
        $this->current_section="menu";
    }
    
    
    public function listado() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section'] =$this->current_section;
        $menu['lista_menu'] = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']="menu/index_view";
        
        
        $datos = $this->Model_base->getlist('menu');
        $data["datos"] = $datos;
        
        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();

        $data['current_section'] =$this->current_section;
        $menu['lista_menu'] = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']="menu/edit_view";
        $data['data'] = $this->Model_base->get(['id'=>$id],'menu');
        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        // EDITAR CATEGORIA
        $data=$this->input->post();
        $imgatng=$data['imgatng'];
        unset($data['imgatng']);
        $data['fecha_update']=date('Y-m-d h:m:s');

        
        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 290;
            $this->my_upload->image_y          = 310;
            $this->my_upload->process('./files/menu/');
            if ($this->my_upload->processed == true ) {
                $temp = explode(".", $this->my_upload->file_dst_name);    
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/menu/" . $this->my_upload->file_dst_name , "./files/menu/" . $newfilename);                
                $data['imagen'] = $newfilename;
                //$this->my_upload->clean();
                @unlink('./files/menu/'.$imgatng);
            } else {
                $error = $this->my_upload->error;
            }
        }
        
        if(isset($error)){
            $this->session->set_userdata("error",$error);                  
        }else{
            $result=$this->Model_base->update(['id'=>$data['id']],$data,'menu');
            if($result==true){
                $this->session->set_userdata("success",'Se procesó correctamente la información');
            }else{
                $error='Ocurrió un error al procesar su información '.$error;
                $this->session->set_userdata("error",$error);            
            }                       
        }

        redirect('mainpanel/controller_menu/edit/'.$data['id']);
    }
    
 


}
?>
