<?php
class Controller_ajax extends CI_Controller {
    function __construct()
    {
        parent::__construct();
    
        $this->load->model('mainpanel/Model_ajax');
    }

    public function index()	{

    }


    public function updateusuario() {
        $json=array();        
        $nombres = $_POST['nombres'];
        $password = $_POST['password'];        
        $newpasw = $_POST['newpasw'];
        $idusuario = $this->session->userdata('id_admin'); 
        $this->session->set_userdata('nombre_admin', $nombres);
        $result=$this->Model_ajax->updateusuario($nombres,$newpasw,$idusuario,$password);                           
        $json['result'] = $result;
        echo json_encode($json);
    }   
    
   public function ordProd() {
        $json=array();      
        $ordenes = $_POST['orden'];	
        $cad1=explode("&",$ordenes);
        $num=count($cad1);
        $orden=0;
	for($o=1;$o<=$num;$o++){
            $str=explode("=",$cad1[$o]);
            $id=$str[1];
            $orden +=1;
            $data=array();
            $data['ordestaca']=$orden;
            $this->Model_ajax->ordProd($id,$data);        
            //$aux = "update productos set orden='$pos' where id_producto='$id'";
            //$sql[] = $aux;
            //$query = mysql_query($aux);	
        }
        $json['result'] = 'Se ordeno correctamente!';
        echo json_encode($json);
    }      
}
?>
