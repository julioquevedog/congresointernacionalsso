<?php
class Controller_temas extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');        
        $this->load->model('mainpanel/Model_temas');                
        $this->load->library('my_upload');
        $this->current_section="eventos";
        date_default_timezone_set("America/Lima");
    }
    
    
    public function listar() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                             = $this->uri->segment(4);        
        $data['current_section']        = $this->current_section;
        $menu['lista_menu']             = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']                 = "temas/index_view";

        $where                          = ['bloque_id'=>$id];
        $table                          = "temas";
        $data["data"]                   = $this->Model_base->getList($table,$where);

        $where                          = ['id'=>$id];
        $data["bloque"]                 = $this->Model_base->get('bloques',$where);

        $where                          = ['id'=>$data["bloque"]->programacion_id];
        $data["programacion"]           = $this->Model_base->get('programacion',$where);

        $where                          = ['id'=>$data["programacion"]->evento_id];
        $data["evento"]                 = $this->Model_base->get('eventos',$where);        

        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']             = "temas/edit_view";

        $table                      = 'temas';
        $where                      = ['id'=>$id];
        $data["data"]               = $this->Model_base->get($table,$where);      
        
        $where                          = ['id'=>$data["data"]->bloque_id];
        $data["bloque"]                 = $this->Model_base->get('bloques',$where);

        $where                          = ['id'=>$data["bloque"]->programacion_id];
        $data["programacion"]           = $this->Model_base->get('programacion',$where);

        $where                          = ['id'=>$data["programacion"]->evento_id];
        $data["evento"]                 = $this->Model_base->get('eventos',$where);      

        $where                          = ['id >'=>0];
        $data["ponentes"]                 = $this->Model_base->getList('ponentes',$where);      


        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data                   = $this->input->post();
        $data['fecha_update']   = date('Y-m-d h:m:s');         

        $where      = ['id'=>$data['id']];
        $table      = 'temas';
        $result=$this->Model_base->update($data,$table,$where);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información '.$error;
            $this->session->set_userdata("error",$error);            
        }                       
        redirect('mainpanel/controller_temas/edit/'.$data['id']);
    }
    

    
    public function nuevo() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                         = $this->uri->segment(4); 
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true);

       
        $where                          = ['id'=>$id];
        $data["bloque"]                 = $this->Model_base->get('bloques',$where);

        $where                          = ['id'=>$data["bloque"]->programacion_id];
        $data["programacion"]           = $this->Model_base->get('programacion',$where);

        $where                          = ['id'=>$data["programacion"]->evento_id];
        $data["evento"]                 = $this->Model_base->get('eventos',$where);            

        $where                          = ['bloque_id'=>$data["bloque"]->id];
        $data["orden"]                  = $this->Model_temas->lastItem($where)->orden;               
        $data["orden"]                  = (int)$data["orden"] + 1;
        
        $where                          = ['id >'=>0];
        $data["ponentes"]                 = $this->Model_base->getList('ponentes',$where);  
                
        $data['cuerpo']="temas/nuevo_view";
        
        $this->load->view("mainpanel/includes/template", $data);        
    }
    
    public function grabar() {
        $this->validacion->validacion_login();
        
        $data                   = $this->input->post();
        $data['fecha_insert']   = date('Y-m-d h:m:s'); 
        $id                     = $data['bloque_id'];
      

        
                
        $resultado = $this->Model_base->save('temas',$data);
        if($resultado==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
            redirect('mainpanel/controller_temas/listar/'.$id);
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);
            redirect('mainpanel/controller_temas/nuevo/'.$id);
        }                   
    }
	
    
    public function delete($id) {
        $this->validacion->validacion_login();

        $where                      = ['id'=>$id];
        $table                      = 'temas';
        $data["data"]               = $this->Model_base->get($table,$where);  


        $result=$this->Model_base->delete($id,'temas');
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información ';
            $this->session->set_userdata("error",$error);            
        }          
        redirect('mainpanel/controller_temas/listar/'.$data["data"]->bloque_id);
    }



    
}
?>
