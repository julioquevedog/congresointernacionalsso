<?php
class Controller_servicios_stands extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');        
        $this->load->model('mainpanel/Model_fotos');                
        $this->load->library('my_upload');
        $this->current_section="album";
        date_default_timezone_set("America/Lima");
    }
    
    
    public function listar() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                             = $this->uri->segment(4);        
        $data['current_section']        = $this->current_section;
        $menu['lista_menu']             = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']                 = "fotos/index_view";

        $where                          = ['album_id'=>$id];
        $table                          = "fotos";
        $data["data"]                   = $this->Model_base->getList($table,$where);

        $where                          = ['id'=>$id];
        $data["album"]                  = $this->Model_base->get('album',$where);

        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']             = "fotos/edit_view";

        $table                      = 'fotos';
        $where                      = ['id'=>$id];
        $data["data"]               = $this->Model_base->get($table,$where);      
        
        $where                      = ['id'=>$data["data"]->album_id];
        $data["album"]              = $this->Model_base->get('album',$where);


        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data                   = $this->input->post();
        $data['fecha_update']   = date('Y-m-d h:m:s');         
        
        $imgatng                = $this->input->post('imgatng');
        $imgatng2               = $this->input->post('imgatng2');        
        unset($data['imgatng']);        
        unset($data['imgatng2']);                

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 430;
            $this->my_upload->image_y          = 573;   
            $this->my_upload->process('./files/fotos/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/fotos/" . $this->my_upload->file_dst_name , "./files/fotos/" . $newfilename);
                $data['imagen'] = $newfilename;
                @unlink('./files/fotos/'.$imgatng);
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_fotos/nuevo/'.$id);
            }

            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 875;
            $this->my_upload->image_y          = 690;     
            $this->my_upload->process('./files/fotos/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/fotos/" . $this->my_upload->file_dst_name , "./files/fotos/thumb" . $newfilename);
                $data['imagen2'] = 'thumb'.$newfilename;
                @unlink('./files/fotos/'.$imgatng2);
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_fotos/nuevo/'.$id);
            }            
        }
        

        $where      = ['id'=>$data['id']];
        $table      = 'fotos';
        $result=$this->Model_base->update($data,$table,$where);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información '.$error;
            $this->session->set_userdata("error",$error);            
        }                       
        redirect('mainpanel/controller_fotos/edit/'.$data['id']);
    }
    

    
    public function nuevo() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                         = $this->uri->segment(4); 
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true);

       
        $where                      = ['id'=>$id];
        $data["album"]              = $this->Model_base->get('album',$where);

        $where                      = ['album_id'=>$data["album"]->id];
        $data["orden"]              = $this->Model_fotos->lastItem($where)->orden;               
        $data["orden"]              = (int)$data["orden"] + 1;
        
        
        $data['cuerpo']             ="fotos/nuevo_view";
        
        $this->load->view("mainpanel/includes/template", $data);        
    }
    
    public function grabar() {
        $this->validacion->validacion_login();
        
        $data                   = $this->input->post();
        $data['fecha_insert']   = date('Y-m-d h:m:s'); 
        $id                     = $data['album_id'];

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 430;
            $this->my_upload->image_y          = 573;   
            $this->my_upload->process('./files/fotos/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/fotos/" . $this->my_upload->file_dst_name , "./files/fotos/thumb" . $newfilename);
                $data['imagen'] = 'thumb'.$newfilename;
                
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_fotos/nuevo/'.$id);
            }
        }else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_fotos/nuevo/'.$id);
            }  
                          

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 875;
            $this->my_upload->image_y          = 690;   
            $this->my_upload->process('./files/fotos/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/fotos/" . $this->my_upload->file_dst_name , "./files/fotos/" . $newfilename);
                $data['imagen2'] = $newfilename;
                $this->my_upload->clean();
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_fotos/nuevo/'.$id);
            }            
        }  else
        {
            $error = $this->my_upload->error;
            $this->session->set_userdata("error",$error);       
            redirect('mainpanel/controller_fotos/nuevo/'.$id);
        }  
             
                
        $resultado = $this->Model_base->save('fotos',$data);
        if($resultado==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
            redirect('mainpanel/controller_fotos/listar/'.$id);
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);
            redirect('mainpanel/controller_fotos/nuevo/'.$id);
        }                   
    }
	
    
    public function delete($id) {
        $this->validacion->validacion_login();

        $where                      = ['id'=>$id];
        $table                      = 'fotos';
        $data["data"]               = $this->Model_base->get($table,$where);  

        @unlink('./files/fotos/'.$data["data"]->imagen);
        @unlink('./files/fotos/'.$data["data"]->imagen2);        

        $result=$this->Model_base->delete($id,'fotos');
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información ';
            $this->session->set_userdata("error",$error);            
        }          
        redirect('mainpanel/controller_fotos/listar/'.$data["data"]->album_id);
    }



    
}
?>
