<?php
class Controller_programacion extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');        
        $this->load->model('mainpanel/Model_programacion');                
        $this->load->library('my_upload');
        $this->current_section="eventos";
        date_default_timezone_set("America/Lima");
    }
    
    
    public function listar() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                             = $this->uri->segment(4);        
        $data['current_section']        = $this->current_section;
        $menu['lista_menu']             = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']                 = "programacion/index_view";
        $where                          = ['evento_id'=>$id];
        $table                          = "programacion";
        $data["data"]                   = $this->Model_base->getList($table,$where);


        $where                          = ['id'=>$id];
        $data["evento"]                 = $this->Model_base->get('eventos',$where);

        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']             = "programacion/edit_view";
        $table                      = 'programacion';
        $where                      = ['id'=>$id];
        $data["data"]               = $this->Model_base->get($table,$where);      
        
        $where                      = ['id'=>$data["data"]->evento_id];
        $data["evento"]             = $this->Model_base->get('eventos',$where);

        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data                   = $this->input->post();
        $data['fecha_update']   = date('Y-m-d h:m:s');         
        
        $imgatng                = $this->input->post('imgatng');
        unset($data['imgatng']);

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 164;
            $this->my_upload->image_y          = 164;   
            $this->my_upload->process('./files/programacion/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/programacion/" . $this->my_upload->file_dst_name , "./files/programacion/" . $newfilename);
                $data['imagen'] = $newfilename;
                $this->my_upload->clean();
                @unlink('./files/programacion/'.$imgatng);
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_programacion/edit/'.$data['id']);
            }
        }   

        $where      = ['id'=>$data['id']];
        $table      = 'programacion';
        $result=$this->Model_base->update($data,$table,$where);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información '.$error;
            $this->session->set_userdata("error",$error);            
        }                       
        redirect('mainpanel/controller_programacion/edit/'.$data['id']);
    }
    

    
    public function nuevo() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                         = $this->uri->segment(4); 
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true);

        $table                      = 'eventos';
        $where                      = ['id'=>$id];
        $data["evento"]             = $this->Model_base->get($table,$where);          

        $where                      = ['evento_id'=>$data["evento"]->id];
        $data["orden"]              = $this->Model_programacion->lastItem($where)->orden;               
        $data["orden"]              = (int)$data["orden"] + 1;
        
        
        $data['cuerpo']="programacion/nuevo_view";
        
        $this->load->view("mainpanel/includes/template", $data);        
    }
    
    public function grabar() {
        $this->validacion->validacion_login();
        
        $data                   = $this->input->post();
        $data['fecha_insert']   = date('Y-m-d h:m:s'); 
        $id                     = $data['evento_id'];

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 164;
            $this->my_upload->image_y          = 164;   
            $this->my_upload->process('./files/programacion/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/programacion/" . $this->my_upload->file_dst_name , "./files/programacion/" . $newfilename);
                $data['imagen'] = $newfilename;
                $this->my_upload->clean();
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_programacion/nuevo/'.$id);
            }
        }   
        else
        {
            $error = $this->my_upload->error;
            $this->session->set_userdata("error",$error);       
            redirect('mainpanel/controller_programacion/nuevo/'.$id);
        }        
        
                
        $resultado = $this->Model_base->save('programacion',$data);
        if($resultado==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
            redirect('mainpanel/controller_programacion/listar/'.$id);
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);
            redirect('mainpanel/controller_programacion/nuevo/'.$id);
        }                   
    }
	
    
    public function delete($id) {
        $this->validacion->validacion_login();

        $where                      = ['id'=>$id];
        $table                      = 'programacion';
        $data["data"]               = $this->Model_base->get($table,$where);  

        @unlink('./files/programacion/'.$data["data"]->imagen);

        $result=$this->Model_base->delete($id,'programacion');
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información ';
            $this->session->set_userdata("error",$error);            
        }          
        redirect('mainpanel/controller_programacion/listar/'.$data["data"]->evento_id);
    }



    
}
?>
