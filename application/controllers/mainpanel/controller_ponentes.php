<?php
class Controller_ponentes extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');        
        $this->load->model('mainpanel/Model_ponentes');                
        $this->load->library('my_upload');
        $this->current_section="ponentes";
        date_default_timezone_set("America/Lima");
    }
    
    
    public function listar() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']        = $this->current_section;
        $menu['lista_menu']             = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']                 = "ponentes/index_view";

        $where                          = ['id >'=>0];
        $table                          = "ponentes";
        $data["data"]                   = $this->Model_base->getList($table,$where);

        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']             = "ponentes/edit_view";

        $table                      = 'ponentes';
        $where                      = ['id'=>$id];
        $data["data"]               = $this->Model_base->get($table,$where);      
        
        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data                   = $this->input->post();
        $data['fecha_update']   = date('Y-m-d h:m:s');  

        $imgatng                = $this->input->post('imgatng');
        unset($data['imgatng']);

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 300;
            $this->my_upload->image_y          = 300;   
            $this->my_upload->process('./files/ponentes/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/ponentes/" . $this->my_upload->file_dst_name , "./files/ponentes/" . $newfilename);
                $data['imagen'] = $newfilename;
                $this->my_upload->clean();
                @unlink('./files/ponentes/'.$imgatng);
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_ponentes/edit/'.$data['id']);
            }
        }  

        $where      = ['id'=>$data['id']];
        $table      = 'ponentes';
        $result=$this->Model_base->update($data,$table,$where);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información '.$error;
            $this->session->set_userdata("error",$error);            
        }                       
        redirect('mainpanel/controller_ponentes/edit/'.$data['id']);
    }
    

    
    public function nuevo() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                         = $this->uri->segment(4); 
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true);

       
        $where                          = ['id >'=>0];
        $data["orden"]                  = $this->Model_ponentes->lastItem($where)->orden;               
        $data["orden"]                  = (int)$data["orden"] + 1;
        
        
        $data['cuerpo']="ponentes/nuevo_view";
        
        $this->load->view("mainpanel/includes/template", $data);        
    }
    
    public function grabar() {
        $this->validacion->validacion_login();
        
        $data                   = $this->input->post();
        $data['fecha_insert']   = date('Y-m-d h:m:s'); 
       // $id                     = $data['id'];
      
        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 300;
            $this->my_upload->image_y          = 300;   
            $this->my_upload->process('./files/ponentes/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/ponentes/" . $this->my_upload->file_dst_name , "./files/ponentes/" . $newfilename);
                $data['imagen'] = $newfilename;
                $this->my_upload->clean();
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_ponentes/nuevo/');
            }
        }  else
        {
            $error = $this->my_upload->error;
            $this->session->set_userdata("error",$error);       
            redirect('mainpanel/controller_ponentes/nuevo/');
        }        
 
        
                
        $resultado = $this->Model_base->save('ponentes',$data);
        if($resultado==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
            redirect('mainpanel/controller_ponentes/listar/');
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);
            redirect('mainpanel/controller_ponentes/nuevo/');
        }                   
    }
	
    
    public function delete($id) {
        $this->validacion->validacion_login();

        $where                      = ['id'=>$id];
        $table                      = 'ponentes';
        $data["data"]               = $this->Model_base->get($table,$where);  

        @unlink('./files/ponentes/'.$data["data"]->imagen);

        $result=$this->Model_base->delete($id,'ponentes');
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información ';
            $this->session->set_userdata("error",$error);            
        }          
        redirect('mainpanel/controller_ponentes/listar/');
    }



    
}
?>
