<?php
class Controller_informativa extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_informativa');
        $this->load->library('my_upload');
    }
    

    
    public function edit($seccion) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section'] = $seccion;
        $menu['lista_menu'] = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']="informativa/index_view";
        // *****************************************************************
        
        // INFORMACION
        $data["seccion"] = $seccion;        
        $contenido= $this->Model_informativa->getContenido($seccion);
        $data["data"] = $contenido;        
        $this->load->view("mainpanel/includes/template", $data);
    }
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data = $this->input->post();
        // $texto=$this->input->post('texto');
        // $texto=str_replace("&gt;",">",$texto);
        // $texto=str_replace("&lt;","<",$texto);       
        $seccion = $this->input->post('seccion');
        $imgantg = $data['imgantg'];
        unset($data['imgantg']);


        if(isset($_FILES["imagen"])){
            $this->my_upload->upload($_FILES["imagen"]);
            if ( $this->my_upload->uploaded == true  )
            {
                $this->my_upload->allowed          = array('image/*');
                $this->my_upload->image_resize     = true;
                $this->my_upload->image_ratio_crop = true;
                switch ($seccion) {
                    case 'txt_conocenos':
                        $this->my_upload->image_x          = 570;
                        $this->my_upload->image_y          = 320; 
                        break;
                    
                    case 'txt_time_rever':
                        $this->my_upload->image_x          = 640;
                        $this->my_upload->image_y          = 303; 
                        break;
                    case 'txt_testimonios':
                        $this->my_upload->image_x          = 1920;
                        $this->my_upload->image_y          = 565; 
                        break;     
                    case 'txt_has_pregunta':
                        $this->my_upload->image_x          = 825;
                        $this->my_upload->image_y          = 679; 
                        break;   
                    case 'txt_register_form':
                        $this->my_upload->image_x          = 500;
                        $this->my_upload->image_y          = 637; 
                        break;                                  
                    case 'txt_texto':
                        $this->my_upload->image_x          = 60;
                        $this->my_upload->image_y          = 60; 
                        break;                         
                }            
                $this->my_upload->process('./files/informativa/');
                
                if ($this->my_upload->processed == true ) {
                    $temp = explode(".", $this->my_upload->file_dst_name);                
                    $newfilename = round(microtime(true)) . '.' . end($temp);
                    rename("./files/informativa/" . $this->my_upload->file_dst_name , "./files/informativa/" . $newfilename);
                    $data['imagen'] = $newfilename;                
                    @unlink("./files/informativa/".$imgantg);
                    $this->my_upload->clean();
                } else {
                    $error = $this->my_upload->error;
                }
            }
        }
       
        if(isset($error)){
           $this->session->set_userdata("error",$error);
        }else{
            $resultado = $this->Model_informativa->updateSeccion($seccion, $data);
            if($resultado==true){
                $this->session->set_userdata("success",'Se procesó correctamente la información');
                
            }else{
                $error='Ocurrió un error al procesar su información';
                $this->session->set_userdata("error",$error);
                
            }             
        }
        //die();
     
        redirect('mainpanel/controller_informativa/edit/'.$seccion);
    }
}
?>