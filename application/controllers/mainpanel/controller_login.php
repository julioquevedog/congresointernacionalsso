<?php
class Controller_login extends CI_Controller {
	function __construct()
	{
		parent::__construct();
                $this->load->library('validacion');
                $this->load->model('mainpanel/model_login');
                $this->load->model('mainpanel/model_base');                
	}
	public function index()
	{
            $this->validacion->validacion_login();
            // GENERAL *********************************************************
            $data['current_section'] = 'inicio';
            $menu['lista_menu'] = $this->load->view('mainpanel/includes/menu', $data, true);
            $this->load->view('mainpanel/includes/header_view', $menu, true);
            $data['modal'] = '';
            $this->load->view('mainpanel/includes/footer_view', $data, true); 
            $data['cuerpo']="admin_inicio";
            // *****************************************************************
            
            $this->load->view("mainpanel/includes/template", $data);
	}
        
        public function login(){        
            $this->load->view("mainpanel/login_view");
        }
        
        public function validar() {

            // PROCESAR LOGIN
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $result = $this->model_login->valida_usuario($username, $password);
            $result=explode("###",$result);
            $pr=$result[0];
            switch ($pr){
                case 'ok':
                    $this->session->set_userdata('usuadm', true);
                    $this->session->set_userdata('nombre_admin', $result[1]);
                    $this->session->set_userdata('id_admin', $result[2]);  
                    $this->session->set_userdata('usuario', $result[3]);                      
                    $ip=getRealIP();
                    $data=['fecha'=>gmdate("Y-m-d H:i:s",time()-18000),'ip'=>$ip,'idusuario'=>$this->session->userdata('usuadm'),'usuario'=>$this->session->userdata('nombre_admin')];
                    $this->model_base->save('sesiones', $data);
                    //echo '<pre>' . print_r($_SESSION, TRUE) . '</pre>';
                    //die();                 
                    break;
                case 'estado':
                    $this->session->set_userdata('error', 'Usuario Inactivo');                    
                    break;
                case 'password':
                    $this->session->set_userdata('error', 'Password Incorrecto');                    
                    break;
                case 'usuario':
                    $this->session->set_userdata('error', 'Usuario Incorrecto');                    
                    break;                
            }
            redirect('mainpanel');
        }
        
        public function logout() {
            $this->session->unset_userdata('usuadm');
            $this->session->unset_userdata('nombre_admin');
            $this->session->unset_userdata('id_admin');
            $this->session->unset_userdata('usuario');            
            $url= base_url()."mainpanel";
            redirect($url);
        }
}
?>