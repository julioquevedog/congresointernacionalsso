<?php
class Controller_eventos extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');        
        $this->load->library('my_upload');
        $this->current_section="eventos";
        date_default_timezone_set("America/Lima");
    }
    
    
    public function listar() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']        = $this->current_section;
        $menu['lista_menu']             = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']                 ="eventos/index_view";
        $table                          = "eventos";
        $data["data"]                   = $this->Model_base->getList($table);
        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']             = "eventos/edit_view";
        $table                      = 'eventos';
        $where                      = ['id'=>$id];
        $data["data"]               = $this->Model_base->get($table,$where);        
        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data                   = $this->input->post();
        $data['fecha_update']   = date('Y-m-d h:m:s');         
        
        $imgatng                = $this->input->post('imgatng');
        $archivotng             = $this->input->post('archivotng');      
        $imagen_plannerantg     = $this->input->post('imagen_plannerantg');              

        unset($data['imgatng']);
        unset($data['archivotng']);        
        unset($data['imagen_plannerantg']);        

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 1920;
            $this->my_upload->image_y          = 940;   
            $this->my_upload->process('./files/eventos/');
            if ($this->my_upload->processed == true ) {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/eventos/" . $this->my_upload->file_dst_name , "./files/eventos/" . $newfilename);
                $data['imagen'] = $newfilename;
                $this->my_upload->clean();
                @unlink('./files/eventos/'.$imgatng);
            } else {
                $error = $this->my_upload->error;
            }
        }

        $this->my_upload->upload($_FILES["imagen_planner"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 150;
            $this->my_upload->image_y          = 150;   
            $this->my_upload->process('./files/eventos/');
            if ($this->my_upload->processed == true ) {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/eventos/" . $this->my_upload->file_dst_name , "./files/eventos/" . $newfilename);
                $data['imagen_planner'] = $newfilename;
                $this->my_upload->clean();
                @unlink('./files/eventos/'.$imagen_plannerantg);
            } else {
                $error = $this->my_upload->error;
            }
        }        

        $this->my_upload->upload($_FILES["archivo"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('application/*');
            $this->my_upload->process('./files/eventos/');
            if ($this->my_upload->processed == true ) {
                $newfilename = $this->my_upload->file_dst_name;
                $data['archivo'] = $newfilename;
                $this->my_upload->clean();
                @unlink('./files/eventos/'.$archivotng);
            } else {
                $error = $this->my_upload->error;
            }
        }        
        
        if(isset($error)){
            $this->session->set_userdata("error",$error);                  
        }else{
            $where      = ['id'=>$data['id']];
            $table      = 'eventos';
            $result=$this->Model_base->update($data,$table,$where);
            if($result==true){
                $this->session->set_userdata("success",'Se procesó correctamente la información');
            }else{
                $error='Ocurrió un error al procesar su información '.$error;
                $this->session->set_userdata("error",$error);            
            }                       
        }
        redirect('mainpanel/controller_eventos/edit/'.$data['id']);
    }
    

    
    public function nuevo() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true);

        $data['cuerpo']="eventos/nuevo_view";
        
        $this->load->view("mainpanel/includes/template", $data);        
    }
    
    public function grabar() {
        $this->validacion->validacion_login();
        // GRABAR CATEGORIA
        $data= $this->input->post();
        $data['fecha_insert']=date('Y-m-d h:m:s'); 

  


        $this->my_upload->upload($_FILES["imagen_planner"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 150;
            $this->my_upload->image_y          = 150;   
            $this->my_upload->process('./files/eventos/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/eventos/" . $this->my_upload->file_dst_name , "./files/eventos/" . $newfilename);
                $data['imagen_planner'] = $newfilename;
                $this->my_upload->clean();
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_eventos/nuevo');
            }
        }  
        else
        {
            $error = $this->my_upload->error;
            $this->session->set_userdata("error",$error);       
            redirect('mainpanel/controller_eventos/nuevo');
        }  
        
        $this->my_upload->upload($_FILES["archivo"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('application/*');
            $this->my_upload->process('./files/eventos/');
            if ($this->my_upload->processed == true ) {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/eventos/" . $this->my_upload->file_dst_name , "./files/eventos/" . $newfilename);
                $data['archivo'] = $newfilename;
                $this->my_upload->clean();
                @unlink('./files/eventos/'.$imgatng2);
            } else {
                $error = $this->my_upload->error;
            }
        }   
        else
        {
            $error = $this->my_upload->error;
            $this->session->set_userdata("error",$error);       
            redirect('mainpanel/controller_eventos/nuevo');
        }          

        $this->my_upload->upload($_FILES["imagen"]);
        if ( $this->my_upload->uploaded == true  )
        {
            $this->my_upload->allowed          = array('image/*');
            $this->my_upload->image_resize     = true;
            $this->my_upload->image_ratio_crop = true;
            $this->my_upload->image_x          = 1920;
            $this->my_upload->image_y          = 940;   
            $this->my_upload->process('./files/eventos/');
            if ( $this->my_upload->processed == true )
            {
                $temp = explode(".", $this->my_upload->file_dst_name);                
                $newfilename = round(microtime(true)) . '.' . end($temp);
                rename("./files/eventos/" . $this->my_upload->file_dst_name , "./files/eventos/" . $newfilename);
                $data['imagen'] = $newfilename;
                
                $resultado = $this->Model_base->save('eventos',$data);
                if($resultado==true){
                    $this->session->set_userdata("success",'Se procesó correctamente la información');
                    redirect('mainpanel/controller_eventos/listar');
                }else{
                    $error='Ocurrió un error al procesar su información';
                    $this->session->set_userdata("error",$error);
                    redirect('mainpanel/controller_eventos/nuevo');
                }                   
            }
            else
            {
                $error = $this->my_upload->error;
                $this->session->set_userdata("error",$error);       
                redirect('mainpanel/controller_eventos/nuevo');
            }
        }
        else
        {
            $error = $this->my_upload->error;
            $this->session->set_userdata("error",$error);       
            redirect('mainpanel/controller_eventos/nuevo');
        }
    }
	
    
    public function delete($id) {
        $this->validacion->validacion_login();

        $table                  = 'eventos';
        $where                  = ['id'=>$id];
        $evento                 = $this->Model_base->get($table,$where);
        $imagen                 = $evento->imagen;
        $imagen2                = $evento->imagen2;
        $imagen_planner         = $evento->imagen_planner;                
        $archivo                = $evento->archivo;                      

        @unlink('files/eventos/'.$imagen);
        @unlink('files/eventos/'.$imagen2);
        @unlink('files/eventos/'.$imagen_planner);                
        @unlink('files/eventos/'.$archivo);
        

        $result=$this->Model_base->delete($id,'eventos');
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información ';
            $this->session->set_userdata("error",$error);            
        }          
        redirect('mainpanel/controller_eventos/listar');
    }



    
}
?>
