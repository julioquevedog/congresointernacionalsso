<?php
class Controller_bloques extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->library('validacion');
        $this->load->model('mainpanel/Model_base');        
        $this->load->model('mainpanel/Model_bloques');                
        $this->load->library('my_upload');
        $this->current_section="eventos";
        date_default_timezone_set("America/Lima");
    }
    
    
    public function listar() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                             = $this->uri->segment(4);        
        $data['current_section']        = $this->current_section;
        $menu['lista_menu']             = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']                 = "bloques/index_view";

        $where                          = ['programacion_id'=>$id];
        $table                          = "bloques";
        $data["data"]                   = $this->Model_base->getList($table,$where);


        $where                          = ['id'=>$id];
        $data["programacion"]           = $this->Model_base->get('programacion',$where);

        $where                          = ['id'=>$data["programacion"]->evento_id];
        $data["evento"]                 = $this->Model_base->get('eventos',$where);        

        $this->load->view("mainpanel/includes/template", $data);
    }
    
    
    public function edit($id) {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true); 
        $data['cuerpo']             = "bloques/edit_view";

        $table                      = 'bloques';
        $where                      = ['id'=>$id];
        $data["data"]               = $this->Model_base->get($table,$where);      
        
        $where                      = ['id'=>$data["data"]->programacion_id];
        $data["programacion"]       = $this->Model_base->get('programacion',$where);

        $where                      = ['id'=>$data["programacion"]->evento_id];
        $data["evento"]             = $this->Model_base->get('eventos',$where);   


        $this->load->view("mainpanel/includes/template", $data);
    }
    
   
    
    public function actualizar() {
        $this->validacion->validacion_login();
        $data                   = $this->input->post();
        $data['fecha_update']   = date('Y-m-d h:m:s');         
        
        

        

        $where      = ['id'=>$data['id']];
        $table      = 'bloques';
        $result=$this->Model_base->update($data,$table,$where);
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información '.$error;
            $this->session->set_userdata("error",$error);            
        }                       
        redirect('mainpanel/controller_bloques/edit/'.$data['id']);
    }
    

    
    public function nuevo() {
        $this->validacion->validacion_login();
        // GENERAL *********************************************************
        $id                         = $this->uri->segment(4); 
        $data['current_section']    = $this->current_section;
        $menu['lista_menu']         = $this->load->view('mainpanel/includes/menu', $data, true);
        $this->load->view('mainpanel/includes/header_view', $menu, true);
        $this->load->view('mainpanel/includes/footer_view', $data, true);

       
        $where                      = ['id'=>$id];
        $data["programacion"]       = $this->Model_base->get('programacion',$where);

        $where                      = ['id'=>$data["programacion"]->evento_id];
        $data["evento"]             = $this->Model_base->get('eventos',$where);          

        $where                      = ['programacion_id'=>$data["programacion"]->id];
        $data["orden"]              = $this->Model_bloques->lastItem($where)->orden;               
        $data["orden"]              = (int)$data["orden"] + 1;
        
        
        $data['cuerpo']="bloques/nuevo_view";
        
        $this->load->view("mainpanel/includes/template", $data);        
    }
    
    public function grabar() {
        $this->validacion->validacion_login();
        
        $data                   = $this->input->post();
        $data['fecha_insert']   = date('Y-m-d h:m:s'); 
        $id                     = $data['programacion_id'];


        
                
        $resultado = $this->Model_base->save('bloques',$data);
        if($resultado==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
            redirect('mainpanel/controller_bloques/listar/'.$id);
        }else{
            $error='Ocurrió un error al procesar su información';
            $this->session->set_userdata("error",$error);
            redirect('mainpanel/controller_bloques/nuevo/'.$id);
        }                   
    }
	
    
    public function delete($id) {
        $this->validacion->validacion_login();

        $where                      = ['id'=>$id];
        $table                      = 'bloques';
        $data["data"]               = $this->Model_base->get($table,$where);  

        //@unlink('./files/bloques/'.$data["data"]->imagen);

        $result=$this->Model_base->delete($id,'bloques');
        if($result==true){
            $this->session->set_userdata("success",'Se procesó correctamente la información');
        }else{
            $error='Ocurrió un error al procesar su información ';
            $this->session->set_userdata("error",$error);            
        }          
        redirect('mainpanel/controller_bloques/listar/'.$data["data"]->programacion_id);
    }



    
}
?>
