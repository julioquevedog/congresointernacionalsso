<?php
//class Controller_index extends Controller_padre {
    class Controller_index extends CI_Controller {    
    
	function __construct()
	{
		parent::__construct();
		$this->load->model('frontend/model_index');
                
                	}
	function index(){
$this->load->view("frontend/index");

}
	function index2()
	{

            // LOAD COMMON **************************
            Controller_padre::carga_comun();
            // **************************************
            
            
            // ******************************GENERAL
            $data=array();
            $data['seccion'] = 'index';
            $dataPrincipal['view_head']=$this->load->view('frontend/includes/view_head', $data, true);            
            $dataPrincipal['view_menu']=$this->load->view('frontend/includes/view_menu', $data, true);
            $dataPrincipal['view_header']=$this->load->view('frontend/includes/view_header', $data, true);
            $dataPrincipal['view_footer']=$this->load->view('frontend/includes/view_footer', $data, true);
            $dataPrincipal['view_foot']=$this->load->view('frontend/includes/view_foot', $data, true);            
            // **************************************


            
            // ******************************BANNERS
            $dataPrincipal['banners'] = $this->model_index->get('banners');            
            // *************************************

            // ******************************ACCESOS INI
            $dataPrincipal['accesosini'] = $this->model_index->get('accesosini');            
            // **************************************            

            $this->load->view("frontend/index", $dataPrincipal);
	}

}
?>