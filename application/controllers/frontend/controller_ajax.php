<?php
require_once APPPATH.'libraries/sendgrid-php/sendgrid-php.php';
class Controller_ajax extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->library('my_phpmailer');  
        $this->load->model('frontend/model_index');   
        $this->load->library('form_validation');                          
    }

    public function index()	{

    }
    
    function send_email_sendgrid(){
        $param = $this->input->post('data');

        // **** ENVIO EMAIL CON SENDGRID ****
        $sendgrid_apikey = API_KEY_SENDGRID;

        $sendgrid = new SendGrid($sendgrid_apikey);
        $url = 'https://api.sendgrid.com/';
        $pass = $sendgrid_apikey;
        
        $params = array(
            'to'        => $param['to'],
            'toname'    => $param['toname'],
            'from'      => $param['from'],
            'fromname'  => $param['fromname'],
            'subject'   => $param['subject'],
            'html'      => $param['html']
          );
        
          if(isset($param['cc'])){
              if($param['cc']<>"") {$params['cc'] = $param['cc'];}
          }

          if(isset($param['bcc'])){
              if($param['bcc']<>"") {$params['bcc'] = $param['bcc'];}
          }                  
          

        $request =  $url.'api/mail.send.json';
        
        $session = curl_init($request);
        curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $sendgrid_apikey));
        curl_setopt ($session, CURLOPT_POST, true);
        curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        
        $response = curl_exec($session);
        curl_close($session);
        
        if($response){
            echo json_encode(['result'=>1]);
        }else{
            echo json_encode(['result'=>0]);
        }
        
        // **** ENVIO EMAIL CON SENDGRID ****        
    }

    public function verifica_intruso(){

        $srcs           = $this->input->post('data');
        $has_private    = 0;
        foreach ($srcs as $value) {
            $valueArray = explode("/",$value);
            $posicion   = count($valueArray);
            $code       = $valueArray[$posicion-1];
            $where      = ['code'=>$code,'tipo'=>1];
            $resultado  = $this->model_index->verifica_code_video('videos',$where); 
            $resultado  = count($resultado);
            $has_private = $has_private + $resultado;
        }

        if($has_private>0){
            if(!$this->session->userdata('usu_video')){
                echo json_encode(['result'=>1]);
            }else{
                echo json_encode(['result'=>0]);
            }
        }else{
            echo json_encode(['result'=>0]);
        }        
    } 
}
?>
