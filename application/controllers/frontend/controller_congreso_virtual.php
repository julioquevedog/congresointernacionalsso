<?php
date_default_timezone_set("America/Lima");
ob_start();
require_once APPPATH.'libraries/sendgrid-php/sendgrid-php.php';
class Controller_congreso_virtual extends CI_Controller {
	function __construct()
	{
		parent::__construct();
        $this->load->model('frontend/model_index');
        $this->load->model('frontend/model_congreso_virtual');        
        $this->load->library('my_phpmailer');  
        // load form and url helpers
        $this->load->helper(array('form', 'url'));
        // load form_validation library
        $this->load->library('form_validation');
    }

	function index()
	{
            // ******************************VALIDA LA EXISTENCIA DE LA SESION
            if(issetSession() == false) {
                redirect('/congreso-virtual/login');
            }
            
            // ******************************GENERAL
            $data=array();
            $data['seccion'] = 'congreso-virtual';
            $dataPrincipal['view_head']=$this->load->view('frontend/includes/view_head', $data, true);            
            $dataPrincipal['view_menu']=$this->load->view('frontend/includes/view_menu', $data, true);
            $dataPrincipal['view_header']=$this->load->view('frontend/includes/view_header', $data, true);
            $dataPrincipal['view_footer']=$this->load->view('frontend/includes/view_footer', $data, true);
            // **************************************

            // ******************************EVENTO
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['evento']        = $this->model_index->getEvento('eventos',$where);
            if(count($dataPrincipal['evento'])==0){
                $where                          = ['id >'=> 0];
                $dataPrincipal['evento']        = $this->model_index->getEvento('eventos',$where);
            }
            // $data['evento']                 = $dataPrincipal['evento'];
            // $dataPrincipal['view_foot']     = $this->load->view('frontend/includes/view_foot', $data, true);            
            // *************************************

            // ******************************PROGRAMACION
            $where                          = ['evento_id'=> $dataPrincipal['evento']['id'],'estado'=>'A'];
            $dataPrincipal['programacion']  = $this->model_index->get('programacion',$where);            
            // *************************************

            // ******************************PONENTES
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['ponentes']      = $this->model_index->get('ponentes',$where);            
            // *************************************            

            // ******************************SPONSOR
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['sponsor']       = $this->model_index->get('sponsor',$where);            
            // *************************************  
          
            // ******************************STANDS
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['stands']       = $this->model_index->get('stands',$where);            
            // *************************************     
            
            // ******************************SERVICIOS STANDS
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['servicios_stands']       = $this->model_index->get('servicios_stands',$where);            
            // *************************************              
            
            $this->load->view("frontend/congreso_virtual/index", $dataPrincipal);
	}

	function meeting()
	{
            $this->load->view("frontend/congreso_virtual/meeting");
    }

	function screen_black()
	{
            $this->load->view("frontend/congreso_virtual/screen_black");
    }    
    
    
	function login()
	{
            // ******************************VALIDA LA EXISTENCIA DE LA SESION
            
            if(issetSession() == true) {
                redirect('/congreso-virtual');
            }
            //issetSession();
            // ******************************GENERAL
            $data=array();
            $data['seccion'] = 'congreso-virtual-login';
            $dataPrincipal['view_head']=$this->load->view('frontend/includes/view_head', $data, true);            
            $dataPrincipal['view_menu']=$this->load->view('frontend/includes/view_menu', $data, true);
            $dataPrincipal['view_header']=$this->load->view('frontend/includes/view_header', $data, true);
            $dataPrincipal['view_footer']=$this->load->view('frontend/includes/view_footer', $data, true);
            // **************************************

            // ******************************EVENTO
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['evento']        = $this->model_index->getEvento('eventos',$where);
            if(count($dataPrincipal['evento'])==0){
                $where                          = ['id >'=> 0];
                $dataPrincipal['evento']        = $this->model_index->getEvento('eventos',$where);
            }
            $data['evento']                 = $dataPrincipal['evento'];
            $dataPrincipal['view_foot']     = $this->load->view('frontend/includes/view_foot', $data, true);            
            // *************************************

            // ******************************SPONSORS
            $where                          = ['estado'=>'A'];
            $dataPrincipal['sponsors']  = $this->model_index->get('sponsor',$where);            
            // *************************************


            $this->load->view("frontend/congreso_virtual/login", $dataPrincipal);
    }
    
	function forget()
	{

            
            // ******************************GENERAL
            $data=array();
            $data['seccion'] = 'congreso-virtual';
            $dataPrincipal['view_head']=$this->load->view('frontend/includes/view_head', $data, true);            
            $dataPrincipal['view_menu']=$this->load->view('frontend/includes/view_menu', $data, true);
            $dataPrincipal['view_header']=$this->load->view('frontend/includes/view_header', $data, true);
            $dataPrincipal['view_footer']=$this->load->view('frontend/includes/view_footer', $data, true);
            // **************************************

            // ******************************EVENTO
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['evento']        = $this->model_index->getEvento('eventos',$where);
            if(count($dataPrincipal['evento'])==0){
                $where                          = ['id >'=> 0];
                $dataPrincipal['evento']        = $this->model_index->getEvento('eventos',$where);
            }
            $data['evento']                 = $dataPrincipal['evento'];
            $dataPrincipal['view_foot']     = $this->load->view('frontend/includes/view_foot', $data, true);            
            // *************************************            

            $this->load->view("frontend/congreso_virtual/forget", $dataPrincipal);
	}    

    function log_off(){
        $this->session->unset_userdata('usu_congreso_virtual');
        redirect('congreso-virtual/login');
    }

    function process_logueo(){
        $data = $this->input->post();
        $correo           = $this->security->xss_clean($data['correo']);
        $password         = $this->security->xss_clean($data['password']); 
        
        // basic required field
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('correo', 'correo', 'required|valid_email');

        if ($this->form_validation->run() == FALSE)
        {
            
            $this->session->set_flashdata('error_login_congreso_virtual',validation_errors());
            
        }
        else
        {
            $hoy = fecha_hoy_Ymd();            
            $result = $this->model_congreso_virtual->login($correo,$password,$hoy);  
            switch ($result) {
                case 'credenciales':
                    $msg = "El correo o password son incorrectas";
                    $this->session->set_flashdata('error_login_congreso_virtual',$msg);
                    redirect('congreso-virtual/login');
                    break;
                
                case 'estado':
                    $msg = "Su cuenta esta Inactiva";
                    $this->session->set_flashdata('error_login_congreso_virtual',$msg);
                    redirect('congreso-virtual/login');                    
                    break;                    
                
                case 'vigencia':
                    $msg = "Periodo de inicio y fin fuera de rango.";
                    $this->session->set_flashdata('error_login_congreso_virtual',$msg);
                    redirect('congreso-virtual/login');                    
                    break; 

                default:
                    $this->session->set_userdata('usu_congreso_virtual',$result);
                    redirect('congreso-virtual/login');
                    break;                                       
            }

            
        }

          
    }

    function process_forget(){
        $data = $this->input->post();
        $email           = $this->security->xss_clean($data['email']);
        
        // basic required field
        $this->form_validation->set_rules('email', 'Ingrese su email', 'required|valid_email');

        if ($this->form_validation->run() == FALSE)
        {
            
            $this->session->set_flashdata('error_login_congreso_virtual',validation_errors());
            
        }
        else
        {
            $where = ["correo"=>$email,"estado"=>"A"];
            $result = $this->model_index->getItem("usuarios_congreso",$where);
            if(!isset($result) || $result == null || $result == NULL || $result == false){
                    $msg = "Correo electrónico, no registrado";
                    $this->session->set_flashdata('recuperaError',$msg);
                    redirect('congreso-virtual/forget');
            }



            // ******************************EVENTO
            $where         = ['id >'=> 0,'estado'=>'A'];
            $evento        = $this->model_index->getEvento('eventos',$where);
            if(count($evento)==0){
                $where         = ['id >'=> 0];
                $evento        = $this->model_index->getEvento('eventos',$where);
            }        

            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
            $new_password = substr(str_shuffle($permitted_chars), 0, 10);

            $data = ['password'=>$new_password];
            $where = ["correo"=>$email, "estado"=>"A"];
            $rr = $this->model_index->update('usuarios_congreso',$data,$where);



            $url_ingreso = 'https://congresointernacional-sso.com/congreso-virtual/login';

            
            $msg = "<h1 style='text-align:center'>Recuperación de contraseña</h1><br />\n";
            $msg .= "<h4>".$evento['nombre']."</h4><br />\n<br />\n";
            $msg .= "==============================<br>\n";
            $msg .= "<b>EMAIL: </b>".$email."<br />\n";
            $msg .= "<b>NUEVA CONTRASEÑA: </b>".$new_password."<br />\n";
            $msg .= "<b>INGRESA AQUI: </b> <a href='".$url_ingreso."'>".$url_ingreso."</a><br />\n";
            $msg .= "==============================<br>\n<br />\n<br />\n";            
            $msg .= "<br>\n<br>\n";
            $msg .= "Atte.<br>\n";
            $msg .= "PHVA PERÚ";


            $param = [
                "html"=>$msg,
                "to"=>$email,
                "toname"=>"Cliente PHVA PERÚ",
                "from"=>"informes@phvaperu.com",
                "fromname"=>"CISSO 2020",
                "subject"=>"Recuperación contraseña"
            ];
            
            $ret = $this->send_email_sendgrid($param);  
            
            if(!$ret) {
                $msg='Ocurrió un error, vuelva a intentar';
                $this->session->set_flashdata('recuperaError', $msg);
            } else {
                $msg='Se envió un email con su nueva contraseña';
                $this->session->set_flashdata("preguntaExito",$msg);
            }              
            redirect('/congreso-virtual/forget');
        }
          
    }

    function send_email($email){

        //$hoy    = date('Y-m-d H:i:s');

        // ******************************EVENTO
        $where         = ['id >'=> 0,'estado'=>'A'];
        $evento        = $this->model_index->getEvento('eventos',$where);
        if(count($evento)==0){
            $where         = ['id >'=> 0];
            $evento        = $this->model_index->getEvento('eventos',$where);
        }        

        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';
        $new_password = substr(str_shuffle($permitted_chars), 0, 10);
echo $new_password;
        $data = ['password'=>$new_password];
        $where = ["correo"=>$email, "estado"=>"A"];
        $rr = $this->model_index->update('usuarios_congreso',$data,$where);
        var_dump($rr);

//die("ddd");
        $url_ingreso = 'https://congresointernacional-sso.com/congreso-virtual/login';

        if($email){
            $msg = "<h1 style='text-align:center'>Recuperación de contraseña</h1><br />\n";
            $msg .= "<h4>".$evento['nombre']."</h4><br />\n<br />\n";
            $msg .= "==============================<br>\n";
            $msg .= "<b>EMAIL: </b>".$email."<br />\n";
            $msg .= "<b>NUEVA CONTRASEÑA: </b>".$new_password."<br />\n";
            $msg .= "<b>INGRESA AQUI: </b> <a href='".$url_ingreso."'>".$url_ingreso."</a><br />\n";
            $msg .= "==============================<br>\n<br />\n<br />\n";            
            $msg .= "<br>\n<br>\n";
            $msg .= "Atte.<br>\n";
            $msg .= "El Equipo de PHVAPERU SAC";  

//            $correo_notificaciones = getConfig('correo_notificaciones');
            
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->From = 'notificaciones@phvaperu.com';
            $mail->FromName = 'PHVA PERU';
            $mail->AddAddress($email,'PHVAPERU');
            //$mail->Subject = utf8_decode("Recuperación contraseña Congreso Internacional de Seguridad y Salud Ocupacional");
            $mail->Subject = 'Recuperación contraseña';
            $mail->Body = $msg;
            $mail->IsHTML(true);
            $mail->CharSet = 'UTF-8';
            if(!$mail->Send()) {
                $msg='Ocurrió un error, vuelva a intentar:'. $mail->ErrorInfo;
                $this->session->set_flashdata('recuperaError', $msg);
            } else {
                $msg='Se envió un email con su nueva contraseña';
                $this->session->set_flashdata("preguntaExito",$msg);
            }  

        }else{
            $msg='Ocurrió un error';
            $this->session->set_flashdata('recuperaError', $msg);            
        }

        redirect('/congreso-virtual/forget');



    }    


    function procesa_pregunta(){
        $data   = $this->input->post();
        $hoy    = date('Y-m-d H:i:s');

        $dato_insert['pregunta']        = $this->security->xss_clean($data['pregunta']);
        $dato_insert['names']           = $this->security->xss_clean($data['names']);        
        $dato_insert['email']           = $this->security->xss_clean($data['email']);  
        $dato_insert['estado']          = 'A';        
        $dato_insert['fecha_insert']    = $hoy;
        
        $pregunta_id                    = $this->model_index->insert('preguntas',$dato_insert);                    

        // *************************************

        if($pregunta_id){
            $msg = "<h1 style='text-align:center'>Nueva pregunta</h1>";
            $msg .= "Datos de la nueva pregunta:<br />\n";
            $msg .= "==============================<br>\n";
            $msg .= "<b>NOMBRES: </b>".$data['names']."<br />\n";
            $msg .= "<b>EMAIL: </b>".$data['email']."<br />\n";
            $msg .= "<b>PREGUNTA: </b>".$data['pregunta']."<br />\n";
            $msg .= "==============================<br>\n";
            $msg .= "<br>\n<br>\n";
            $msg .= "Atte.<br>\n";
            $msg .= "El Equipo de PHVAPERU SAC";  

            $correo_notificaciones = getConfig('correo_notificaciones');
            
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->From = 'notificaciones@phvaperu.com';
            $mail->FromName = 'Nueva Pregunta';
            $mail->AddAddress($correo_notificaciones,'PHVAPERU');
            $mail->Subject = utf8_decode($data['names']);
            $mail->Body = $msg;
            $mail->IsHTML(true);
    
            if(!$mail->Send()) {
                $msg='Ocurrió un error al enviar su pregunta:'. $mail->ErrorInfo;
                $this->session->set_flashdata('preguntaError', $msg);
            } else {
                $msg='Se envió con exito su pregunta';
                $this->session->set_flashdata("preguntaExito",$msg);
            }  

        }else{
            $msg='Ocurrió un error al enviar su pregunta';
            $this->session->set_flashdata('preguntaError', $msg);            
        }

        redirect('/#has_pregunta');



    }   

    function process_consulta(){
        $data = $this->input->post();

        $correo = $data['correo'];
        $celular = $data['celular'];
        $mensaje = $data['consulta'];
        $nombres = $data['nombres'];

        $html = "Solicitud de servicio - CISSO 2020, datos del solicitante:<br><br>";
        $html.= "Nombres: ".$nombres."<br>";
        $html.= "Correo: ".$correo."<br>";
        $html.= "Celular: ".$celular."<br>";
        $html.= "Mensaje: ".$mensaje."<br><br><br>";
        $html.= "<strong>Atte.<br>PHVA PERÚ</strong>";  
        
        $param = [
            "html"=>$html,
            "to"=>"julioquevedog@gmail.com",
            "toname"=>"Cliente PHVA PERÚ",
            "from"=>"informes@phvaperu.com",
            "fromname"=>"CISSO 2020",
            "subject"=>"Solicitud de servicio - CISSO 2020"
        ];
        
        $ret = $this->send_email_sendgrid($param);
        if($ret){
            redirect('/congreso-virtual');
        }
    }
     
    function send_email_sendgrid($param){

                // **** ENVIO EMAIL CON SENDGRID ****
                $sendgrid_apikey = API_KEY_SENDGRID;
        
                $sendgrid = new SendGrid($sendgrid_apikey);
                $url = 'https://api.sendgrid.com/';
                $pass = $sendgrid_apikey;
                
                $params = array(
                    'to'        => $param['to'],
                    'toname'    => $param['toname'],
                    'from'      => $param['from'],
                    'fromname'  => $param['fromname'],
                    'subject'   => $param['subject'],
                    'html'      => $param['html']
                  );
                
                  if(isset($param['cc'])){
                      if($param['cc']<>"") {$params['cc'] = $param['cc'];}
                  }

                  if(isset($param['bcc'])){
                      if($param['bcc']<>"") {$params['bcc'] = $param['bcc'];}
                  }                  
                  

                $request =  $url.'api/mail.send.json';
                
                $session = curl_init($request);
                curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
                curl_setopt($session, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $sendgrid_apikey));
                curl_setopt ($session, CURLOPT_POST, true);
                curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
                curl_setopt($session, CURLOPT_HEADER, false);
                curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
                
                $response = curl_exec($session);
                curl_close($session);
                
                return $response;
                
            // **** ENVIO EMAIL CON SENDGRID ****        
    }

    function brochure($id){

        $where                      = ['id'=> $id];
        $evento                     = $this->model_index->getItem('eventos',$where); 

        $file = 'files/eventos/'.$evento['archivo']; 
        $filename = $evento['archivo']; 
        header("Content-type: application/octet-stream"); 
        header("Content-Type: application/force-download"); 
        header("Content-Disposition: attachment; filename=\"$filename\"\n"); readfile($file); 
                
    }
    
}
?>