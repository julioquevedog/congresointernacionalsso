<?php
date_default_timezone_set("America/Lima");
ob_start();
class Controller_index extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('frontend/model_index');
        $this->load->library('my_phpmailer');  
        // load form and url helpers
        $this->load->helper(array('form', 'url'));
        // load form_validation library
        $this->load->library('form_validation');
    }

	function index()
	{

           
            
            // ******************************GENERAL
            $data=array();
            $data['seccion'] = 'index';
            $dataPrincipal['view_head']=$this->load->view('frontend/includes/view_head', $data, true);            
            $dataPrincipal['view_menu']=$this->load->view('frontend/includes/view_menu', $data, true);
            $dataPrincipal['view_header']=$this->load->view('frontend/includes/view_header', $data, true);
            $dataPrincipal['view_footer']=$this->load->view('frontend/includes/view_footer', $data, true);
            // **************************************

            // ******************************EVENTO
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['evento']        = $this->model_index->getEvento('eventos',$where);
            if(count($dataPrincipal['evento'])==0){
                $where                          = ['id >'=> 0];
                $dataPrincipal['evento']        = $this->model_index->getEvento('eventos',$where);
            }
            $data['evento']                 = $dataPrincipal['evento'];
            $dataPrincipal['view_foot']     = $this->load->view('frontend/includes/view_foot', $data, true);            
            // *************************************

            // ******************************PROGRAMACION
            $where                          = ['evento_id'=> $dataPrincipal['evento']['id'],'estado'=>'A'];
            $dataPrincipal['programacion']  = $this->model_index->get('programacion',$where);            
            // *************************************


            // ******************************BLOQUES
            //$programacion_ids=[];
            $dataPrincipal['bloques']=  array();
            if(count($dataPrincipal['programacion'])>0){
                foreach ($dataPrincipal['programacion'] as $key => $value) {$programacion_ids[] = $value['id'];}
                $where                          = ['field'=> 'programacion_id','data'=>$programacion_ids];
                $dataPrincipal['bloques']       = $this->model_index->getIn('bloques',$where);    
            }
            // *************************************


            // ******************************TEMAS
             $dataPrincipal['temas'] = array();
            if(count($dataPrincipal['bloques'])>0){
                foreach ($dataPrincipal['bloques'] as $key => $value) {$bloque_ids[] = $value['id'];}
                $where                          = ['field'=> 'a.bloque_id','data'=>$bloque_ids];
                $dataPrincipal['temas']         = $this->model_index->getInTema('temas',$where); 
            }
            // *************************************


            // ******************************VIDEOS
            $where                          = ['estado'=>'A'];
            $dataPrincipal['videos']        = $this->model_index->get('videos',$where);            
            // *************************************


            // ******************************TESTIMONIOS
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['testimonios']   = $this->model_index->get('testimonios',$where);            
            // *************************************
            
            // ******************************PONENTES
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['ponentes']      = $this->model_index->get('ponentes',$where);            
            // *************************************            
            
            // ******************************FAQS
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['faqs']          = $this->model_index->get('faqs',$where);            
            // *************************************  

            // ******************************PRECIOS
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['precios']       = $this->model_index->get('precios',$where);            
            // *************************************  

            // ******************************ALBUM
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['album']         = $this->model_index->get('album',$where);            
            // *************************************  
            
            // ******************************FOTOS
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['fotos']         = $this->model_index->get('fotos',$where);            
            // *************************************              
            
            // ******************************SPONSOR
            $where                          = ['id >'=> 0,'estado'=>'A'];
            $dataPrincipal['sponsor']       = $this->model_index->get('sponsor',$where);            
            // *************************************  

            // ******************************txt_video
            $where                              = ['seccion'=> 'txt_video'];
            $dataPrincipal['txt_video']         = $this->model_index->getItem('textos_web',$where);        
            
            
            // ******************************txt_conocenos
            $where                              = ['seccion'=> 'txt_conocenos'];
            $dataPrincipal['txt_conocenos']     = $this->model_index->getItem('textos_web',$where);        
            
            // ******************************txt_time_rever
            $where                              = ['seccion'=> 'txt_time_rever'];
            $dataPrincipal['txt_time_rever']    = $this->model_index->getItem('textos_web',$where);               
            // *************************************            
            
            // ******************************txt_sponsor
            $where                              = ['seccion'=> 'txt_sponsor'];
            $dataPrincipal['txt_sponsor']       = $this->model_index->getItem('textos_web',$where);               
            // *************************************     

            // ******************************txt_evento
            $where                              = ['seccion'=> 'txt_evento'];
            $dataPrincipal['txt_evento']        = $this->model_index->getItem('textos_web',$where);               
            // *************************************   

            // ******************************txt_testimonios
            $where                              = ['seccion'=> 'txt_testimonios'];
            $dataPrincipal['txt_testimonios']   = $this->model_index->getItem('textos_web',$where);               
            // ************************************* 
            
            // ******************************txt_register_form
            $where                              = ['seccion'=> 'txt_register_form'];
            $dataPrincipal['txt_register_form'] = $this->model_index->getItem('textos_web',$where);               
            // ************************************* 

            // ******************************txt_ponente
            $where                              = ['seccion'=> 'txt_ponente'];
            $dataPrincipal['txt_ponente']       = $this->model_index->getItem('textos_web',$where);               
            // *************************************              
            
            // ******************************txt_faq
            $where                              = ['seccion'=> 'txt_faq'];
            $dataPrincipal['txt_faq']           = $this->model_index->getItem('textos_web',$where);               
            // *************************************              
            
            // ******************************txt_has_pregunta
            $where                              = ['seccion'=> 'txt_has_pregunta'];
            $dataPrincipal['txt_has_pregunta']  = $this->model_index->getItem('textos_web',$where);               
            // *************************************                
            
            // ******************************txt_precios
            $where                              = ['seccion'=> 'txt_precios'];
            $dataPrincipal['txt_precios']       = $this->model_index->getItem('textos_web',$where);               
            // ************************************* 

            // ******************************txt_galeria
            $where                              = ['seccion'=> 'txt_galeria'];
            $dataPrincipal['txt_galeria']       = $this->model_index->getItem('textos_web',$where);               
            // *************************************             

            // ******************************txt_mapa
            $where                              = ['seccion'=> 'txt_mapa'];
            $dataPrincipal['txt_mapa']          = $this->model_index->getItem('textos_web',$where);             
            
            $this->load->view("frontend/index", $dataPrincipal);
	}

    function log_off(){
        $this->session->unset_userdata('usu_video');
        redirect('./#loginVideo');
    }

    function logueo(){
        $data = $this->input->post();
        $correo           = $this->security->xss_clean($data['correo']);
        $password         = $this->security->xss_clean($data['password']); 
        
        // basic required field
        $this->form_validation->set_rules('password', 'password', 'required');
        $this->form_validation->set_rules('correo', 'correo', 'required|valid_email');

        if ($this->form_validation->run() == FALSE)
        {
            
            $this->session->set_flashdata('error_login_video',validation_errors());
            
            
        }
        else
        {
            $hoy = fecha_hoy_Ymd();
            $result = $this->model_index->login($correo,$password,$hoy);             
            switch ($result) {
                case 'credenciales':
                    $msg = "El correo o password son incorrectas";
                    $this->session->set_flashdata('error_login_video',$msg);
                    break;
                
                case 'estado':
                    $msg = "Su cuenta esta Inactiva";
                    $this->session->set_flashdata('error_login_video',$msg);
                    break;                    
                
                case 'vigencia':
                    $msg = "Su cuenta ya no tiene vigencia.";
                    $this->session->set_flashdata('error_login_video',$msg);
                    break; 

                default:
                    $this->session->set_userdata('usu_video',$result);
                    break;                                       
            }

            
        }

        redirect('./#loginVideo');
          
    }

    function procesar_reserva(){
        $data = $this->input->post();
        $hoy                    = date('Y-m-d H:i:s');

        $dato_insert['names']           = $this->security->xss_clean($data['names']);
        $dato_insert['email']           = $this->security->xss_clean($data['email']);        
        $dato_insert['phone']           = $this->security->xss_clean($data['phone']);                
        $dato_insert['comentarios']     = $this->security->xss_clean($data['comentarios']);                        
        $dato_insert['precio_id']       = $this->security->xss_clean($data['precio_id']);                                
        $dato_insert['fecha_insert']    = $hoy;        
        $dato_insert['estado']          = 'A';                



        // ******************************PRECIO PLAN
        $precio_id              = $data['precio_id'];        
        $where                  = ['id'=>$precio_id];
        $plan                   = $this->model_index->getItem('precios',$where);
        $nombrePlan             = $plan['titulo'].' '.$plan['moneda'].$plan['precio'];
        //$nombrePlan             = 'd';        
        // ******************************PRECIO PLAN


        $dato_insert['texto_precio']    = $nombrePlan;                
        $reserva_id                     = $this->model_index->insert('reservas',$dato_insert);                    

        // ******************************EVENTO
        $where                          = ['id >'=> 0,'estado'=>'A'];
        $dataPrincipal['evento']        = $this->model_index->getEvento('eventos',$where);
        if(count($dataPrincipal['evento'])==0){
            $where                      = ['id >'=> 0];
            $dataPrincipal['evento']    = $this->model_index->getEvento('eventos',$where);
        }
        $evento                         = $dataPrincipal['evento'];

        // *************************************

        if($reserva_id){

            $msg = "<h1 style='text-align:center'>Reserva</h1>";
            $msg .= "<h3>Datos de la reserva:</h3>";
            $msg .= "==============================<br>\n";
            $msg .= "<b>NOMBRES: </b>".$data['names']."<br />\n";
            $msg .= "<b>EMAIL: </b>".$data['email']."<br />\n";
            $msg .= "<b>TELEFONO: </b>".$data['phone']."<br />\n";
            $msg .= "<b>EVENTO: </b>".$evento['nombre']."<br />\n";
            $msg .= "<b>PLAN INTERESADO: </b>".$nombrePlan."<br />\n";            
            $msg .= "<b>COMENTARIO: </b>".$data['comentarios']."<br />\n";
            $msg .= "<b>REALIZADO: </b>".$hoy."<br />\n";            
            $msg .= "==============================<br>\n";
            $msg .= "<br>\n<br>\n";
            $msg .= "Atte.<br>\n";
            $msg .= "El Equipo de PHVAPERU SAC";             

            $correo_notificaciones = getConfig('correo_notificaciones');
            
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->From = 'notificaciones@phvaperu.com';
            $mail->FromName = 'Reserva CONGRESO';
            $mail->AddAddress($correo_notificaciones,'PHVAPERU');
            $mail->Subject = utf8_decode($data['names'].' / '.$evento['nombre']);
            $mail->Body = $msg;
            $mail->IsHTML(true);
    
            if(!$mail->Send()) {
                $msg='Ocurrió un error al enviar sus datos :'. $mail->ErrorInfo;
                $this->session->set_flashdata('reservaError', $msg);
            } else {
                $msg='Se procesó con exito la reserva';
                $this->session->set_flashdata("reseraExito",$msg);
            }  

        }else{
            $msg='Ocurrió un error al enviar sus datos';
            $this->session->set_flashdata('reservaError', $msg);            
        }

        redirect('/#tg-venue');



    }

    function procesa_pregunta(){
        $data   = $this->input->post();
        $hoy    = date('Y-m-d H:i:s');

        $dato_insert['pregunta']        = $this->security->xss_clean($data['pregunta']);
        $dato_insert['names']           = $this->security->xss_clean($data['names']);        
        $dato_insert['email']           = $this->security->xss_clean($data['email']);  
        $dato_insert['estado']          = 'A';        
        $dato_insert['fecha_insert']    = $hoy;
        
        $pregunta_id                    = $this->model_index->insert('preguntas',$dato_insert);                    

        // *************************************

        if($pregunta_id){
            $msg = "<h1 style='text-align:center'>Nueva pregunta</h1>";
            $msg .= "Datos de la nueva pregunta:<br />\n";
            $msg .= "==============================<br>\n";
            $msg .= "<b>NOMBRES: </b>".$data['names']."<br />\n";
            $msg .= "<b>EMAIL: </b>".$data['email']."<br />\n";
            $msg .= "<b>PREGUNTA: </b>".$data['pregunta']."<br />\n";
            $msg .= "==============================<br>\n";
            $msg .= "<br>\n<br>\n";
            $msg .= "Atte.<br>\n";
            $msg .= "El Equipo de PHVAPERU SAC";  

            $correo_notificaciones = getConfig('correo_notificaciones');
            
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->From = 'notificaciones@phvaperu.com';
            $mail->FromName = 'Nueva Pregunta';
            $mail->AddAddress($correo_notificaciones,'PHVAPERU');
            $mail->Subject = utf8_decode($data['names']);
            $mail->Body = $msg;
            $mail->IsHTML(true);
    
            if(!$mail->Send()) {
                $msg='Ocurrió un error al enviar su pregunta:'. $mail->ErrorInfo;
                $this->session->set_flashdata('preguntaError', $msg);
            } else {
                $msg='Se envió con exito su pregunta';
                $this->session->set_flashdata("preguntaExito",$msg);
            }  

        }else{
            $msg='Ocurrió un error al enviar su pregunta';
            $this->session->set_flashdata('preguntaError', $msg);            
        }

        redirect('/#has_pregunta');



    }    

    function brochure($id){

        $where                      = ['id'=> $id];
        $evento                     = $this->model_index->getItem('eventos',$where); 

        $file = 'files/eventos/'.$evento['archivo']; 
        $filename = $evento['archivo']; 
        header("Content-type: application/octet-stream"); 
        header("Content-Type: application/force-download"); 
        header("Content-Disposition: attachment; filename=\"$filename\"\n"); readfile($file); 
                
    }
    
}
?>