function ordProductos(orden) {

    $.ajax({
        type: 'POST',
        url: 'mainpanel/controller_ajax/ordProd',
        data: {orden: orden},
        dataType : 'json',
        success: function(json) {
            result=json.result;
            alert(result);            
        }
    });
}

function update_misdatos() {
    nombres=$("#nombres").val();
    if(nombres===""){
        alert("El campo nombres no puede estar vacio");
        $("#nombres").focus();        
        return false;
    }
    
    usuario=$("#usuario").val();
    if(usuario===""){
        alert("El campo usuario no puede estar vacio");
        $("#usuario").focus();
        return false;
    }
    
    password=$("#password").val();
    if(password===""){
        alert("Ingrese el password actual");
        $("#password").focus();
        return false;
    }
    
    repnewpasw=$("#repnewpasw").val();
    newpasw=$("#newpasw").val();
    if(repnewpasw !== newpasw){
        alert("Confirme bien su password, no coindicen");
        $("#newpasw").focus();
        return false;        
    }    

    $.ajax({
        type: 'POST',
        url: 'mainpanel/controller_ajax/updateusuario',
        data: {nombres: nombres,newpasw:newpasw,password:password},
        dataType : 'json',
        success: function(json) {
            result=json.result;
            switch (result){
                case true:
                    alert("Se modificó con exito !!");
                    location.reload();
                    break;
                case '':
                    alert("El password actual es Incorrecto");
                    break;
            }
        }
    });
}
