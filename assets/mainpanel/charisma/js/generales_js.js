//*********** MODALES **************//
$(".btndelete").on('click',function(){
    var msg=$(this).data('message');
    var id=$(this).data('id');    
    var url=$(this).data('url');        
    var edep=$(this).data('elementos-dependientes');        

    $('#tituloModal').html('Eliminar !');
    $('#cuerpoModal').html('<p>'+msg+'</p>');
    str = '<a href="#" class="btn" data-dismiss="modal">CANCELAR</a>';
    
    if(edep != 0 && edep != undefined && edep!= ''){
        edep = '<p class="red">Existen '+edep+' elementos dependientes. No se puede eliminar.</p>';
        $('#cuerpoModal').append(edep);           

    }else{
        str += '<a href="'+url+'/'+id+'" class="btn btn-primary">BORRAR</a>';
    }
  
    $('#botoneraModal').html(str);
    $('#botoneraModal').show();
    $('#myModal').modal('show');       

});

function deleteNoticia(id) {
    $('#tituloModal').html('Esta a punto de borrar la noticia!');
    $('#cuerpoModal').html('<p>Esta seguro que quiere hacerlo?</p>');
    str = '<a href="#" class="btn" data-dismiss="modal">CANCELAR</a>';
    str += '<a href="mainpanel/noticias/delete/'+id+'" class="btn btn-primary">BORRAR</a>';
    $('#botoneraModal').html(str);
    $('#botoneraModal').show();
    $('#myModal').modal('show');   
}

function deleteServicio(id) {
    $('#tituloModal').html('Esta a punto de borrar el servicio!');
    $('#cuerpoModal').html('<p>Esta seguro que quiere hacerlo?</p>');
    str = '<a href="#" class="btn" data-dismiss="modal">CANCELAR</a>';
    str += '<a href="mainpanel/servicios/delete/'+id+'" class="btn btn-primary">BORRAR</a>';
    $('#botoneraModal').html(str);
    $('#botoneraModal').show();
    $('#myModal').modal('show');   
}

function deleteBanner(id_banner) {
    $('#tituloModal').html('Esta a punto de borrar este banner!');
    $('#cuerpoModal').html('<p>Esta seguro que quiere hacerlo?</p>');
    str = '<a href="#" class="btn" data-dismiss="modal">CANCELAR</a>';
    str += '<a href="mainpanel/banners/delete/'+id_banner+'" class="btn btn-primary">BORRAR</a>';
    $('#botoneraModal').html(str);
    $('#botoneraModal').show();
    $('#myModal').modal('show');   
}

function showBanner(banner, titulo) {
    $('#tituloModal').html(titulo);
    img = '<img src="files/banners/'+banner+'" />';
    $('#cuerpoModal').html(img);
    $('#botoneraModal').hide();
    $('#myModal').modal('show'); 
}

function deleteFoto(idfoto) {
    $('#tituloModal').html('Esta a punto de borrar esta fotografia!');
    $('#cuerpoModal').html('<p>Esta seguro que quiere hacerlo?</p>');
    str = '<a href="#" class="btn" data-dismiss="modal">CANCELAR</a>';
    str += '<a href="mainpanel/fotos/delete/'+idfoto+'" class="btn btn-primary">BORRAR</a>';
    $('#botoneraModal').html(str);
    $('#botoneraModal').show();
    $('#myModal').modal('show');
}
function deleteCategoria(id_categoria, num_productos) {
    $('#tituloModal').html('Esta a punto de borrar esta categoría!');
    $('#cuerpoModal').html('<p>Esta categoria tiene: <strong>'+num_productos+' producto(s)</strong>.</p><p>Al borrar la categoría se eliminaran todos estos productos</p><p>y tambien las fotos por producto.</p>');
    str = '<a href="#" class="btn" data-dismiss="modal">CANCELAR</a>';
    str += '<a href="mainpanel/catalogo/delete/'+id_categoria+'" class="btn btn-primary">BORRAR</a>';
    $('#botoneraModal').html(str);
    $('#botoneraModal').show();
    $('#myModal').modal('show');    
}
function deleteProducto(id_producto) {
    $('#tituloModal').html('Esta a punto de borrar este Producto!');
    $('#cuerpoModal').html('<p>Al borrar el producto se eliminaran tambien sus fotos</p>');
    str = '<a href="#" class="btn" data-dismiss="modal">CANCELAR</a>';
    str += '<a href="mainpanel/catalogo/delete_producto/'+id_producto+'" class="btn btn-primary">BORRAR</a>';
    $('#botoneraModal').html(str);
    $('#botoneraModal').show();
    $('#myModal').modal('show');    
}
//*************  FUNCIONES ******////
$("select[name='id_categorias']").change(function(){
    var cat=$(this).val();
    var subcat=$("select[name='id_categoria_padre']").attr('data-opciones');
    subcat=JSON.parse(subcat);
    console.log(subcat);
    var option='';
    $.each(subcat, function(i, item) {
        if(item.id_categoria==cat){
            option+='<option value="'+item.id_subcategoria+'">'+item.nombre+'</option>';
        }
    })
    option=(option!='')? option:'<option value="0">No hay data</option>';
    $("select[name='id_categoria_padre']").html(option);

});

function valida_banner() {
    titulo=$("#titulo").val();
    if(titulo===""){
        alert("Titulo no puede estar vacio");
        $("#titulo").focus();        
        return false;
    }

    orden=$("#orden").val();
    if(orden===""){
        alert("Ingrese el orden de presentación del Banner");
        $("#orden").focus();        
        return false;
    } 
    
    banner=$("#banner").val();
    if(banner===""){
        alert("Suba una imagen para el Banner");
        return false;
    }    
}
function valida_foto() {
    titulo=$("#titulo").val();
    if(titulo===''){
        alert("Ingrese un tiutlo para la foto");
        $("#titulo").focus();        
        return false;
    }
    
    orden=$("#orden").val();
    if(orden===""){
        alert("Debe ingresar un orden para la fotografia");
        $("#orden").focus();
        return false;
    }     
    
    foto=$("#foto").val();
    if(foto===""){
        alert("Debe subir una fotografia");
        return false;
    }      
}
function valida_categoria() {
    nombre=$("#nombre").val();
    if(nombre===""){
        alert("Debe ingresar el nombre de la Cateogria");
        $("#nombre").focus();        
        return false;
    }
    
    orden=$("#orden").val();
    if(orden===""){
        alert("Debe ingresar un orden para la Categoria");
        $("#orden").focus();
        return false;
    }     
    
    foto=$("#foto").val();
    if(foto===""){
        alert("Debe subir una imagen para la Categoria");
        return false;
    }    
}
function valida_producto() {
    id_categoria_padre=$("#id_categoria_padre").val();
    if(id_categoria_padre===0){
        alert("Debe elejir una Cateogria");
        $("#id_categoria_padre").focus();        
        return false;
    }
    
    nombre=$("#nombre").val();
    if(nombre===""){
        alert("Debe ingresar el nombre");
        $("#nombre").focus();        
        return false;
    }
    
    foto=$("#foto").val();
    if(foto===""){
        alert("Debe subir una imagen para el producto");
        return false;
    }      
    
    orden=$("#orden").val();
    if(orden===""){
        alert("Debe ingresar un orden para el producto");
        $("#orden").focus();
        return false;
    }     
/*     precio=$("#precio").val();
    if(precio===""){
        alert("Debe ingresar el precio del producto");
        $("#precio").focus();
        return false;
    }  */
    sumilla=$("#sumilla").val();
    if(sumilla===""){
        alert("Debe ingresar una sumilla para el producto");
        $("#sumilla").focus();
        return false;
    }   
    var descripcion = CKEDITOR.instances['descripcion'].getData();
    if(descripcion===""){
        alert("Debe ingresar una descripcion para el producto");
        CKEDITOR.instances['descripcion'].focus();
        return false;
    }  
  
}
function valida_informativa() {
   
    titulo=$("#titulo").val();
    if(titulo===""){
        alert("Debe ingresar el titulo");
        $("#titulo").focus();        
        return false;
    }
  
    var texto = CKEDITOR.instances['texto'].getData();
    if(texto===""){
        alert("Debe ingresar un texto para la seccion");
        CKEDITOR.instances['texto'].focus();
        return false;
    }  
  
}


function valida_servicio() {
    titulo=$("#titulo").val();
    if(titulo===""){
        alert("Debe ingresar el titulo del Servicio");
        $("#titulo").focus();        
        return false;
    }
    orden=$("#orden").val();
    if(orden===""){
        alert("Debe ingresar un numero orden para el Servicio");
        $("#orden").focus();        
        return false;
    }
    imagen=$("#imagen").val();
    if(imagen===""){
        alert("Ingrese una imagen para el Servicio");
        $("#imagen").focus();        
        return false;
    }    
    sumilla=$("#sumilla").val();
    if(sumilla===""){
        alert("Debe ingresar la sumilla para el servicio");
        $("#sumilla").focus();        
        return false;
    }
    var descripcion = CKEDITOR.instances['descripcion'].getData();
    if(descripcion===""){
        alert("Debe ingresar la descripcion del servicio");
        CKEDITOR.instances['descripcion'].focus();
        return false;
    }     
}

function valida_noticia() {
    titulo=$("#titulo").val();
    if(titulo===""){
        alert("Debe ingresar el titulo de la Noticia");
        $("#titulo").focus();        
        return false;
    }
    orden=$("#orden").val();
    if(orden===""){
        alert("Debe ingresar un numero orden para la Noticia");
        $("#orden").focus();        
        return false;
    }
    fecha=$("#fecha").val();
    if(fecha===""){
        alert("Debe ingresar la fecha publicacion para la Noticia");
        $("#fecha").focus();        
        return false;
    }
    imagen=$("#imagen").val();
    if(imagen===""){
        alert("Ingrese una imagen para la Noticia");
        $("#imagen").focus();        
        return false;
    }    
    sumilla=$("#sumilla").val();
    if(sumilla===""){
        alert("Debe ingresar la sumilla de la Noticia");
        $("#sumilla").focus();        
        return false;
    }
    var descripcion = CKEDITOR.instances['descripcion'].getData();
    if(descripcion===""){
        alert("Debe ingresar la descripcion de la noticia");
        CKEDITOR.instances['descripcion'].focus();
        return false;
    }     
}

function valida_parametro() {
    tipo=$("#tipo").val();
    llave=$("#llave").val();
    if(llave===""){
        alert("El parametro no puede estar vacio");
        return false;
    }
    
    tipo=$("#tipo").val();
    if(tipo==0){
        valor=$("#valor").val();
        if(valor===""){
            alert("El valor no puede estar vacio");
            return false;
        }
    }
    if(tipo==1){
        var valor = CKEDITOR.instances['valor'].getData();
        if(valor===""){
            alert("El valor no puede estar vacio");
            CKEDITOR.instances['valor'].focus();
            return false;
        } 
    }    

    descripcion=$("#descripcion").val();
    if(descripcion===""){
        alert("La descripcion no puede estar vacio");
        return false;
    }  
        
}


function concatena(id)
{
    id_eliminar=$("#id_eliminar").val();
    id=$("#"+id).val();
    if($("#"+id).is(':checked')===false){
            cad=id_eliminar.split("##");
            rt=cad.length;
            cont2=0;
            for(e=0;e<rt;e++){
                    id_1=cad[e];
                    if(id!==id_1){
                            cont2+=1;
                            if(cont2===1){
                                    str=id_1;
                            }else{
                                    str=str+'##'+id_1;
                            }
                    }
            }
            if(cont2===0){str='';}
    }else{
            if(id_eliminar===''){
                    str=id;								
            }else{
                    str=id_eliminar+'##'+id;
            }
    }

    $("#id_eliminar").val(str);    
}